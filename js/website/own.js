jQuery(document).ready(function(){
    app_navigation();
    app_headerSlider();
    app_fancyVimeo()
});

function app_headerSlider() {
    jQuery('a.headerSlider').on('click', function() {
       var $this = jQuery(this);
       var $headerSlider = jQuery('#l_headerSlider');
       
       $headerSlider.animate({
          marginTop: 0           
       }, 800, 'easeOutBounce', function() {
            jQuery.ajax({
                url: $this.attr('href'),
                type: 'get',
                success: function(data) {
                    $headerSlider.html(data);
                    
                    $headerSlider.find('.background').hide().fadeIn(250);
                }
            });          
       });
       
       return false;
        
    });
    
    jQuery('.closeHeaderSlider').on('click', function() {
       var $this = jQuery(this);
       var $headerSlider = jQuery('#l_headerSlider');
       
       $headerSlider.animate({
          marginTop: -814           
       }, 800, 'easeOutBounce', function() {
            $headerSlider.html('<div class="inner loader"></div>');        
       });
       
    });
}

function app_navigation() {
    // hover subnavigation
    jQuery('#l_navigation > ul > li.withSub').hover(function() {
        var $this = jQuery(this);
        var $child = $this.children('ul');
        
        $this.addClass('hover');
        $child.show();
        
    }, function() {
        var $this = jQuery(this);
        var $child = $this.children('ul');
        
        $this.removeClass('hover');
        $child.hide();        
    });
    
    
    jQuery('#l_navigation > ul > li.withSub').each(function() {
        var $el = jQuery(this);
        
        // subnavigation even
        var $subNavEven = $el.children('ul').children('li:even');
        var subNavEvenMaxWidth = $el.width() + 5;
        
        // subnavigation odd
        var $subNavOdd = $el.children('ul').children('li:odd');
        var subNavOddMaxWidth = $el.width() + 5;
        
        $subNavEven.each(function() {
            var $this = jQuery(this);
            if (subNavEvenMaxWidth < $this.width()) {
                subNavEvenMaxWidth = $this.width();
            }
            $this.css('clear', 'left');
            
        });
        
        $subNavEven.each(function() {
            var $this = jQuery(this);
            $this.width(subNavEvenMaxWidth);
        }).addClass('even');  
        
        
        
        $subNavOdd.each(function() {
            var $this = jQuery(this);
            if (subNavOddMaxWidth < $this.width()) {
                subNavOddMaxWidth = $this.width();
            }
            
        });
        
        $subNavOdd.each(function() {
            var $this = jQuery(this);
            //$this.width(subNavOddMaxWidth);
        }).addClass('odd');    
        
    });
    
}

function app_fancyVimeo() {
    jQuery(".fancyVimeo").livequery('click', function() {
        jQuery.fancybox({
            'padding'       : 0,
            'autoScale'     : false,
            'transitionIn'  : 'none',
            'transitionOut' : 'none',
            'width'         : 980,
            'height'        : 432,
            'overlayOpacity': 0.9,
            'overlayColor': '#000000',
            'titleShow':    false,
            'href'          : 'http://vimeo.com/moogaloop.swf?clip_id=' + jQuery(this).attr('data-clip') + '&autoplay=1',
            'type'          : 'swf'
        });   
    });
    
}
