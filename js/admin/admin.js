jQuery(document).ready(function(){

});

function app_structureTreeView() {
  app_structureSortable('.treeView');
  app_structureSortable('.treeView ol');

  jQuery('.treeView li .node > .expander > a').click(function() {
    $this = jQuery(this);
    if ($this.hasClass('open')) { 
      jQuery.ajax({
        type: 'POST',
        url: '/admin/structure/index/structure/id/' + $this.attr('rel'),
        success: function(msg) {
          $this.addClass('close');
          $this.removeClass('open'); 
          $this.parent().parent().siblings('ol').show();
        }
      });
    }
    else if ($this.hasClass('close')) { 
      jQuery.ajax({
        type: 'POST',
        url: '/admin/structure/index/structure/id/' + $this.attr('rel'),
        success: function(msg) {
          $this.removeClass('close'); 
          $this.addClass('open'); 
          $this.parent().parent().siblings('ol').hide();
        }
      });
    }
  });
  
  jQuery('.treeView .action a.delete').click(function() {
    $this = jQuery(this);
    if (confirm('Wollen sie dieses Struktur-Element wirklich löschen?')) {      
      jQuery.ajax({
        type: 'POST',
        url: $this.attr('href'),
        success: function(msg) {
          $this.parents('.node').parent().fadeOut(250, function() { jQuery(this).remove(); });
        }
      });
    }
    return false;
  });
}

function app_structureSortable(cssClass) {
  jQuery(cssClass).sortable({
    cursor: 'move',
    opacity: 0.7,
    update: function() {
      var order = jQuery(this).sortable("toArray");
      serStr = '';
      var sorterCounter = 0;
      jQuery.each(order, function(index, value) {
        valueSplit = value.split('_');
        if (sorterCounter > 0) { serStr += '&'; }
        serStr = serStr + 'id[' + valueSplit[2] + ']=' + valueSplit[1];
        sorterCounter++;
      });
      jQuery.ajax({
        type: 'POST',
        url: '/admin/structure/index/sort',
        data: serStr,
        success: function(msg) {
          
        }
      });
    }
  }); 
}

function app_structureForm(){
  var $linkDirect = jQuery('#Structure_link_redirect');
  $linkDirect.click(function() {
    if ($linkDirect.is(':checked')) {
      $linkDirect.siblings('.expand').slideDown(250);
    } else {
      $linkDirect.siblings('.expand').slideUp(250);
    }
  });
  
  var $linkRedirectPoint = jQuery('#Structure_link_redirect_point');
  $linkRedirectPoint.change(function() {
    var value = jQuery(this).val();
    if (value == 'other') {
      jQuery('#Structure_point_link').show();
    } else if (value == 'child') {
      jQuery('#Structure_point_link').hide();      
    }
  });
  
  var $isProtected = jQuery('#Structure_is_protected');
  $isProtected.click(function() {
	$this = jQuery(this);
    if ($this.is(':checked')) {
    	$this.siblings('.expand').slideDown(250);
    } else {
    	$this.siblings('.expand').slideUp(250);
    }
  });
}

function app_filemanagerTreeView(checkBoxMode) {
  if (!checkBoxMode) {
	  app_filemanagerSortable('.treeView');
	  app_filemanagerSortable('.treeView ol');
  } else {
	  jQuery('.row.widget .checkbox_filemanager').click(function() {
		 $this = jQuery(this);
		 if ($this.is(':checked')) {
			 jQuery('ol .action input[type="checkbox"]', $this.parents('li.check-' + $this.val())).attr('checked', 'checked');
		 } else {
			 jQuery('ol .action input[type="checkbox"]', $this.parents('li.check-' + $this.val())).attr('checked', '');			 
		 }
	  });
  }

  jQuery('.treeView li .node > .expander > a').click(function() {
    $this = jQuery(this);
    if ($this.hasClass('open')) { 
      if (!checkBoxMode) {
	      jQuery.ajax({
	        type: 'POST',
	        url: '/admin/filemanager/index/file/id/' + $this.attr('rel'),
	        success: function(msg) {
	          $this.addClass('close');
	          $this.removeClass('open'); 
	          $this.parent().parent().siblings('ol').show();
	        }
	      });
      } else {
          $this.addClass('close');
          $this.removeClass('open'); 
          $this.parent().parent().siblings('ol').show();    	  
      }
    }
    else if ($this.hasClass('close')) { 
      if (!checkBoxMode) {
	      jQuery.ajax({
	        type: 'POST',
	        url: '/admin/filemanager/index/file/id/' + $this.attr('rel'),
	        success: function(msg) {
	          $this.removeClass('close'); 
	          $this.addClass('open'); 
	          $this.parent().parent().siblings('ol').hide();
	        }
	      });
      } else {
          $this.removeClass('close'); 
          $this.addClass('open'); 
          $this.parent().parent().siblings('ol').hide();      
      }
    }
  });
  
  jQuery('.treeView .action a.delete').click(function() {
    $this = jQuery(this);
    if (confirm('Wollen sie dieses Datei-Element wirklich löschen?')) {      
      jQuery.ajax({
        type: 'POST',
        url: $this.attr('href'),
        success: function(msg) {
          $this.parents('.node').parent().fadeOut(250, function() { jQuery(this).remove(); });
        }
      });
    }
    return false;
  });
}

function app_filemanagerSortable(cssClass) {
  jQuery(cssClass).sortable({
    cursor: 'move',
    opacity: 0.7,
    update: function() {
      var order = jQuery(this).sortable("toArray");
      serStr = '';
      var sorterCounter = 0;
      jQuery.each(order, function(index, value) {
        valueSplit = value.split('_');
        if (sorterCounter > 0) { serStr += '&'; }
        serStr = serStr + 'id[' + valueSplit[2] + ']=' + valueSplit[1];
        sorterCounter++;
      });
      jQuery.ajax({
        type: 'POST',
        url: '/admin/filemanager/index/sort',
        data: serStr,
        success: function(msg) {
          
        }
      });
    }
  }); 
}

function app_filemanagerForm() {
  app_filemanagerCreateFileFields();
  
  var $isProtected = jQuery('.file_is_protected');
  $isProtected.click(function() {
	$this = jQuery(this);
    if ($this.is(':checked')) {
    	$this.siblings('.expand').slideDown(250);
    } else {
    	$this.siblings('.expand').slideUp(250);
    }
  });
  
  $fileProtectedUser = jQuery('.file_protected_user');
  $fileProtectedUser.click(function() {
		$this = jQuery(this);
		$this.parent().siblings('.expand_user').slideToggle(250);
  });
  
}

var fileTableCount = 0;
function app_filemanagerCreateFileFields()
{
  // handle form for files
  var $fileTable = jQuery('<table></table>');
  $fileTable.attr('class', 'files');
  // label
  $tr = jQuery('<tr></tr>');
  $th = jQuery('<th></th>');
  $th.html('<label for="file_label_' + fileTableCount + '">Bezeichnung</label>');
  $td = jQuery('<td></td>');
  $field = jQuery('<input type="text" value="" name="file_label[' + fileTableCount + ']" id="file_label_' + fileTableCount + '" />');
  $td.append($field);
  $tr.append($th);
  $tr.append($td);
  $fileTable.append($tr);
  // description
  $tr = jQuery('<tr></tr>');
  $th = jQuery('<th></th>');
  $th.html('<label for="file_description_' + fileTableCount + '">Beschreibung</label>');
  $td = jQuery('<td></td>');
  $field = jQuery('<textarea name="file_description[' + fileTableCount + ']" id="file_description_' + fileTableCount + '"></textarea>');
  $td.append($field);
  $tr.append($th);
  $tr.append($td);
  $fileTable.append($tr);
  // is protected
  $tr = jQuery('<tr></tr>');
  $th = jQuery('<th></th>');
  $th.html('<label for="file_is_protected_' + fileTableCount + '">Geschützte Datei</label>');
  $td = jQuery('<td></td>');
  $field = jQuery('<input value="1" class="file_is_protected" type="checkbox" name="file_is_protected[' + fileTableCount + ']" id="file_is_protected_' + fileTableCount + '" />');
  $td.append($field);
  $tr.append($th);
  $tr.append($td);
  $fileTable.append($tr);
  // status
  $tr = jQuery('<tr></tr>');
  $th = jQuery('<th></th>');
  $th.html('<label for="file_status_' + fileTableCount + '">Status</label>');
  $td = jQuery('<td></td>');
  $field = jQuery('<select name="file_status[' + fileTableCount + ']" id="file_status_' + fileTableCount + '"></select>');
  $fieldOption = jQuery('<option value="active">Aktiv</option>');
  $field.append($fieldOption);
  $fieldOption = jQuery('<option value="inactive">Inaktiv</option>');
  $field.append($fieldOption);
  $td.append($field);
  $tr.append($th);
  $tr.append($td);
  $fileTable.append($tr);
  // file
  $tr = jQuery('<tr></tr>');
  $th = jQuery('<th></th>');
  $th.html('<label for="file_file_' + fileTableCount + '">Datei</label>');
  $td = jQuery('<td></td>');
  $field = jQuery('<input type="file" onchange="app_filemanagerFileState(this)" name="file_file[' + fileTableCount + ']" id="file_file_' + fileTableCount + '" />');
  $fieldCheck = jQuery('<div class="uploadChecked" style="display: none;"><input onclick="app_filemanagerRemoveFile(this);" value="1" type="checkbox" name="file_checked[' + fileTableCount + ']" id="file_checked_' + fileTableCount + '" /> <label for="file_checked_' + fileTableCount + '">Diese Datei hochladen</label></div>');
  $td.append($field);
  $td.append($fieldCheck);
  $tr.append($th);
  $tr.append($td);
  $fileTable.append($tr);
  jQuery('.fileRow .addnew').hide();
  jQuery('.fileRow .fileRowContainer').append($fileTable);
  fileTableCount++;
}

function app_filemanagerFileState(el)
{
  var $this = jQuery(el);
  $this.siblings('.uploadChecked').show();
  $this.siblings('.uploadChecked').children('input[type="checkbox"]').attr('checked', 'checked');
  jQuery('.fileRow .addnew').show(); 
}

function app_filemanagerRemoveFile(el)
{
  var $this = jQuery(el);
  if ($this.parents('div.fileRowContainer').children().length > 1) {
      $this.parents('table.files').fadeOut(250, function() { jQuery(this).remove(); });
  } else {
    $this.parent().hide();
    jQuery('.fileRow .addnew').hide(); 
    $this.parents('table.files tr td').children('input').val('');
  }
}

function app_filemanagerDeleteFile(el)
{
	$this = jQuery(el);
    if (confirm('Wollen sie dieses Datei wirklich löschen?')) {      
        jQuery.ajax({
          type: 'POST',
          url: $this.attr('href'),
          success: function(msg) {
            $this.parents('table.files').fadeOut(250, function() { jQuery(this).remove(); });
          }
        });
    }
    return false;
}


function app_linkmanagerForm() {
  app_linkmanagerCreateLinkFields();
  
  var $isProtected = jQuery('.link_is_protected');
  $isProtected.click(function() {
  $this = jQuery(this);
    if ($this.is(':checked')) {
      $this.siblings('.expand').slideDown(250);
    } else {
      $this.siblings('.expand').slideUp(250);
    }
  });
  
  $linkProtectedUser = jQuery('.link_protected_user');
  $linkProtectedUser.click(function() {
    $this = jQuery(this);
    $this.parent().siblings('.expand_user').slideToggle(250);
  });
  
}

var linkTableCount = 0;
function app_linkmanagerCreateLinkFields()
{
  // handle form for links
  var $linkTable = jQuery('<table></table>');
  $linkTable.attr('class', 'files');
  // label
  $tr = jQuery('<tr></tr>');
  $th = jQuery('<th></th>');
  $th.html('<label for="link_label_' + linkTableCount + '">Bezeichnung</label>');
  $td = jQuery('<td></td>');
  $field = jQuery('<input type="text" value="" name="link_label[' + linkTableCount + ']" id="link_label_' + linkTableCount + '" />');
  $td.append($field);
  $tr.append($th);
  $tr.append($td);
  $linkTable.append($tr);
  // description
  $tr = jQuery('<tr></tr>');
  $th = jQuery('<th></th>');
  $th.html('<label for="link_description_' + linkTableCount + '">Beschreibung</label>');
  $td = jQuery('<td></td>');
  $field = jQuery('<textarea name="link_description[' + linkTableCount + ']" id="link_description_' + linkTableCount + '"></textarea>');
  $td.append($field);
  $tr.append($th);
  $tr.append($td);
  $linkTable.append($tr);
  // is protected
  $tr = jQuery('<tr></tr>');
  $th = jQuery('<th></th>');
  $th.html('<label for="link_is_protected_' + linkTableCount + '">Geschützte Datei</label>');
  $td = jQuery('<td></td>');
  $field = jQuery('<input value="1" class="link_is_protected" type="checkbox" name="link_is_protected[' + linkTableCount + ']" id="link_is_protected_' + linkTableCount + '" />');
  $td.append($field);
  $tr.append($th);
  $tr.append($td);
  $linkTable.append($tr);
  // status
  $tr = jQuery('<tr></tr>');
  $th = jQuery('<th></th>');
  $th.html('<label for="link_status_' + linkTableCount + '">Status</label>');
  $td = jQuery('<td></td>');
  $field = jQuery('<select name="link_status[' + linkTableCount + ']" id="link_status_' + linkTableCount + '"></select>');
  $fieldOption = jQuery('<option value="active">Aktiv</option>');
  $field.append($fieldOption);
  $fieldOption = jQuery('<option value="inactive">Inaktiv</option>');
  $field.append($fieldOption);
  $td.append($field);
  $tr.append($th);
  $tr.append($td);
  $linkTable.append($tr);
  // link
  $tr = jQuery('<tr></tr>');
  $th = jQuery('<th></th>');
  $th.html('<label for="link_url_' + linkTableCount + '">Link</label>');
  $td = jQuery('<td></td>');
  $field = jQuery('<input type="text" onchange="app_linkmanagerLinkState(this)" name="link_url[' + linkTableCount + ']" id="link_url_' + linkTableCount + '" />');
  $fieldCheck = jQuery('<div class="uploadChecked" style="display: none;"><input onclick="app_linkmanagerRemoveLink(this);" value="1" type="checkbox" name="link_checked[' + linkTableCount + ']" id="link_checked_' + linkTableCount + '" /> <label for="link_checked_' + linkTableCount + '">Diesen Link speichern</label></div>');
  $td.append($field);
  $td.append($fieldCheck);
  $tr.append($th);
  $tr.append($td);
  $linkTable.append($tr);
  jQuery('.linkRow .addnew').hide();
  jQuery('.linkRow .linkRowContainer').append($linkTable);
  linkTableCount++;
}

function app_linkmanagerLinkState(el)
{
  var $this = jQuery(el);
  $this.siblings('.uploadChecked').show();
  $this.siblings('.uploadChecked').children('input[type="checkbox"]').attr('checked', 'checked');
  jQuery('.linkRow .addnew').show(); 
}

function app_linkmanagerRemoveLink(el)
{
  var $this = jQuery(el);
  if ($this.parents('div.linkRowContainer').children().length > 1) {
      $this.parents('table.files').fadeOut(250, function() { jQuery(this).remove(); });
  } else {
    $this.parent().hide();
    jQuery('.linkRow .addnew').hide(); 
    $this.parents('table.files tr td').children('input').val('');
  }
}

function app_linkmanagerDeleteLink(el)
{
  $this = jQuery(el);
    if (confirm('Wollen sie diesen Link wirklich löschen?')) {      
        jQuery.ajax({
          type: 'POST',
          url: $this.attr('href'),
          success: function(msg) {
            $this.parents('table.files').fadeOut(250, function() { jQuery(this).remove(); });
          }
        });
    }
    return false;
}





function app_newsForm() {
  app_filemanagerCreateFileFields();
  app_linkmanagerCreateLinkFields();
  
  var $isProtected = jQuery('.file_is_protected');
  $isProtected.click(function() {
  $this = jQuery(this);
    if ($this.is(':checked')) {
      $this.siblings('.expand').slideDown(250);
    } else {
      $this.siblings('.expand').slideUp(250);
    }
  });
  
  $fileProtectedUser = jQuery('.file_protected_user');
  $fileProtectedUser.click(function() {
    $this = jQuery(this);
    $this.parent().siblings('.expand_user').slideToggle(250);
  });
  
  var $isProtectedLink = jQuery('.link_is_protected');
  $isProtectedLink.click(function() {
  $this = jQuery(this);
    if ($this.is(':checked')) {
      $this.siblings('.expand').slideDown(250);
    } else {
      $this.siblings('.expand').slideUp(250);
    }
  });
  
  $linkProtectedUser = jQuery('.link_protected_user');
  $linkProtectedUser.click(function() {
    $this = jQuery(this);
    $this.parent().siblings('.expand_user').slideToggle(250);
  });
  
}

function app_deleteOptionFile(el, pageOptionId, fieldKey) {
  $this = jQuery(el);
  if (confirm("Wollen Sie dieses Element wirklich löschen?")) {
    jQuery.ajax({
      type: 'POST',
      url: $this.attr('href'),
      data: "pageOptionId=" + pageOptionId + "&fieldKey=" + fieldKey,
      success: function(msg){
        $this.parent().fadeOut(250, function(){
          jQuery(this).remove();
        });
      }
    });
  }
  return false;
}

function app_saveForm(el)
{
  $this = jQuery(el);
  $submitButton = jQuery('.buttons input[type="submit"]');
  $valSubmitButton = $submitButton.val();
  $submitButton.replaceWith('<p>' + $valSubmitButton + '...</p>');
  return true;
}

function app_formPage()
{
  $h3 = jQuery('h3.fields');
  $h3.click(function() {
    jQuery('#container-' + jQuery(this).attr('id')).slideToggle(250);
  });
  
  $h4 = jQuery('.row.widget h4');
  $h4.click(function() {
	  jQuery('#container-WidgetAdmin' + jQuery(this).attr('id')).slideToggle(250);
  });
}
