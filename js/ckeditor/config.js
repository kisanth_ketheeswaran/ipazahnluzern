﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
  
  config.toolbar = 'Basic';
  config.startupOutlineBlocks = true ;
  config.toolbar_Basic = 
    [
     ['Source','-','Bold','Italic','Underline','-','Subscript','Superscript','-',
     'NumberedList','BulletedList','-','-','Paste','PasteText','PasteFromWord','-','Format',
     'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-',
     'Link','Unlink','Anchor','-',
     'Image','Flash','Table']
    ]
    
    config.toolbar_Listing = 
      [
       ['Source','-','Bold','Italic','Underline', '-','NumberedList','BulletedList','-','Link','Unlink', 'ShowBlocks','-','Paste','PasteText','PasteFromWord']
      ]
      
   config.filebrowserBrowseUrl = '/js/kcfinder/browse.php?type=files';
   config.filebrowserImageBrowseUrl = '/js/kcfinder/browse.php?type=images';
   config.filebrowserFlashBrowseUrl = '/js/kcfinder/browse.php?type=flash';
   config.filebrowserUploadUrl = '/js/kcfinder/upload.php?type=files';
   config.filebrowserImageUploadUrl = '/js/kcfinder/upload.php?type=images';
   config.filebrowserFlashUploadUrl = '/js/kcfinder/upload.php?type=flash';
};
