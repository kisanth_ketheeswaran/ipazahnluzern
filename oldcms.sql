-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 31. Jul 2014 um 10:34
-- Server Version: 5.5.37-0ubuntu0.14.04.1
-- PHP-Version: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `oldcms`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cms_widget`
--

CREATE TABLE IF NOT EXISTS `cms_widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `target` varchar(250) NOT NULL,
  `position` int(11) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `cms_widget`
--

INSERT INTO `cms_widget` (`id`, `label`, `name`, `target`, `position`, `status`) VALUES
(3, 'Kontaktformular', 'ContactForm', 'Page', 2, 'active'),
(4, 'Dynamische Datei', 'PhpFile', 'Page', 1, 'active');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cms_widget_contact_form`
--

CREATE TABLE IF NOT EXISTS `cms_widget_contact_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_widget_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Daten für Tabelle `cms_widget_contact_form`
--

INSERT INTO `cms_widget_contact_form` (`id`, `cms_widget_id`, `target_id`, `value`, `position`) VALUES
(1, 3, 1, 0, NULL),
(2, 3, 58, 0, NULL),
(3, 3, 57, 0, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cms_widget_php_file`
--

CREATE TABLE IF NOT EXISTS `cms_widget_php_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_widget_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `value` varchar(250) NOT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Daten für Tabelle `cms_widget_php_file`
--

INSERT INTO `cms_widget_php_file` (`id`, `cms_widget_id`, `target_id`, `value`, `position`) VALUES
(1, 4, 1, '', NULL),
(2, 4, 58, '', NULL),
(3, 4, 57, '', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(250) CHARACTER SET utf8 NOT NULL,
  `answer` text CHARACTER SET utf8,
  `sort_order` int(11) NOT NULL,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL,
  `locale_id` varchar(45) CHARACTER SET utf8 NOT NULL,
  `unit_hash` varchar(250) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `glossary`
--

CREATE TABLE IF NOT EXISTS `glossary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(250) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8,
  `url` varchar(250) NOT NULL,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL,
  `locale_id` varchar(45) CHARACTER SET utf8 NOT NULL,
  `unit_hash` varchar(250) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `job`
--

CREATE TABLE IF NOT EXISTS `job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(250) NOT NULL,
  `file` varchar(250) DEFAULT NULL,
  `description_text` text,
  `date` date NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` varchar(45) NOT NULL,
  `locale_id` varchar(45) NOT NULL,
  `unit_hash` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `locale`
--

CREATE TABLE IF NOT EXISTS `locale` (
  `id` varchar(10) NOT NULL,
  `label` varchar(250) NOT NULL,
  `shortcut` varchar(45) DEFAULT NULL,
  `is_default` tinyint(4) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `locale`
--

INSERT INTO `locale` (`id`, `label`, `shortcut`, `is_default`, `position`, `status`) VALUES
('de', 'Deutsch', 'DE', 1, 1, 'active');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structure_id` int(11) NOT NULL,
  `cms_h1` varchar(250) DEFAULT NULL,
  `cms_content` longtext,
  `cms_content_position` int(11) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `meta_title` text,
  `meta_tags` longtext,
  `layout` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Daten für Tabelle `page`
--

INSERT INTO `page` (`id`, `structure_id`, `cms_h1`, `cms_content`, `cms_content_position`, `meta_keywords`, `meta_description`, `meta_title`, `meta_tags`, `layout`) VALUES
(1, 1, 'Home', '<p>\r\n	Das ist der Text</p>\r\n', 1, 'Home', 'Home', 'Home', '', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `page_option`
--

CREATE TABLE IF NOT EXISTS `page_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emotion_pic` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Daten für Tabelle `page_option`
--

INSERT INTO `page_option` (`id`, `emotion_pic`, `page_id`) VALUES
(1, NULL, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `portfolio`
--

CREATE TABLE IF NOT EXISTS `portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `company` varchar(250) DEFAULT NULL,
  `description` text,
  `reference_topic_id` text,
  `reference_business_id` text,
  `reference_technology_id` text,
  `links` text,
  `sort_order` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  `locale_id` varchar(45) NOT NULL,
  `unit_hash` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `portfolio_business`
--

CREATE TABLE IF NOT EXISTS `portfolio_business` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `locale_id` varchar(45) NOT NULL,
  `unit_hash` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `portfolio_pic`
--

CREATE TABLE IF NOT EXISTS `portfolio_pic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `portfolio_hash` varchar(250) CHARACTER SET utf8 NOT NULL,
  `pic` varchar(250) CHARACTER SET utf8 NOT NULL,
  `label` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `type` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `portfolio_technology`
--

CREATE TABLE IF NOT EXISTS `portfolio_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `locale_id` varchar(45) NOT NULL,
  `unit_hash` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `portfolio_topic`
--

CREATE TABLE IF NOT EXISTS `portfolio_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `locale_id` varchar(45) NOT NULL,
  `unit_hash` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `press`
--

CREATE TABLE IF NOT EXISTS `press` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `summary` text,
  `date` date NOT NULL,
  `logo` varchar(250) DEFAULT NULL,
  `file` varchar(250) DEFAULT NULL,
  `url` text,
  `status` varchar(45) NOT NULL,
  `unit_hash` varchar(250) NOT NULL,
  `locale_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` varchar(45) NOT NULL,
  `label` varchar(250) NOT NULL,
  `position` int(11) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `role`
--

INSERT INTO `role` (`id`, `label`, `position`, `status`) VALUES
('admin', 'Administrator', 2, 'active'),
('member', 'Mitglied', 1, 'active');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `session`
--

INSERT INTO `session` (`id`, `expire`, `data`) VALUES
('havoc7a8u4j3fe6j3uooaguam3', 1406797074, '5f4cd835e39e2e42d3eb3807894634f7role|s:5:"admin";language|s:2:"de";5f4cd835e39e2e42d3eb3807894634f7__id|s:1:"1";5f4cd835e39e2e42d3eb3807894634f7__name|s:18:"info@firegroup.com";5f4cd835e39e2e42d3eb3807894634f7status|s:6:"active";5f4cd835e39e2e42d3eb3807894634f7email|s:18:"info@firegroup.com";5f4cd835e39e2e42d3eb3807894634f7__states|a:3:{s:4:"role";b:1;s:6:"status";b:1;s:5:"email";b:1;}structure_5|s:1:"5";1a43765641d6ab6e99b18e598554ee05role|s:5:"admin";1a43765641d6ab6e99b18e598554ee05__id|s:1:"1";1a43765641d6ab6e99b18e598554ee05__name|s:18:"info@firegroup.com";1a43765641d6ab6e99b18e598554ee05status|s:6:"active";1a43765641d6ab6e99b18e598554ee05email|s:18:"info@firegroup.com";1a43765641d6ab6e99b18e598554ee05__states|a:3:{s:4:"role";b:1;s:6:"status";b:1;s:5:"email";b:1;}structure_6|s:1:"6";piisite|O:9:"Structure":11:{s:19:"\0CActiveRecord\0_new";b:0;s:26:"\0CActiveRecord\0_attributes";a:25:{s:2:"id";s:1:"5";s:4:"type";s:4:"site";s:5:"label";s:7:"website";s:4:"link";s:7:"website";s:7:"in_main";s:1:"0";s:9:"in_header";s:1:"0";s:9:"in_footer";s:1:"0";s:7:"is_home";s:1:"0";s:9:"is_extern";s:1:"0";s:12:"is_protected";s:1:"0";s:10:"is_default";s:1:"1";s:10:"is_landing";s:1:"0";s:14:"protected_role";N;s:13:"link_redirect";s:1:"0";s:10:"point_link";N;s:19:"link_redirect_point";s:0:"";s:6:"target";s:0:"";s:6:"domain";s:1:"*";s:8:"position";s:1:"1";s:6:"layout";s:0:"";s:6:"status";s:6:"active";s:9:"source_id";s:1:"0";s:9:"parent_id";s:1:"0";s:7:"site_id";s:1:"5";s:9:"locale_id";s:2:"de";}s:23:"\0CActiveRecord\0_related";a:0:{}s:17:"\0CActiveRecord\0_c";N;s:18:"\0CActiveRecord\0_pk";s:1:"5";s:21:"\0CActiveRecord\0_alias";s:1:"t";s:15:"\0CModel\0_errors";a:0:{}s:19:"\0CModel\0_validators";N;s:17:"\0CModel\0_scenario";s:6:"update";s:14:"\0CComponent\0_e";N;s:14:"\0CComponent\0_m";N;}piidomain|s:12:"oldcms.fg.ch";');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `summary` text,
  `pic` varchar(250) DEFAULT NULL,
  `link` varchar(250) DEFAULT NULL,
  `type` varchar(45) NOT NULL,
  `slider_group_id` int(11) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `locale_id` varchar(45) NOT NULL,
  `unit_hash` varchar(250) NOT NULL,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `slider_group`
--

CREATE TABLE IF NOT EXISTS `slider_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `slider_group`
--

INSERT INTO `slider_group` (`id`, `name`) VALUES
(1, 'Gruppe 1'),
(3, 'Gruppe 2'),
(4, 'Gruppe 3');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `structure`
--

CREATE TABLE IF NOT EXISTS `structure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `label` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  `in_main` tinyint(1) NOT NULL DEFAULT '1',
  `in_header` tinyint(1) NOT NULL DEFAULT '0',
  `in_footer` tinyint(1) NOT NULL DEFAULT '1',
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `is_extern` tinyint(1) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_default` int(11) NOT NULL DEFAULT '0',
  `is_landing` tinyint(1) NOT NULL DEFAULT '0',
  `protected_role` text,
  `link_redirect` tinyint(1) NOT NULL DEFAULT '0',
  `point_link` varchar(250) DEFAULT NULL,
  `link_redirect_point` varchar(45) NOT NULL DEFAULT 'child',
  `target` varchar(45) NOT NULL DEFAULT '_self',
  `domain` text,
  `position` int(11) NOT NULL,
  `layout` varchar(250) DEFAULT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'active',
  `source_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `site_id` int(11) NOT NULL,
  `locale_id` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Daten für Tabelle `structure`
--

INSERT INTO `structure` (`id`, `type`, `label`, `link`, `in_main`, `in_header`, `in_footer`, `is_home`, `is_extern`, `is_protected`, `is_default`, `is_landing`, `protected_role`, `link_redirect`, `point_link`, `link_redirect_point`, `target`, `domain`, `position`, `layout`, `status`, `source_id`, `parent_id`, `site_id`, `locale_id`) VALUES
(1, 'nav', 'Home', 'ueber-uns', 1, 0, 0, 1, 0, 0, 0, 0, '', 0, '', 'child', '_self', NULL, 1, NULL, 'active', 0, 5, 5, 'de'),
(5, 'site', 'website', 'website', 0, 0, 0, 0, 0, 0, 1, 0, NULL, 0, NULL, '', '', '*', 1, '', 'active', 0, 0, 5, 'de');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `email` varchar(250) CHARACTER SET utf8 NOT NULL,
  `password` varchar(250) CHARACTER SET utf8 NOT NULL,
  `role` varchar(45) CHARACTER SET utf8 NOT NULL,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `email`, `password`, `role`, `status`) VALUES
(1, 'Firegroup', 'GmbH', 'info@firegroup.com', '8b2070b8e11827bd2a3d09be85214475', 'admin', 'active');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
