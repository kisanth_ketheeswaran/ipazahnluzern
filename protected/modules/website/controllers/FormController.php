<?php

class FormController extends WebsiteController {

    public function sendContact($model) {
        $contactForm = $model;
        $emailText = '';
        //$emailText .= Yii::t('app', 'Vorname') . ': ' . $contactForm['first_name'] . "\n";
        $emailText .= Yii::t('app', 'Nachname') . ': ' . $contactForm->last_name . "\n";
        //$emailText .= Yii::t('app', 'Firma') . ': ' . $contactForm['company'] . "\n";
        $emailText .= Yii::t('app', 'Telefon') . ': ' . $contactForm->phone . "\n";
        //$emailText .= Yii::t('app', 'Erreichbar') . ': ' . $contactForm['accessible'] . "\n";
        $emailText .= Yii::t('app', 'E-Mail') . ': ' . $contactForm->email . "\n\n\n";
        $emailText .= $contactForm->message. "\n";

        $extra = "From: " . utf8_decode($contactForm->last_name) . " <" . $contactForm->email . ">\n";
        $extra .= "Content-Type: text/plain; charset=UTF-8\n";
        $extra .= "Content-Transfer-Encoding: 8bit\n";

        mail(Yii::app()->params['adminEmail'], 'Kontaktformular - Site Name', $emailText, $extra);
        return 'suc';
    }

    public function actionAjaxContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            $valid = $model->validate();
            if ($valid) {
                //do anything here
                //echo json_encode('suc');
                $lol = $this->sendContact($model);
                echo json_encode($lol);
                Yii::app()->end();
            } else {
                $error = $model->validate();
                $error = $model->errors;
                if (!empty($error)){
                    $error = $this->changeArrayKeys($error);
                    echo CJSON::encode($error);
                }
                Yii::app()->end();
            }
        }
    }
    
    public function changeArrayKeys($array){
        $indArray = array_values($array);
        return $indArray;
    }
}
