<?php

class TerminController extends WebsiteController {

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else {
                $this->h1 = Yii::t('app', 'Error') . ' ' . $code;
                $this->render('error', $error);
            }
        }
    }

    public function actionPruefTermin() {
        $model = new Buchung;
        if (isset($_POST['Buchung'])) {
            $url = Yii::app()->user->returnUrl;
            $url2 = Yii::app()->request;
            $terminplanid = $_POST['Terminplan']['id'];
            $behandlungen = $_POST['Behandlungsart'];
            $model->attributes = $_POST['Buchung'];
            $model->terminplan_idfs = $terminplanid;
            $model->buchungsdatum = date("d-m-Y");
            $model->status = "offen";
            if ($model->save()) {
                foreach ($behandlungen as $behandlung) {
                    $buchungBehandlung = new Buchungbehandlung;
                    $buchungBehandlung->behandlungsart_idfs = $behandlung;
                    $buchungBehandlung->buchung_idfs = $model->id;
                    $buchungBehandlung->save();
                }
                $gesendet = $this->sendeMailAnBehandler($model->id);
                $this->sendeMailAnPatient($model->id);
                if ($gesendet) {
                    echo json_encode("suc");
                } else {
                    echo json_encode($gesendet);
                }
            }
        }
    }

    /* Kisanth Ketheeswaran
     * Diese Funktion bekommt den Terminplan-Id und holt die Arbeitszeit, in dem 
     * es das Datum und den Terminplan-Id der Funktion holZeiten(),
     * welches im Model Buchung, weitergibt und diese auch ausführt,
     * als Rückgabewert bekommt es ein Array aus Arbeitszeiten.
     * Diese werden dann per ajax als Radiobuttons auf der Webseite angezeigt.
     */

    public function actionholtHeutigeZeitSlots() {
        $terminplanId = $_POST['id'];
        $zeiten = array();
        $zeiten = Buchung::holZeiten(date('d-m-Y'), $terminplanId);
        $i = 0;
        if (empty($zeiten)) {
            echo 'Nix';
        } elseif ($zeiten[0] == 'nichts') {
            echo 'Nix';
        } else {
            foreach ($zeiten as $schluessel => $zeit) {
                echo '<div class="boxen">' . CHtml::tag('input', array('value' => $schluessel,
                    'type' => 'radio', 'id' => 'Buchung_zeit', 'name' => 'Buchung[zeit]'), '<label>' . CHtml::encode($zeit) . '</label>'
                        , true) . '</div>';
            }
        }
    }

    /* Kisanth Ketheeswaran
     * Diese Funktion bekommt den Terminplan-Id und holt die Arbeitszeit, in dem 
     * es das Datum und den Terminplan-Id der Funktion holZeiten(),
     * welches im Model Buchung, weitergibt und diese auch ausführt,
     * als Rückgabewert bekommt es ein Array aus Arbeitszeiten.
     * Diese werden dann per ajax als Radiobuttons auf der Webseite angezeigt.
     */

    public function actionHolZeitSlots() {
        $terminplanId = $_POST['id'];
        $zeiten = array();
        $datum = $_POST['datum'];
        $zeiten = Buchung::holZeiten($datum, $terminplanId);
        $i = 0;
        if (empty($zeiten)) {
            echo 'Nix';
        } elseif ($zeiten[0] == 'nichts') {
            echo 'Nix';
        } else {
            foreach ($zeiten as $schluessel => $zeit) {
                echo '<div class="boxen">' . CHtml::tag('input', array('value' => $schluessel,
                    'type' => 'radio', 'id' => 'Buchung_zeit', 'name' => 'Buchung[zeit]'), '<label>' . CHtml::encode($zeit) . '</label>'
                        , true) . '</div>';
            }
        }
    }

    public function actionHolNichtBuchbareTermine() {
        $terminplanId = $_POST['id'];
        $daten = array();
        $daten = Buchung::holNichtBuchbareTermine(date('d-m-Y'), $terminplanId);
        echo json_encode($daten);
    }

    /* Kisanth Ketheeswaran
     * Diese Funktion holt je nach Terminplaner, seine Behandlungen.
     * Dies werden dann per ajax als Checkboxen auf der Webseite angezeigen.
     */

    public function actionholtBehandlungen() {
        $behandlerId = $_POST['id'];
        $behandlungen = Behandlungsart::model()->findAll('terminplan_idfs=:id', array(':id' => (int) $behandlerId));
        $list = CHtml::listData($behandlungen, 'id', 'behandlung');
        foreach ($behandlungen as $behandlung) {
            echo '<div class="boxen">' . CHtml::tag('input', array('value' => $behandlung->id,
                'type' => 'checkbox', 'id' => 'Behandlungsart_id' . $behandlung->id
                , 'name' => 'Behandlungsart[id' . $behandlung->id . ']'), '<label>' . CHtml::encode($behandlung->behandlung) . '</label>'
                    , true);
            if (!empty($behandlung->bemerkung)) {
                echo '<div class="bemerkung">
                            <div class="bemerkungCont">' .
                CHtml::encode($behandlung->bemerkung)
                . '</div>
                            <div class="tooltipZeiger"></div>
                    </div></div><div class="clearBoth"></div>';
            } else {
                echo '</div><div class="clearBoth"></div>';
            }
        }
    }

    public function actionAjaxContact() {
        $model = new Buchung;
        if (isset($_POST['Buchung'])) {
            $model->attributes = $_POST['Buchung'];
            $valid = $model->validate();
            if ($valid) {
                //do anything here
                echo json_encode('suc');
                //$lol = $this->sendContact($model);
                //echo json_encode($lol);
                Yii::app()->end();
            } else {
                $error = $model->validate();
                $error = $model->errors;
                if (!empty($error)) {
                    $error = $this->changeArrayKeys($error);
                    echo CJSON::encode($error);
                }
                Yii::app()->end();
            }
        }
    }

    public function changeArrayKeys($array) {
        $indArray = array_values($array);
        $newArray = array();
        foreach ($array as $schluessel => $wert) {
            $newArray[$schluessel] = $array[$schluessel][0];
        }
        return $newArray;
    }

    public function sendeMailAnBehandler($buchungId) {
        require_once Yii::app()->basePath . '/extensions/PHPMailer/class.phpmailer.php';
        $mail = new PHPMailer;
        $buchung = Buchung::model()->with('terminplanIdfs', 'behandlungsart')->findByPk($buchungId);

        $mail->From = 'k.waran26@gmail.com';
        $mail->FromName = 'Kisasnth Ketheeswaran';
        $mail->addAddress('kk@firegroup.com');
        //$mail->addAddress('m.schnueriger@firegroup.com');   // Add a recipient
        $mail->Subject = 'Terminanfrage von ' . $buchung->vorname . " " . $buchung->nachname;
        $mail->Body = "Eine neue Buchung kam herein, von" . $buchung->vorname . " " . $buchung->nachname . ".";
        if (!$mail->send()) {
            return $mail->ErrorInfo;
        } else {
            return true;
        }
    }

    public function sendeMailAnPatient($buchungId) {
        require_once Yii::app()->basePath . '/extensions/PHPMailer/class.phpmailer.php';
        $mail = new PHPMailer;
        $buchung = Buchung::model()->with('terminplanIdfs', 'behandlungsart')->findByPk($buchungId);

        $mail->From = $buchung->terminplanIdfs->email;
        $mail->FromName = $buchung->terminplanIdfs->full_name;
        $mail->addAddress($buchung->email);
        //$mail->addAddress('m.schnueriger@firegroup.com');   // Add a recipient

        $mail->Subject = 'Terminanfrage von ' . $buchung->vorname . " " . $buchung->nachname;
        $mail->Body = "Gewünschte Behandlerr: " . $buchung->terminplanIdfs->full_name;
        $mail->Body .= " \n Gewünschte Behandlung: ";
        foreach ($buchung->behandlungsart as $behandlung) {
            $mail->Body .= $behandlung->behandlung . ", ";
        }

        $mail->Body .=" \n Gewünschter Termin: " . $buchung->datum . " \n " .
                "Persönliche Daten: " . $buchung->anrede . " " . $buchung->vorname . " " . $buchung->nachname . " ";
        if (isset($buchung->geburtsdatum)) {
            $mail->Body .= $buchung->geburtsdatum;
        }
        $mail->Body .= $buchung->strasse . ", " . $buchung->plz . " " . $buchung->ort . ", Tel: " . $buchung->telefon . "," .
                $buchung->email;
        $mail->Body .= " \n Bemerkung: " . $buchung->bemerkung ;
        $mail->Body .=$buchung->terminerinnerung == 1 ? " \n Terminerinnerung: Ja" : " \n Terminerinnerung: Nein";
        $mail->Body .=$buchung->bestatigung == 1 ? " \n Bestätigungamail: Ja" : " \n Bestätigungamail: Nein";


        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        if (!$mail->send()) {
            return $mail->ErrorInfo;
        } else {
            return true;
        }
    }

}
