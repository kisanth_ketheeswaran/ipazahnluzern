<?php

class IndexController extends WebsiteController {

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else {
                $this->h1 = Yii::t('app', 'Error') . ' ' . $code;
                $this->render('error', $error);
            }
        }
    }

    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                if (Yii::app()->user->role == 'admin') {
                    setcookie('isAdminUser', 1, time() + 3600);
                    $this->redirect(array('/admin/structure/index'));
                } elseif (Yii::app()->user->role == 'behandler') {
                    setcookie('isAdminUser', 1, time() + 3600);
                    $this->redirect(array('/admin/terminplan/index'));
                } else {
                    $this->redirect('/');
                }
            }
        }

        $this->h1 = Yii::t('app', 'Anmeldung');

        // display the login form
        $this->render('login', array('model' => $model));
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionCms() {
        $this->cNav = Structure::getCurrentNavigation();
        $this->cSite = $_SESSION['piisite'];

        if (empty($this->cNav)) {
            throw new CHttpException(404, Yii::t('app', 'Seite konnte nicht gefunden werden.'));
        }
        $this->cPage = $this->cNav->page;
        $this->h1 = $this->cPage->cms_h1;
        if (empty($this->cPage->layout)) {
            if (!empty($this->cSite->layout)) {
                $this->layout = '//layouts/' . $this->cSite->layout;
            }
        } else {
            $this->layout = '//layouts/' . $this->cPage->layout;
        }


        $this->browserTitle = $this->cPage->meta_title;
        $this->render('cms');
    }

    public function actionReqTest01() {
        echo CHtml::tag('input',array('type'=>'radio'),false,true);
        Yii::app()->end();
    }

    public function actionSearch($sucher) {
        $searchParts = explode(' ', $sucher);
        $page = new Page();
        $faq = new Faq();
        $dataProvider = $page->searchDataProvider($searchParts);
        $faqProvider = $faq->searchDataProvider($searchParts);

        $anzFaqs = $faqProvider->totalItemCount;
        $anzS = $dataProvider->totalItemCount;
        $this->h1 = ($dataProvider->totalItemCount + $faqProvider->totalItemCount) == 1 ? Yii::t('site', '1 Ergebnis') : ($dataProvider->totalItemCount + $faqProvider->totalItemCount) . ' ' . Yii::t('site', 'Ergebnisse');
        $records = array();
        for ($i = 0; $i < $dataProvider->totalItemCount; $i++) {
            $data = $dataProvider->data[$i];
            array_push($records, $data);
        }
        for ($i = 0; $i < $faqProvider->totalItemCount; $i++) {
            $data = $faqProvider->data[$i];
            array_push($records, $data);
        }

        $provAll = new CArrayDataProvider($records, array(
            'pagination' => array('pageSize' => 5) //optional add a pagination
                )
        );

        $this->render('search', array(
            'page' => $page,
            'searchParts' => $searchParts,
            'provAll' => $provAll,
                //'dataProvider' => $dataProvider,
                //'faqProvider'=>$faqProvider
        ));
    }

    public function actionGallery($id) {
        $this->h1 = "Galerie";
        $allElements = GalleryElement::model()->findAll('gallery_id = :id AND locale_id = :locale ORDER BY position', array(':id' => $id, ':locale' => Yii::app()->language));
        $this->render('galleryView', array('allElements' => $allElements));
    }

}
