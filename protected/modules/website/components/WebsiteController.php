<?php

class WebsiteController extends Controller
{
  public $layout='//layouts/col-none';
  public $defaultAction = 'cms';
  public $h1;
  public $cNav;
  public $cPage;
  public $cSite;
  public $browserTitle;
}