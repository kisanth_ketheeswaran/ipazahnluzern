<?php

class WidgetGallery extends CWidget {

    public $galerie;
    
    
    public function init() {
        $this->galerie = Gallery::model()->findAll('status = "active" AND locale_id = :locale', array(':locale' => Yii::app()->language));
    }

    public function run() {
        if (!empty($this->galerie))
            $this->render('gallery');
    }

}
