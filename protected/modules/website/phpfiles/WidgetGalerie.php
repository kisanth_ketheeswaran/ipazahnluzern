<?php

class WidgetGalerie extends CWidget {

    public $galerie;
    
    
    public function init() {
        $this->galerie = Galerie::model()->findAll('status = "active" AND locale_id = :locale ORDER BY position', array(':locale' => Yii::app()->language));
    }

    public function run() {
        if (!empty($this->galerie))
            $this->render('galerie');
    }

}
