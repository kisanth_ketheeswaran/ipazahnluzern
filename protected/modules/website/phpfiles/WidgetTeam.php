<?php

class WidgetTeam extends CWidget {

    public $teams;
    
    
    public function init() {
        $this->teams = Team::model()->findAll('status = "active" AND locale_id = :locale ORDER BY position', array(':locale' => Yii::app()->language));
    }

    public function run() {
        if (!empty($this->teams))
            $this->render('team');
    }

}
