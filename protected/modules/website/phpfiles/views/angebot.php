<?
$angebote = $this->angebote;
$anzAng = count($angebote);
$i = 1;
if (!empty($angebote)):
    ?>
    <div class="row irow" id="angebotCont">
        <? 
        $i=1;
        foreach ($angebote as $angebot):
            ?>
            <div class="col-sm-4 angebotBox">
                <img src="/images/media/angebot/<? echo $angebot->bild ?>" alt="" class="img-icon">
                <h4><? echo $angebot->titel ?></h4>
                <p><? echo $angebot->text ?></p>
            </div>
        <? if($i==3):
            echo '<div class="clearBoth"></div>';
        endif; ?>
            <?
            $i++;
        endforeach;
        ?>
    </div>
    <hr>
<? endif; ?>
