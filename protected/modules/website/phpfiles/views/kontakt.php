<?
$kontakt = $this->kontakt;
?>
<div id="kontaktWidget">
<div class="row">
    <!-- MAP -->
    <div class="google-map">
        <div class="google-map-container" data-markers="47.048830, 8.305928" data-center="47.048830, 8.305928" data-zoom="14"></div>
        <a href="#google-map-popup" class="a-map fa fa-search" data-toggle="modal"></a>
    </div>
    <!-- /.google-map -->
</div>

<div class="row irow">

    <? if (!empty($kontakt->titel1) && !empty($kontakt->anfuehrungstext1)): ?>
        <div class="col-sm-4">
            <h4><? echo $kontakt->titel1; ?></h4>
            <address><? echo $kontakt->anfuehrungstext1; ?></address>
        </div>
    <? endif; ?>

    <? if (!empty($kontakt->titel2) && !empty($kontakt->telefon) && !empty($kontakt->fax) && !empty($kontakt->email)): ?>
        <div class="col-sm-4">
            <h4><? echo $kontakt->titel2 ?></h4>
            <dl class="dl-horizontal">
                <dt>Telefon:</dt>
                <dd>
                    <p><? echo $kontakt->telefon ?></p>
                </dd>
                <dt>Fax:</dt>
                <dd>
                    <p><? echo $kontakt->fax ?></p>
                </dd>
                <dt>Email:</dt>
                <dd>
                    <p>
                        <a href="mailto:<? echo $kontakt->email ?>"><? echo $kontakt->email ?></a>
                    </p>
                </dd>
            </dl>
        </div>
    <? endif; ?>

    <? if (!empty($kontakt->titel3) && !empty($kontakt->anfuehrungstext3)): ?>
        <div class="col-sm-4">
            <h4><? echo $kontakt->titel3 ?></h4>
            <p><? echo $kontakt->anfuehrungstext3 ?></p>
        </div>
    <? endif; ?>

</div>

<hr/>


<div class="row irow">
    <div class="col-sm-5">
        <h3><? echo $kontakt->titel; ?><small>&mdash;</small></h3>
        <p><? echo $kontakt->text; ?></p>
    </div>
    <div class="col-sm-7">
        <form action="../../php/email.php" method="post" id="send-form" class="send-form" novalidate>
            <fieldset class="form-wrap">
                <div class="form-group">
                    <input type="text" name="name" id="send-form-name" placeholder="Your Name" required="required">
                </div>
                <div class="form-group">
                    <input type="email" name="email" id="send-form-email" placeholder="Email" required="required">
                </div>
                <div class="form-group">
                    <textarea name="message" placeholder="Message" id="send-form-message" required="required"></textarea>
                </div>
            </fieldset>
            <button type="submit" class="btn btn-default">Send</button>
        </form>
    </div>
</div>

<!-- Modal -->
<div class="modal map-modal" id="google-map-popup">
    <a href="#" class="map-close" data-dismiss="modal"></a>
    <div class="google-map-popup"></div>
</div>
<!-- /.modal -->
</div>

<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&#038;sensor=false&#038;ver=1"></script>