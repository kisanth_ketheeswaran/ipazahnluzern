<?
$terminplaene = $this->terminplaene;
$monate = $this->monate;
$behandlungslist = CHtml::listData($terminplaene->behandlungsarts, 'id', 'behandlung');
$list = CHtml::listData($terminplaene, 'id', 'full_name');
?>

<div class="terminanfrageCont">
    <div class="schritteCont">
        <div class="schritte active" id="schritt1"><span class="nummer">1</span> Gewünschter Behandler <div class="grenze"></div></div>
        <div class="schritte" id="schritt2"><span class="nummer">2</span> Gewünschte Behandlung <div class="grenze"></div></div>
        <div class="schritte" id="schritt3"><span class="nummer">3</span> Gewünschter Termin <div class="grenze"></div></div>
        <div class="schritte" id="schritt4"><span class="nummer">4</span> Persönliche Daten <div class="grenze"></div></div>
        <div class="schritte" id="schritt5"><span class="nummer">5</span> Abschluss</div>
    </div>
    <div class="clearBoth"></div>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'terminanfrage-form',
        'action' => '/website/termin/pruefTermin',
        'enableAjaxValidation' => false,
            //'htmlOptions' => array('onSubmit' => 'return app_saveForm(this)', 'enctype' => 'multipart/form-data')
    ));
    ?>
    <div class="anfrageCont">

        <div class="normalCont" id="terminplanCont" style="">
            <h3>Bitte wählen Sie den gewünschten Behandler</h3>
            <?
            $i = 0;
            foreach ($terminplaene as $ter):
                ?>
                <div class="boxen">
                    <?
                    echo $form->radioButton(new Terminplan, 'id', array('uncheckValue' => null, 'value' => $ter->id, 'name' => 'Terminplan[id]', 'id' => 'Terminplan_id_' . $ter->id));
                    ?>
                    <label><? echo $ter->full_name ?></label>
                    <? if (!empty($ter->bemerkung)): ?>
                        <div class="bemerkung">
                            <div class="bemerkungCont">
                                <? echo $ter->bemerkung ?>
                            </div>
                            <div class="tooltipZeiger"></div>
                        </div>
                    <? endif; ?>
                </div>
                <div class="clearBoth"></div>
                <?
                $i++;
            endforeach;
            ?>
            <br>
            <? //echo $form->radioButtonList(new Terminplan, 'id', $list);  ?>
            <div class="errorNormalCont">
                <div class="alert alert-danger fade in errorCont" id="errorBehandler">
                    <i class="fa fa-exclamation-circle"></i> 
                    Bitte wählen Sie einen Behandler aus
                </div>
            </div>
        </div>

        <div class="normalCont" id="behandlungCont" style="display: none;">
            <h3>Bitte wählen Sie die gewünschten Behandlungen</h3>
            <div id="checkBoxBehandlungen">

            </div>
            <div class="errorNormalCont">
                <div class="alert alert-danger fade in errorCont" id="errorBehandlung">
                    <i class="fa fa-exclamation-circle"></i> 
                    Bitte wählen Sie mindestens eine Behandlungs aus
                </div>
            </div>
        </div>

        <div class="normalCont" id="datumCont" style="display: none;">
            <h3>Bitte wählen Sie den für Sie passenden Termin aus</h3>
            <div class="anfrageKalender">
                <div id="datepicker"></div>
            </div>
            <input type="text" name="Buchung[datum]" id="Buchung_datum" style="display: none">
            <p class="freieTerminText">
                Freie Termine für 
                <?
                echo ucfirst(Standardkalender::ubersetztTagEnglischZuDeutsch(date('l'))) . ', ' . date('j') . '.' . $monate[date('F')] . ' ' . date('Y');
                ?>
            </p>
            <div id="datumZeitCont">

            </div>
            <div class="clearBoth"></div>
            <div class="legende">
                <div class="ausgewaehlter"></div>
                <div class="freieTermine"></div>
                <div class="heute"></div>
            </div>
            <div class="errorNormalCont">
                <div class="alert alert-danger fade in errorCont" id="errorZeit">
                    <i class="fa fa-exclamation-circle"></i> 
                    Bitte wählen Sie eine Zeit aus
                </div>
            </div>
        </div>

        <div class="normalCont" id="anfrageFormular" style="display: none;">
            <h3>Bitte füllen Sie Ihre persönlichen Daten ein (* Pflichtfelder)</h3>
            <div class="anfrageFormular">
                <fieldset class="form-wrap linkeFelder">
                    <div class="form-group">
                        <select name="Buchung[anrede]" id="Buchung_anrede">
                            <option value="" disabled selected>Anrede *</option>
                            <option value="herr">Herr</option>
                            <option value="frau">Frau</option>
                        </select>
                    </div>
                    <div class="form-group zweiFelder">

                        <?php echo $form->textField(new Buchung, 'nachname', array('class' => '', 'placeholder' => "Nachname *")); ?>
                        <?php echo $form->error(new Buchung, 'nachname'); ?>
                        <?php echo $form->textField(new Buchung, 'vorname', array('class' => '', 'placeholder' => 'Vorname *')); ?>
                        <?php echo $form->error(new Buchung, 'vorname'); ?>
                    </div>
                    <div class="form-group">

                        <?php echo $form->textField(new Buchung, 'geburtsdatum', array('placeholder' => "Geburtsdatum", 'class' => '')); ?>
                        <?php echo $form->error(new Buchung, 'geburtsdatum'); ?>
                    </div>
                    <div class="form-group">

                        <?php echo $form->textField(new Buchung, 'strasse', array('placeholder' => "Strasse *", 'class' => '')); ?>
                        <?php echo $form->error(new Buchung, 'strasse'); ?>
                    </div>
                    <div class="form-group zweiFelder">

                        <?php echo $form->textField(new Buchung, 'plz', array('placeholder' => "PLZ *", 'class' => '')); ?>
                        <?php echo $form->error(new Buchung, 'plz'); ?>
                        <?php echo $form->textField(new Buchung, 'ort', array('placeholder' => "Ort *", 'class' => '')); ?>
                        <?php echo $form->error(new Buchung, 'ort'); ?>
                    </div>
                    <div class="form-group zweiFelder">

                        <?php echo $form->textField(new Buchung, 'telefon', array('placeholder' => Yii::t('site', 'Telefon *'), 'class' => '')); ?>
                        <?php echo $form->error(new Buchung, 'telefon'); ?>
                        <?php echo $form->textField(new Buchung, 'email', array('class' => '', 'placeholder' => 'E-Mail *')); ?>
                        <?php echo $form->error(new Buchung, 'email'); ?>
                    </div>
                </fieldset>
                <fieldset class="form-wrap rechteFelder">
                    <div class="form-group">

                        <?php echo $form->textArea(new Buchung, 'bemerkung', array('placeholder' => Yii::t('site', 'Bemerkung'), 'class' => '')); ?>
                        <?php echo $form->error(new Buchung, 'bemerkung'); ?>
                    </div>
                    <div class="checkBoxen">
                        <?php echo $form->checkBox(new Buchung, 'terminerinnerung', array('class' => '')); ?>
                        <?php echo $form->labelEx(new Buchung, 'terminerinnerung'); ?>
                        <?php echo $form->error(new Buchung, 'terminerinnerung'); ?>
                        <br>
                        <?php echo $form->checkBox(new Buchung, 'bestatigung', array('class' => '')); ?>
                        <?php echo $form->labelEx(new Buchung, 'bestatigung',array("label"=>"Bestätigungskopie senden")); ?>
                        <?php echo $form->error(new Buchung, 'bestatigung'); ?>
                    </div>
                </fieldset>

            </div>
            <div class="col-sm-5 errorAnfrageCont">

            </div>
        </div>
        <div class="clearBoth"></div>


        <div class="normalCont" id="abschlussCont" style="display: none;">
            <h3>Bitte prüfen Sie Ihre Angaben und klicken Sie dann auf Terminanfrage absenden</h3>
            <div class="abschlussAngaben">
                <div class="angabenCont">
                    <span class="titelAngaben">Gewünschte Behandler</span> <span id="behandler" class="ausgabe"></span>
                </div>
                <div class="angabenCont">
                    <span class="titelAngaben">Gewünschte Behandlung</span> <span id="behandlung" class="ausgabe"></span>
                </div>
                <div class="angabenCont">
                    <span class="titelAngaben">Gewünschte Termin</span> <span id="termine" class="ausgabe"></span>
                </div>
                <div class="angabenCont">
                    <span class="titelAngaben">Persönliche Daten</span> <span id="personal" class="ausgabe"></span>
                </div>
                <div class="angabenCont">
                    <span class="titelAngaben">Bemerkung</span> <span id="bemerkung" class="ausgabe"></span>
                </div>
                <div class="angabenCont">
                    <span class="titelAngaben">Terminerinnerung</span> <span id="terminerinnerung" class="ausgabe"></span>
                </div>
                <div class="angabenCont">
                    <span class="titelAngaben">Bestätigungskopie senden</span> <span id="bestaetigung" class="ausgabe"></span>
                </div>
            </div>
            <div class="clearBoth"></div>
        </div>

        <div class="alert alert-success fade in" id="erfolgreich" style="display: none;">
            <button class="fa fa-close" data-dismiss="alert" type="button"></button>
            <i class="fa fa-check-circle"></i> 
            E-Mail wurde versendet.
        </div>

        <div class="clearBoth"></div>
        <hr>
        <div class="buttonCont">
            <div class="linkCont" id="zuruck">
                <span>zurück</span>
            </div>
            <div class="linkCont" id="weiter">
                <span>weiter</span>
            </div>
            <div class="linkCont" id="senden" style="display: none;">
                <span>Terminanfrage absenden</span>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>

<script>
    var behandler = '';
    $('#weiter').on('click', function() {
        var terminplanDisplay = $('#terminplanCont').css('display');
        var behandlungDisplay = $('#behandlungCont').css('display');
        var personDisplay = $('#anfrageFormular').css('display');
        var datumDisplay = $('#datumCont').css('display');
        if (terminplanDisplay == "block") {
            if (pruefeObBehandlerGewaehlt()) {
                $('#schritt1').removeClass('active');
                $('#schritt2').addClass('active');
                holtCheckBoxListe(behandler);
                $('#behandlungCont').delay(800).fadeIn();
            } else {
                $('#errorBehandler').fadeIn();
            }
        } else if (behandlungDisplay == "block") {
            if (pruefeObBehandlungGewaehlt()) {
                console.log(behandler);
                holNichtBuchbareTermine(behandler);
                holtHeutigeZeitSlots(behandler);
                $('#schritt2').removeClass('active');
                $('#schritt3').addClass('active');
            } else {
                $('#errorBehandlung').fadeIn();
            }
        } else if (datumDisplay == "block") {
            if (pruefeObZeitGewaehlt()) {
                console.log(behandler);
                $('#anfrageFormular').delay(800).fadeIn();
                $('#schritt3').removeClass('active');
                $('#schritt4').addClass('active');
            } else {
                $('#errorZeit').fadeIn();
            }
        }
        else if (personDisplay == "block") {
            send();
        }

    });

    $('#zuruck').on('click', function() {
        var terminplanDisplay = $('#terminplanCont').css('display');
        var behandlungDisplay = $('#behandlungCont').css('display');
        var personDisplay = $('#anfrageFormular').css('display');
        var datumDisplay = $('#datumCont').css('display');
        var abschluss = $('#abschlussCont').css('display');
        if (terminplanDisplay == "block") {

        } else if (behandlungDisplay == "block") {
            $('#terminplanCont').delay(800).fadeIn();
            $('#behandlungCont').fadeOut();
        } else if (datumDisplay == "block") {
            $('#behandlungCont').delay(800).fadeIn();
            $('#datumCont').fadeOut();
        }
        else if (personDisplay == "block") {
            $('#anfrageFormular').fadeOut();
            $('#datumCont').delay(800).fadeIn();

        } else if (abschluss == "block") {
            $('#abschlussCont').fadeOut();
            $("#senden").fadeOut();
            $('#anfrageFormular').delay(800).fadeIn();
            $("#weiter").delay(800).fadeIn();

        }
    });



    function send() {
        var data = $("#terminanfrage-form").serialize();
        jQuery.ajax({
            url: '/website/termin/ajaxContact',
            type: 'post',
            dataType: 'json',
            data: data,
            success: function(output) {
                if (output === 'suc') {
                    abschlussAngaben();
                } else {
                    var errorCont = $('.errorAnfrageCont');
                    var html2 = '<div class="alert alert-danger fade in"> <button class="fa fa-close" data-dismiss="alert" type="button"></button><i class="fa fa-exclamation-circle"></i> <h6>Error message</h6> <ul>';
                    var arraytr = jQuery.makeArray(output);
                    var newArray = arraytr[0];
                    var trial = JSON.stringify(newArray);
                    var trial2 = jQuery.makeArray(trial);
                    var trial3 = jQuery.parseJSON(trial);
                    var result = $.parseJSON(trial2);
                    var shoeArray = [];
                    $.each(result, function(k, v) {
                        var idName = k;
                        var element = $("#Buchung_" + idName);
                        element.css('border', '1px solid red');
                        if (k == "anrede") {
                            $("#Buchung_anrede_chosen").css('border', '1px solid red');
                        }
                        html2 += '<li>' + v + '</li>';
                    });
                    html2 += '</ul></div>';
                    errorCont.html(html2);
                }
            },
            error: function(output) {

            }

        });
    }

    function abschlussAngaben() {
        holtBehandlungen();
        holtBehandler();
        holtZeitDatum();
        holtPersonal();
        $("#anfrageFormular").fadeOut();
        $("#weiter").fadeOut();
        $('#abschlussCont').delay(800).fadeIn();
        $("#senden").delay(800).fadeIn();
    }

    var arrayDatum;
    function holNichtBuchbareTermine(terminid) {
        jQuery.ajax({
            'url': '/website/termin/holNichtBuchbareTermine',
            'cache': false,
            'type': 'post',
            'data': {id: terminid},
            'success': function(html) {
                arrayDatum = jQuery.parseJSON(html);
                console.log(arrayDatum);
                zeigeDatepicker();
            }
        });
    }

    $('#senden').on('click', function() {
        var data = $("#terminanfrage-form").serialize();
        jQuery.ajax({
            'url': '/website/termin/pruefTermin',
            'cache': false,
            dataType: 'json',
            'type': 'post',
            'data': data,
            'success': function(output) {
                if (output === 'suc') {
                    $("#erfolgreich").fadeIn();
                    $(".buttonCont").hide();
                } else {
                    $("#erfolgreich").html(output);
                }
            }
        });
    });

    function holtHeutigeZeitSlots(terminid) {
        jQuery.ajax({
            'url': '/website/termin/holtHeutigeZeitSlots',
            'cache': false,
            'type': 'post',
            'data': {id: terminid},
            'success': function(html) {
                jQuery("#datumZeitCont").html(html);
            }
        });
    }

    function zeigeDatepicker() {
        console.log("ist in datePicker");
        $.datepicker.regional['de'] = {clearText: 'löschen', clearStatus: 'aktuelles Datum löschen',
            closeText: 'schließen', closeStatus: 'ohne Änderungen schließen',
            prevText: 'zuruck', prevStatus: 'letzten Monat zeigen',
            nextText: 'Vor', nextStatus: 'nächsten Monat zeigen',
            currentText: 'heute', currentStatus: '',
            monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni',
                'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
            monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun',
                'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
            monthStatus: 'anderen Monat anzeigen', yearStatus: 'anderes Jahr anzeigen',
            weekHeader: 'Wo', weekStatus: 'Woche des Monats',
            dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
            dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
            dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
            dayStatus: 'Setze DD als ersten Wochentag', dateStatus: 'Wähle D, M d',
            dateFormat: 'dd.mm.yy', firstDay: 1,
            initStatus: 'Wähle ein Datum', isRTL: false};
        $.datepicker.setDefaults($.datepicker.regional['de']);
        jQuery('#datepicker').datepicker({
            minDate: '0d',
            maxDate: '+6m;',
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            onSelect: holZeitSlots,
            beforeShowDay: function(date) {
                var string = jQuery.datepicker.formatDate('dd-mm-yy', date);
                return [arrayDatum.indexOf(string) == -1]
            }
        });
        $('#datumCont').fadeIn();
    }

    function holZeitSlots(ausgewaehltesDatum) {
        jQuery.ajax({
            'url': '/website/termin/holZeitSlots',
            'cache': false,
            'type': 'post',
            'data': {id: behandler, datum: ausgewaehltesDatum},
            'success': function(html) {
                jQuery("#datumZeitCont").html(html);
            }
        });

    }

    function holtCheckBoxListe(behandler) {
        jQuery.ajax({
            'url': '/website/termin/holtBehandlungen',
            'cache': false,
            'type': 'post',
            'data': {id: behandler},
            'success': function(html) {
                jQuery("#checkBoxBehandlungen").html(html);
            }
        });
    }

    function pruefeObBehandlerGewaehlt() {
        var valid = false
        $('#terminplanCont').find('input:radio').each(function() {
            if ($(this).is(':checked')) {
                behandler = $(this).attr('value');
                behandlerName = $(this).val();
                valid = true;
                $('#terminplanCont').fadeOut();
                return valid;
            }
        });
        return valid;
    }

    function pruefeObZeitGewaehlt() {
        var valid = false
        $('#datumZeitCont').find('input:radio').each(function() {
            if ($(this).is(':checked')) {
                valid = true;
                $('#datumCont').fadeOut();
                return valid;
            }
        });
        return valid;
    }

    function pruefeObBehandlungGewaehlt() {
        var valid = false
        var i = 0;
        $('#behandlungCont').find('input:checkbox').each(function() {
            if ($(this).is(':checked')) {
                valid = true;
                $('#behandlungCont').fadeOut();
                return valid;
            }
        });
        return valid;
    }


    function holtBehandlungen() {
        var behandlungen = [];
        var i = 0;
        var text = ""
        $('#behandlungCont').find('input:checkbox').each(function() {
            if ($(this).is(':checked')) {
                behandlungen[i] = $(this).siblings('label').text();
                console.log("Behandlung " + behandlungen[i]);
                if (behandlungen[i + 1] != "" || behandlungen[i + 1] != null) {
                    text += behandlungen[i] + ", ";
                } else {
                    text += behandlungen[i];
                }
                i++;
            }
        });
        $("#behandlung").html(text);

    }


    function holtBehandler() {
        var behandlerName = "";
        $('#terminplanCont').find('input:radio').each(function() {
            if ($(this).is(':checked')) {
                behandlerName = $(this).siblings('label').text();
            }
        });
        $("#behandler").html(behandlerName);
    }


    function holtZeitDatum() {
        var zeit = "";
        var monat;
        var jahr;
        var gewaehlterTag;
        $('#datumZeitCont').find('input:radio').each(function() {
            if ($(this).is(':checked')) {
                zeit = $(this).siblings('label').text();
                monat = $('td.ui-datepicker-current-day').attr('data-month');
                monat = ('0' + (parseInt(monat) + 1)).slice(-2);
                jahr = $('td.ui-datepicker-current-day').attr('data-year');
                gewaehlterTag = $('td.ui-datepicker-current-day').find('a').text();
                gewaehlterTag = ('0' + (parseInt(gewaehlterTag) + 1)).slice(-2);
            }
        });
        $("#termine").html(gewaehlterTag + "." + monat + "." + jahr);
        $("#Buchung_datum").val(gewaehlterTag + "-" + monat + "-" + jahr);

    }


    function holtPersonal() {
        var terminErinnerung = "";
        var bestaetigung = "";
        var bemerkung = "";
        var personalText = "";
        var anrede = "";
        var nachname = "";
        var vorname = "";
        var geburtstag = "";
        var strasse = "";
        var plz = "";
        var ort = "";
        var telefon = "Tel ";
        var mail = "";
        anrede = $("#Buchung_anrede option:selected").text();
        nachname = $("#Buchung_nachname").val();
        vorname = $("#Buchung_vorname").val();
        geburtstag = $("#Buchung_geburtsdatum").val();
        strasse = $("#Buchung_strasse").val();
        plz = $("#Buchung_plz").val();
        ort = $("#Buchung_ort").val();
        telefon += $("#Buchung_telefon").val();
        mail = $("#Buchung_email").val();
        bemerkung = $("#Buchung_bemerkung").val();
        if ($("#Buchung_terminerinnerung").is(":checked")) {
            terminErinnerung = "Ja";
        } else {
            terminErinnerung = "Nein";
        }
        if ($("#Buchung_bestatigung").is(":checked")) {
            bestaetigung = "Ja";
        } else {
            bestaetigung = "Nein";
        }
        personalText = "" + anrede + " " + vorname + " " + nachname + " " + geburtstag + ", " + strasse + ", " + plz + " " + ort + ", " + telefon + ", " + mail;
        $("#personal").html(personalText);
        $("#bemerkung").html(bemerkung);
        $("#terminerinnerung").html(terminErinnerung);
        $("#bestaetigung").html(bestaetigung);

    }

</script>