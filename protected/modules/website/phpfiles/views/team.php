<?
$teams = $this->teams;
?>
<div class="row team">
    <? foreach ($teams as $team): ?>
        <div class="col-sm-4 col-xs-6">
            <img src="/images/media/team/<? echo $team->bild ?>" alt="">
            <a class="mask" href="#person-<?echo $team->id ?>" data-toggle="modal">
                <h4>
                    <? echo $team->vorname . ' ' . $team->nachname ?>
                    <cite><? echo $team->funktion ?></cite>
                </h4>
            </a>
        </div>
    <? endforeach; ?>
</div>

<? foreach ($teams as $per): ?>
<div class="modal person-modal" id="person-<? echo $per->id ?>">
    <a href="" class="close" data-dismiss="modal"></a>
    <div class="divtable">
        <div class="">
            <article class="container text-center">
                <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                    <img src="/images/media/team/<? echo $per->bild ?>" alt="">
                    <h4>
                        <? echo $per->vorname . ' ' . $per->nachname ?>
                        <cite><? echo $per->funktion ?></cite>
                    </h4>
                    <div class="teamText"><? echo $per->text ?></div>
                    
                    <!--<nav>
                        <a href="">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </nav>-->
                </div>
            </article>
        </div>
    </div>
</div>
<? endforeach; ?>