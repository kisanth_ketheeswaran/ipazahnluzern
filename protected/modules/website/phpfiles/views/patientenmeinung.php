<?
$bilder = $this->bilder;
if (!empty($bilder)):
    ?>
    <div class="row irow" id="patientCont">
        <?
        $i = 1;
        foreach ($bilder as $bild):
            ?>
            <div class="col-md-3 col-xs-6" id="patientBox">
                <a class="patient thumbnail" rel="group1" href="/images/media/patientenmeinung/<? echo $bild->bild ?>">
                    <img src="/images/media/patientenmeinung/<? echo $bild->bild ?>" />
                </a>
            </div>
            <?
            if ($i == 4):
                echo '<div class="clearBoth"></div>';
            endif;
            ?>
        <?
        $i++;
    endforeach;
    ?>
    </div>
    <HR>
<? endif; ?>

<script>
    $(document).ready(function() {
        $("a.patient").fancybox({
            openEffect: 'none',
            closeEffect: 'none'
        });
    });
</script>