<?
$kunden = $this->kunden;
?>
<? if(!empty($kunden)):?>
<div class="row slider oneslider quotes irow kunde-slider">
    <ul data-auto="true">
        <? foreach ($kunden as $kunde): ?>
            <li>
                <div class="col-sm-3 text-center col-sm-offset-1">
                    <img src="/images/media/kunde/<? echo $kunde->bild ?>" class="img-circle img-sm" alt="">
                </div>
                <div class="col-sm-7 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <blockquote>
                        <p>“<? echo $kunde->zitat ?>”</p>
                    </blockquote>
                    <h5><? echo $kunde->vorname.' '.$kunde->nachname ?><cite><? echo $kunde->firma ?></cite></h5>
                </div>
            </li>
        <? endforeach; ?>
    </ul>
    <a href="" class="prev">&larr;</a>
    <a href="" class="next">&rarr;</a>
</div>
<hr/>
<? endif; ?>
