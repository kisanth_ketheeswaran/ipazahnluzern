<?
$galerie = $this->galerie;
if (!empty($galerie)):
    ?>
    <div class="slider carousel">
        <ul>
            <?
            foreach ($galerie as $gal):
                ?>
                <li class="col-sm-4 col-xs-6">
                    <img src="/images/media/galerie/<? echo $gal->bild ?>" alt="">
                    <a href="#gal-<? echo $gal->id ?>" data-toggle="modal" class="mask">
                        <h4>
                            <? echo $gal->titel; ?>
                            <cite><? echo $gal->untertitel; ?></cite>
                        </h4>
                    </a>
                </li>
                <?
            endforeach;
            ?>
        </ul>
        <div class="slider-nav">
            <a href="" class="prev">&larr;</a>
            <nav class="nav-pages"></nav>
            <a href="" class="next">&rarr;</a>
        </div>
    </div>

    <? foreach ($galerie as $per): ?>
        <div class="modal slider-modal" id="gal-<? echo $per->id ?>">
            <a href="" class="close" data-dismiss="modal"></a>
            <div class="divtable">
                <div class="">
                    <article class="container text-center">
                        <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                            <img src="/images/media/galerie/<? echo $per->bild ?>" alt="">
                        </div>
                    </article>
                </div>
            </div>
        </div>
    <? endforeach; ?>
    <?
endif;
?>