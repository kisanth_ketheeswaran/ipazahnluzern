<?
$faqGroup = $this->faqGroup;
$requestParts = explode('/', Yii::app()->request->url);
$category = $requestParts[count($requestParts) - 1];
$activeCat = null;

if (strpos($category, 'category=') !== false) {
    $categoryParts = explode('=', $category);
    $activeCat = FaqGroup::model()->find('unit_hash = :hash AND locale_id = :locale', array(':locale' => Yii::app()->language, ':hash' => $categoryParts[1]));
} else {
    $activeCat = FaqGroup::model()->find('locale_id = :locale order by name', array(':locale' => Yii::app()->language));
}
$faqs = FaqGroup::model()->getFaqs($activeCat);
$lok = Yii::app()->controller;
?>

<div class="row irow" id="faqCont">
    <div class="col-sm-4">
        <ul class="nav nav-tabs tabs-vertical">
            <? foreach ($faqGroup as $group): ?>
                <li class="<? echo $group->unit_hash == $activeCat->unit_hash ? 'active' : '' ?>">
                    <a href="<? echo Yii::app()->controller->cNav->getUrl() . '/category=' . $group->unit_hash ?>">
                        <? echo $group->name ?><span class="badge faqAnz"><? echo FaqGroup::model()->getFaqs($group, Yii::app()->language) ? count(FaqGroup::model()->getFaqs($group, Yii::app()->language)) : '0' ;?></span>
                    </a>
                </li>
            <? endforeach; ?>
        </ul>
    </div>
    <div class="col-sm-8">
        <h3><? echo $activeCat->name ?></h3>
        <hr class="xs-margin"/>
        <? foreach ($faqs as $faq): ?>
            <div class="faq">
                <h5><? echo $faq->question ?></h5>
                <p><? echo $faq->answer ?></p>
            </div>
        <? endforeach; ?>
    </div>
</div>








