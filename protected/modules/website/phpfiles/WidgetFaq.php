<?php

class WidgetFaq extends CWidget {

    public $faq = null;
    public $faqGroup;

    public function init() {
        $this->faq = new Faq();
        $this->faqGroup = FaqGroup::model()->findAll('locale_id = :locale ORDER BY name ASC', array('locale' => Yii::app()->language));
    }

    public function run() {
        $this->render('faq');
    }

}