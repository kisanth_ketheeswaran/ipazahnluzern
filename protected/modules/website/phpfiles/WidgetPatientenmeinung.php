<?php

class WidgetPatientenmeinung extends CWidget {

    public $bilder;

    public function init() {
        $this->bilder = Patientenmeinung::model()->findAll();
    }

    public function run() {
        $this->render('patientenmeinung');
    }

}