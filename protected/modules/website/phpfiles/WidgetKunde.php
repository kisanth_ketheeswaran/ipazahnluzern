<?php

class WidgetKunde extends CWidget {

    public $kunden;
    
    
    public function init() {
        $this->kunden = Kunde::model()->findAll('status = "active" AND locale_id = :locale ORDER BY position', array(':locale' => Yii::app()->language));
    }

    public function run() {
        if (!empty($this->kunden))
            $this->render('kunde');
    }

}
