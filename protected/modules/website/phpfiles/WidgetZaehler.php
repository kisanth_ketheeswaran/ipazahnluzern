
<?php

class WidgetZaehler extends CWidget {

    public $zaehler;

    public function init() {
        $this->zaehler = Zaehler::model()->findAll('locale_id = :locale ORDER BY position,titel ASC', array('locale' => Yii::app()->language));;
    }

    public function run() {
        $this->render('zaehler');
    }

}
