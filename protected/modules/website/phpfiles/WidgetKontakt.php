<?php

class WidgetKontakt extends CWidget {

    public $kontakt;
    
    
    public function init() {
        $this->kontakt = Kontaktseite::model()->find('locale_id = :locale', array(':locale' => Yii::app()->language));
    }

    public function run() {
        if (!empty($this->kontakt))
            $this->render('kontakt');
    }

}
