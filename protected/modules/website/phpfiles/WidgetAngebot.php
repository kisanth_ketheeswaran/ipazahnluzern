<?php

class WidgetAngebot extends CWidget {

    public $angebote;
    
    
    public function init() {
        $this->angebote = Angebot::model()->findAll('status = "active" AND locale_id = :locale ORDER BY position', array(':locale' => Yii::app()->language));
    }

    public function run() {
        if (!empty($this->angebote))
            $this->render('angebot');
    }

}
