<?php

class WidgetTerminanfrage extends CWidget {

    public $terminplaene;
    public $monate = array(
        'January' => 'Januar',
        'February' => 'Februar',
        'March' => 'März',
        'April'=>'April',
        'May' => 'Mai',
        'June' => 'Juni',
        'July' => 'Juli',
        'August'=> 'August',
        'September'=> 'September',
        'October' => 'Oktober',
        'November' => 'November',
        'December' => 'Dezember');

    public function init() {
        $this->terminplaene = Terminplan::model()->with('behandlungsarts')->findAll('t.locale_id = :locale', array(':locale' => Yii::app()->language));
    }

    public function run() {
        $this->render('terminanfrage');
    }

}
