<?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=> $provAll,
        'summaryText' => '',
        'itemView'=>'_searchItem',
        'emptyText' => 'Leider konnten wir keine Einträge zu Ihrer Suchanfrage finden.',
        'pager' => array(
            'header' => '',
            'firstPageLabel' => '',
            'lastPageLabel' => '',
            'nextPageLabel' => '',
            'prevPageLabel' => ''
        )
    ));
    
   /*$this->widget('zii.widgets.CListView', array(
        'dataProvider'=> $faqProvider,
        'summaryText' => '',
        'itemView'=>'_faqSearchItem',
        'emptyText' => 'Leider konnten wir keine Einträge zu Ihrer Suchanfrage finden.',
        'pager' => array(
            'header' => '',
            'firstPageLabel' => '',
            'lastPageLabel' => '',
            'nextPageLabel' => '',
            'prevPageLabel' => ''
        )
    ));*/
    
?>

<script type="text/javascript">
<!--
	jQuery('.searchFoundItem').live('mouseenter', function() {
		var $this = jQuery(this);
		
		//$this.addClass('hover');
	});
	
	jQuery('.searchFoundItem').live('mouseleave', function() {
		var $this = jQuery(this);
		
		//$this.removeClass('hover');
	});
	
	jQuery('.searchFoundItem').live('click', function() {
		var $this = jQuery(this);
		
		window.location.href = $this.find('a.foundLink').attr('href');
	});
//-->
</script>