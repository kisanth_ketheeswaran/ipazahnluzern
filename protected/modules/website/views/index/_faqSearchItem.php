<?
$faqGroup = FaqGroup::model()->findAll('locale_id=:locale', array(':locale' => Yii::app()->language));
;
foreach ($faqGroup as $ga):
    ?>
    <div class="searchFoundItem <?php echo $index % 2 ? 'even' : 'odd'; ?>">
        <div class="searchFoundInnerItem">
            <h3><?php echo $ga->unit_hash==$data->faq_group_id ? $ga->name : '';?></h3>
            <!--<a href="<?php /*echo $data->structure->getUrl(); ?>" title="" class="foundLink"><?php echo Yii::app()->request->hostInfo . $data->structure->getUrl(); */?></a>-->

            <?php if ($data->question) : ?>
                <div class="foundContent"><?php echo App::cutText(strip_tags($data->question), 230); ?></div>
            <?php endif; ?>
            <div class="clear"></div>
            <a href="<?php echo '/'.Yii::app()->language.'/faq.html/category='.$ga->unit_hash; ?>" title="" class="foundMoreLink"><?php echo Yii::t('site', 'mehr'); ?></a>
            <div class="clear"></div>
        </div>
    </div>
<? endforeach; ?>
