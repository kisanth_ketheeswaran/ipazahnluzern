<div class="form website">
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'login-form',
  'enableAjaxValidation'=>true,
)); ?>

  <div class="row">
    <?php echo $form->labelEx($model,'username'); ?>
    <?php echo $form->textField($model,'username'); ?>
    <?php echo $form->error($model,'username'); ?>
  </div>

  <div class="row">
    <?php echo $form->labelEx($model,'password'); ?>
    <?php echo $form->passwordField($model,'password'); ?>
    <?php echo $form->error($model,'password'); ?>
  </div>

  <div class="row rememberMe checkbox">
    <?php echo $form->checkBox($model,'rememberMe'); ?>
    <?php echo $form->label($model,'rememberMe'); ?>
    <?php echo $form->error($model,'rememberMe'); ?>
  </div>

  <div class="row buttons">
      <label for="">&nbsp;</label>
    <?php echo CHtml::submitButton('Login'); ?>
  </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
