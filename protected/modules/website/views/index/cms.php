<?php if (App::hasAccess('admin')) : ?>
<p class="editPage"><?php echo CHtml::link(Yii::t('app', 'Seite bearbeiten'), array('/admin/structure/index/updatePage', 'id' => $this->cPage->id, 'locale' => Yii::app()->language), array('class' => 'edit')); ?></p>
<?php endif; ?>


<?php
    $widgetElements = array(); 
    foreach ($this->cPage->getWidgets() as $cmsWidget)
    {
    	  ob_start();
    	  $this->widget('application.modules.website.dynamic.WidgetWebsite' . $cmsWidget->name, array('target' => $this->cPage, 'localeId' => $this->cPage->structure->locale_id));
    	  $widgetContent = ob_get_contents();
    	  ob_end_clean();
    	  $cmsWidgetModelString = 'CmsWidget' . $cmsWidget->name;
    	  $cmsWidgetModel = new $cmsWidgetModelString();
    	  $cmsWidgetObject = $cmsWidgetModel->getObject($this->cPage);
    	  
    	  if (!empty($widgetContent)) {
    	  	if(is_object($cmsWidgetObject))
            	$widgetElements[$cmsWidgetObject->position] = $widgetContent;
			else 
				$widgetElements[0] = $widgetContent;
			
    	  }
    }
    /*$widgetElements[$this->cPage->cms_content_position] =  $this->cPage->cms_content;*/
    ksort($widgetElements);
?>

<div class="cms-content cms-widget">
    <?php 
        foreach ($widgetElements as $el) {
            echo '<div class="widget">';
            echo $el;
            echo '</div>';
        }
    ?>
</div>



