<div class="row irow">
    <div class="col-sm-2">
    </div>
    <div class="col-sm-8">
        <p class="lead">Unfortunately the content you’re looking for isn’t here.	There may be a misspelling in your web address or you may have clicked a link for content that no longer exists.</p>
    </div>
</div>
<hr/>
<div class="row irow">
    <div class="col-sm-8 col-sm-offset-4">
        <a href="/" class="btn btn-primary">Home</a>
    </div>
</div>