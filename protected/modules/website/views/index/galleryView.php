<?php if (!empty($allElements)): ?>
    <div class="galleryVor galleryTitel col-lg-6 col-md-6 col-sm-6 col-xs-6"><h3>Vorher</h3></div>
    <div class="galleryNach galleryTitel col-lg-6 col-md-6 col-sm-6 col-xs-6"><h3>Nachher</h3></div>
    <div class="clearBoth"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <ul class = "bxslider noBefore">
            <? foreach ($allElements as $elements): ?>
                <li><div class="bxContainer"><img src="/images/media/gallery/<? echo $elements->image; ?>" /></div></li>

            <? endforeach; ?>
        </ul>
    </div>
    <script>
        $('.bxslider').bxSlider({
            mode: 'fade',
            adaptiveHeight: true,
            captions: true
        });
    </script>
<? endif; ?>