<?php

class WidgetWebsiteContactForm extends CWidget {

    public $localeId;
    public $target;
    public $value = 0;
    public $cmsWidget;
    public $kontakt;

    public function init() {
        $this->cmsWidget = CmsWidgetContactForm::model()->find('target_id = :target', array(':target' => $this->target->id));
        if (!empty($this->cmsWidget)) {
            $this->value = $this->cmsWidget->value;
        }
        $this->kontakt = Kontaktseite::model()->find('locale_id = :locale', array(':locale' => Yii::app()->language));
    }

    public function run() {
        if ($this->value == 1 && !empty($this->kontakt)) {
            $this->render('kontakt');
        }
    }

}
