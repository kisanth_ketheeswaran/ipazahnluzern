<?php 

class WidgetWebsitePhpFile extends CWidget
{
  public $localeId;
  public $target;
  public $value = null;
  public $cmsWidget;
  public function init()
  {
    $this->cmsWidget = CmsWidgetPhpFile::model()->find('target_id = :target', array(':target' => $this->target->id));
    if (!empty($this->cmsWidget)) {
      $this->value = $this->cmsWidget->value;
    }
  }
  
  public function run()
  {
    $this->render('websitePhpFile');
  }
}
