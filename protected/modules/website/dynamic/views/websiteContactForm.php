<?php if ($this->value == 1) : ?>
<?php $model = new ContactForm(); ?>


<div class="form website" id="contact-form" style="padding-bottom: 20px;"> 

<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'contactform-form',
    'enableAjaxValidation'=>true,
    'focus'=>array($model,'message'),
    'htmlOptions' => array(
        'class' => 'ajax',
        'onSubmit' => 'javascript: return false;'
    )
)); ?>  

    <div class="row">
        <?php //echo $form->label($model, 'message', array('required' => true, 'style' => 'width: auto; float: none; margin-bottom: 7px; margin-top: 0;')); ?>
        <div class="textarea l-m"><?php echo $form->textArea($model, 'message', array('placeholder' => Yii::t('site', 'Ihre Nachricht *'), 'class' => 'required')); ?></div>
    </div>
    
    <div class="row">
        <?php echo $form->label($model, 'first_name', array('required' => true)); ?>
        <div class="textfield m left"><?php echo $form->textField($model, 'first_name', array('class' => 'required')); ?></div>    
        
        <?php echo $form->label($model, 'last_name', array('required' => true, 'style' => 'margin-left: 34px; width: 112px;')); ?>
        <div class="textfield m left"><?php echo $form->textField($model, 'last_name', array('class' => 'required')); ?></div> 
        
        <div class="clear"></div>                    
    </div>
    
    <div class="row">
        <?php echo $form->label($model, 'company', array('required' => false)); ?>
        <div class="textfield l left"><?php echo $form->textField($model, 'company'); ?></div>    
    </div>
    
    <div class="row">
        <?php echo $form->label($model, 'phone', array('required' => true)); ?>
        <div class="textfield m left"><?php echo $form->textField($model, 'phone', array('class' => 'required')); ?></div>    
        
        <?php echo $form->label($model, 'email', array('required' => true, 'style' => 'margin-left: 34px; width: 112px;')); ?>
        <div class="textfield m left"><?php echo $form->textField($model, 'email', array('class' => 'required')); ?></div> 
        
        <div class="clear"></div>                    
    </div>
    
    <div class="row">
        <?php echo $form->label($model, 'accessible', array('required' => false)); ?>
        <div class="textfield l left"><?php echo $form->textField($model, 'accessible'); ?></div>    
    </div>
    
    <div class="row button">
        <div class="msg"></div>
        <a class="ajaxLoader" style="display: none;"></a>
        <a class="buttonLink m right" style="margin-right: 10px;" href="javascript: void(0);"><?php echo Yii::t('site', 'Anfrage absenden'); ?></a>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php endif; ?>

<script type="text/javascript">
<!--
    jQuery('#contactform-form a.buttonLink').click(function() {
       var $this = jQuery(this);
       var $ajaxLoader = $this.siblings('.ajaxLoader');
       var $msg = $this.siblings('.msg');
       
       $this.hide();
       $ajaxLoader.show();
       
       var hasError = false;
       var data = 'ajax=1';
       $this.parents('form').find(':input').each(function() {
           if (jQuery(this).hasClass('required') && jQuery(this).val() == '') {
               hasError = true;
           }
           
           data += '&' + jQuery(this).attr('name') + '=' + jQuery(this).val();
       });
       
       if (hasError) {
           $this.show();
           $ajaxLoader.hide();
           $msg.removeClass('success');
           $msg.addClass('error');
           $msg.html('<?php echo Yii::t('site', 'Bitte füllen Sie alle Felder mit einem * (Stern) aus.'); ?>');
       } else {
           $msg.html('');
           
           jQuery.ajax({
              type: 'post',
              url: '/website/form/contact',
              data: data,
              success: function() {
                   $this.show();
                   $ajaxLoader.hide();
                   $msg.removeClass('error');
                   $msg.addClass('success');
                   $msg.html('<?php echo Yii::t('site', 'Vielen Dank! Ihre Nachricht wurde versendet.'); ?>');           
                  
              } 
           });
           
       }    
        
    });
//-->
</script>