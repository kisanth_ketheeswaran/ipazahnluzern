<?php

class BackendController extends Controller
{
  public $layout='//layouts/admin';
  public $defaultAction = 'admin';
  public $h1;
  
  public function init() {
    if (!App::hasAccess('admin') && Yii::app()->user->role != 'behandler') {
      $this->redirect('/login');
      exit;
    }
    if(Yii::app()->user->role == 'behandler'){
        if(!$this->pruefeZugangVonBehandler()){
           $this->redirect('/login');
        }
    }

    
  }
  
  /*Kisanth Ketheeswaran
   * Hier kontrollier ich, dass der Behandler nicht auf einem Modul geht welches 
   * er nicht sehen darf, deshalb tue ich alles modules, die er sehen darf,
   * in einem Array und dieser Array wird,
   * dannach mit der anfgefragten Url durchsucht   
   */
  public function pruefeZugangVonBehandler(){
      $id = CController::getId();
      $url = explode('/', $id);
      $bestimmterUrl = $url[0];
      $korrekteUrl = array('buchung','terminplan','termin');
      foreach ($korrekteUrl as $ru){
          if($ru==$bestimmterUrl){
              return true;
          }
      }
      return false;
  }
}