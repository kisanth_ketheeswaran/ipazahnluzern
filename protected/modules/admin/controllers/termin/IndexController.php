<?php

class IndexController extends BackendController {
    
    private $valOk = true;

    public function actionCreate() {
        $this->h1 = Yii::t('app', 'Termin / Erfassung');
        $model = new Termin;
        $terminplan = Terminplan::model()->with('behandlungsarts')->findAll();
        $behandlungsart = Behandlungsart::model()->findAll(array('order'=>'terminplan_idfs'));
        $sourceModel = $model;
        
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Termin']) && !empty($_POST['Behandlungsart']) 
                && isset($_POST['Terminplan'])) {
            $behandlung = $_POST['Behandlungsart'];
            $model->attributes = $_POST['Termin'];
            $model->terminplan_idfs = $_POST['Terminplan']['nachname'];
            if ($model->save()) {
                
                if(is_array($behandlung)){
                    Behandlung::model()->speicherBehandlung($model->id, $behandlung);
                }

                Yii::app()->user->setFlash('success', Yii::t('app', 'Erfasst.'));
                $this->redirect(array('update', 'id' => $sourceModel->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'sourceModel' => $sourceModel,
            'terminplan'=>$terminplan,
            'behandlungsart'=>$behandlungsart,
        ));
    }

    public function actionUpdate($id) {
        $this->h1 = Yii::t('app', 'Termin / Bearbeitung');

        $sourceModel = Termin::model()->with('terminplanIdfs','behandlungsart')->findByPk($id);
        
        $terminplan = Terminplan::model()->findAll();
        $behandlungsart = Behandlungsart::model()->with('behandlungs')->findAll(array('order'=>'terminplan_idfs'));

        $model = Termin::model()->with('terminplanIdfs','behandlungsart')->findByPk($sourceModel->id);
        if (empty($model)) {
            $model = new Termin();
        }

        if ($model->isNewRecord) {
            $model->attributes = $sourceModel->attributes;
        }

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);

        if (isset($_POST['Termin']) && !empty($_POST['Behandlungsart']) 
                && isset($_POST['Terminplan'])) {
            $behandlung = $_POST['Behandlungsart'];
            $model->attributes = $_POST['Termin'];
            $model->terminplan_idfs = $_POST['Terminplan']['nachname'];
            if ($model->save()) {
                
                if(is_array($behandlung)){
                    Behandlung::model()->speicherBehandlung($model->id, $behandlung);
                }
                
                Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                $this->redirect(array('update', 'id' => $sourceModel->id));
            }
        }
        $behandlung = Behandlung::model()->with('behandlungsartIdfs','terminIdfs')->findAll('termin_idfs =:id',array(':id'=>$model->id));
        $this->render('update', array(
            'model' => $model,
            'sourceModel' => $sourceModel,
            'terminplan'=>$terminplan,
            'behandlungsart'=>$behandlungsart,
            'buchBehandlung'=>$behandlung,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = Termin::model()->findByPk($id);
            Behandlung::model()->deleteAll('Termin_idfs = :id',array(':id'=>$model->id));
            $model->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionAdmin($date,$terminplan) {
        $date = empty($date) ? date('d-m-Y'): $date;

        $this->render('admin', array(
            'model' => $model,
            'nachname' => $nachname,
        ));
    }

    public function loadModel($id) {
        $model = Termin::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'termin-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    } 

}
