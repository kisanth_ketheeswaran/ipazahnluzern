<?php

class IndexController extends BackendController {
    /*
     * wenn zeit alles in schleifen durch gehen wegen den wochentagen, auch in _form
     *       */

    /* Wenn alle Models richtig validiert sind
     * bleibt es bei true, ansonsten false.
     * Dannach gibt es eine Fehlermeldung.
     * val ist die Abkürzung zu validierung. */

    private $valOk = true;

    /*
     *Kisanth Ketheeswaran
     * Hier werden alle Klassen intialisiert die man nachher für das Formular im Backend braucht
     * Und auch kommen diese Objekte zurück, wenn man das Formular ausgefüllt hat, werden die Daten gespeichert bzw. validiert.
     * So sind alle actionCreate() Funktionen  */
    public function actionCreate() {
        $this->h1 = Yii::t('app', 'Terminplan / Erfassung');
        $model = new Terminplan;
        $sourceModel = $model;

        /* Kisanth Ketheesaran
         * Intialisieren der Wochentage 
         */
        $montag = new Montag;
        $dienstag = new Dienstag;
        $mittwoch = new Mittwoch;
        $donnerstag = new Donnerstag;
        $freitag = new Freitag;
        $samstag = new Samstag;
        $sonntag = new Sonntag;
        $ferien = new Ferien;
        $freiertag = new Freiertag;
        $behandlungsart = new Behandlungsart;
        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);
        if (isset($_POST['Terminplan']) && isset($_POST['Montag']) && isset($_POST['Dienstag']) 
                && isset($_POST['Mittwoch']) && isset($_POST['Donnerstag']) && isset($_POST['Freitag']) 
                && isset($_POST['Samstag']) && isset($_POST['Sonntag'])) {

            $model->attributes = $_POST['Terminplan'];
            /*  */
            $montag = $this->pruefTag($montag, 'Montag');
            $dienstag = $this->pruefTag($dienstag, 'Dienstag');
            $mittwoch = $this->pruefTag($mittwoch, 'Mittwoch');
            $donnerstag = $this->pruefTag($donnerstag, 'Donnerstag');
            $freitag = $this->pruefTag($freitag, 'Freitag');
            $samstag = $this->pruefTag($samstag, 'Samstag');
            $sonntag = $this->pruefTag($sonntag, 'Sonntag');
            $ferien = $this->pruefFerien();
            $freiertag = $this->pruefFreiertag();
            $behandlungsart = $this->pruefBehandlungsart();
            if ($this->valOk == true) {
                if ($model->save()) {

                    if (!empty($ferien)) {
                        $ferien->terminplan_idfs = $model->id;
                        $ferien->save();
                    }
                    if (!empty($freiertag)) {
                        $freiertag->terminplan_idfs = $model->id;
                        $freiertag->save();
                    }
                    if (!empty($behandlungsart)) {
                        $behandlungsart->terminplan_idfs = $model->id;
                        $behandlungsart->save();
                    }

                    $wochentage = array(
                        'mo_idfs' => $montag,
                        'di_idfs' => $dienstag,
                        'mi_idfs' => $mittwoch,
                        'do_idfs' => $donnerstag,
                        'fr_idfs' => $freitag,
                        'sa_idfs' => $samstag,
                        'so_idfs' => $sonntag,
                    );

                    $this->speichernAlleModels($model, $wochentage);

                    Yii::app()->user->setFlash('success', Yii::t('app', 'Erfasst.'));
                    $this->redirect(array('update', 'id' => $sourceModel->id));
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
            'sourceModel' => $sourceModel,
            'montag' => $montag,
            'dienstag' => $dienstag,
            'mittwoch' => $mittwoch,
            'donnerstag' => $donnerstag,
            'freitag' => $freitag,
            'samstag' => $samstag,
            'sonntag' => $sonntag,
            'ferien' => $ferien,
            'freiertag' => $freiertag,
            'behandlungsart'=>$behandlungsart,
        ));
    }

    public function actionUpdate($id) {
        $this->h1 = Yii::t('app', 'Terminplan / Bearbeitung');

        $sourceModel = Terminplan::model()->with('feriens', 'freiertags')->findByPk($id);
        $alleFerien = $sourceModel->feriens;
        $alleFreietage = $sourceModel->freiertags;
        $ferien = new Ferien;
        $freiertag = new Freiertag;
        $standart = Standardkalender::model()->with('moIdfs', 'diIdfs', 'miIdfs', 'doIdfs', 'frIdfs', 'saIdfs', 'soIdfs')->find('terminplan_idfs=:id', array(':id' => $id));
        $montag = $standart->moIdfs;
        $dienstag = $standart->diIdfs;
        $mittwoch = $standart->miIdfs;
        $donnerstag = $standart->doIdfs;
        $freitag = $standart->frIdfs;
        $samstag = $standart->saIdfs;
        $sonntag = $standart->soIdfs;
        $behandlungsart = new Behandlungsart;
        if (!isset($_GET['locale']) || empty($_GET['locale'])) {
            $localeId = LocaleModel::getDefault()->id;
        } else {
            $localeId = $_GET['locale'];
        }
        $model = Terminplan::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $sourceModel->unit_hash, ':locale' => $localeId));
        if (empty($model)) {
            $model = new Terminplan();
        }

        if ($model->isNewRecord) {
            $model->attributes = $sourceModel->attributes;
            $model->locale_id = $localeId;
        }

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Terminplan']) && isset($_POST['Montag']) && isset($_POST['Dienstag']) 
                && isset($_POST['Mittwoch']) && isset($_POST['Donnerstag']) && isset($_POST['Freitag']) 
                && isset($_POST['Samstag']) && isset($_POST['Sonntag'])) {

            $model->attributes = $_POST['Terminplan'];
            $montag = $this->pruefTag($montag, 'Montag');
            $dienstag = $this->pruefTag($dienstag, 'Dienstag');
            $mittwoch = $this->pruefTag($mittwoch, 'Mittwoch');
            $donnerstag = $this->pruefTag($donnerstag, 'Donnerstag');
            $freitag = $this->pruefTag($freitag, 'Freitag');
            $samstag = $this->pruefTag($samstag, 'Samstag');
            $sonntag = $this->pruefTag($sonntag, 'Sonntag');
            $ferien = $this->pruefFerien();
            $freiertag = $this->pruefFreiertag();
            $behandlungsart = $this->pruefBehandlungsart();
            if ($this->valOk == true) {
                if ($model->save()) {
                    if (!empty($ferien)) {
                        $ferien->terminplan_idfs = $model->id;
                        $ferien->save();
                    }
                    if (!empty($freiertag)) {
                        $freiertag->terminplan_idfs = $model->id;
                        $freiertag->save();
                    }
                     if (!empty($behandlungsart)) {
                        $behandlungsart->terminplan_idfs = $model->id;
                        $behandlungsart->save();
                    }
                    $wochentage = array(
                        'mo_idfs' => $montag,
                        'di_idfs' => $dienstag,
                        'mi_idfs' => $mittwoch,
                        'do_idfs' => $donnerstag,
                        'fr_idfs' => $freitag,
                        'sa_idfs' => $samstag,
                        'so_idfs' => $sonntag,
                    );

                    $this->speichernAlleModels($model, $wochentage);
                    Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                    $this->redirect(array('update', 'id' => $sourceModel->id, 'locale' => $model->locale_id));
                }
            }
        }

        $this->render('update', array(
            'model' => $model,
            'sourceModel' => $sourceModel,
            'montag' => $montag,
            'dienstag' => $dienstag,
            'mittwoch' => $mittwoch,
            'donnerstag' => $donnerstag,
            'freitag' => $freitag,
            'samstag' => $samstag,
            'sonntag' => $sonntag,
            'ferien' => $ferien,
            'freiertag' => $freiertag,
            'behandlungsart'=>$behandlungsart,
        ));
    }

    /* Kisanth Ketheesaran
     * Löscht den Terminplaner und auch alle einträge von seinen Relationen
     */

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = Terminplan::model()->with('feriens', 'freiertags', 'standardkalenders' ,'behandlungsarts'/*,'buchungs','termins' */)->findByPk($id);
            $this->loeschtStandard($model->standardkalenders->id);
            Behandlungsart::model()->deleteAll('terminplan_idfs = :id',array(':id'=>$model->id));
            Ferien::model()->deleteAll('terminplan_idfs = :id',array(':id'=>$model->id));
            Freiertag::model()->deleteAll('terminplan_idfs = :id',array(':id'=>$model->id));
            $model->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /* Kisanth Ketheeswaran
     * Löscht alle Wochentage von einem ausgewählten Terminplaner
     */
    public function loeschtStandard($id) {
        $standard = Standardkalender::model()->with('miIdfs', 'moIdfs', 'saIdfs', 'soIdfs', 'diIdfs', 'doIdfs', 'frIdfs')->findByPk($id);
        $standard->miIdfs->delete();
        $standard->moIdfs->delete();
        $standard->saIdfs->delete();
        $standard->soIdfs->delete();
        $standard->diIdfs->delete();
        $standard->doIdfs->delete();
        $standard->frIdfs->delete();
        $standard->delete();
    }
    
    public function actionAdmin() {
        if (Yii::app()->user->role == 'behandler') {
            
        }
        $this->h1 = Yii::t('app', 'Terminplan');
        
        $model = new Terminplan('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Terminplan']))
            $model->attributes = $_GET['Terminplan'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = Terminplan::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'terminplan-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /* Kisanth Ketheeswaran
     * Diese Funktion bekommt den Wochentag und mit Hilfe des Models
     * schaut es ob die Regeln im Model eingehalten werden.
     * Auch werden hier die Wochentage in der DB gespeichert, wenn die Validierung
     * richtig ist.
     */

    public function pruefTag($tag, $tagModel) {
        $tag->attributes = $_POST[$tagModel];
        if (!empty($tag->frei)) {
            $tag->bis = "";
            $tag->von = "";
            return $tag;
        }
        if ($tag->validate()) {
            return $tag;
        } else {
            $this->valOk = false;
            return $tag;
        }
    }

    /* Kisanth Ketheeswaran
     * Diese Funktion bekommt den Terminplaner und seine Wochentage,
     * diese werden dann in den verschiedenen Datenbanken Tabellen gespeichert.
     */

    public function speichernAlleModels($model, $wochentag) {
        $standart = Standardkalender::model()->pruefeDBEintrag($model);
        if ($standart->isNewRecord) {
            foreach ($wochentag as $key => $tag) {
                $tag->save(false);
                $standart->$key = (int) $tag->id;
            }
            $standart->terminplan_idfs = (int) $model->id;
            $standart->save(false);
        } else {
            foreach ($wochentag as $key => $tag) {
                $tag->save(false);
                $standart->$key = (int) $tag->id;
            }
            $standart->save(false);
        }
    }

    /* Kisanth Ketheeswaran
     * Valiedert das Obejekt Ferien
     */

    public function pruefFerien() {
        $ferien = new Ferien;
        $ferien->attributes = $_POST['Ferien'];
        if (!empty($ferien->von_datum) || !empty($ferien->bis_datum) || !empty($ferien->von_zeit) || !empty($ferien->bis_zeit)) {
            if ($ferien->validate()) {
                return $ferien;
            } else {
                $this->valOk = false;
                return $ferien;
            }
        } else {
            return $ferien;
        }
    }

    /* Kisanth Ketheeswaran
     * Valiedert das Obejekt Freiertag
     */

    public function pruefFreiertag() {
        $tag = new Freiertag;
        $tag->attributes = $_POST['Freiertag'];
        if (!empty($tag->datum) || !empty($tag->bis) || !empty($tag->von)) {
            if ($tag->validate()) {
                return $tag;
            } else {
                $this->valOk = false;
                return $tag;
            }
        }
        return $tag;
    }
    
    /*Kisanth Ketheeswaran
     *Validiert das Objekt Behandlungsart    */
    public function pruefBehandlungsart(){
        $behandlung = new Behandlungsart;
        $behandlung->attributes = $_POST['Behandlungsart'];
        if(!empty($behandlung->behandlung) || !empty($behandlung->bemerkung)|| !empty($behandlung->position)){
            if($behandlung->validate()){
                return $behandlung;
            }else{
                $this->valOk = false;
                return $behandlung;
            }
        }
        return $behandlung;
    }
    


    /*Kisanth Ketheeswaran
     * Zeigt alle Ferien die er ausgewählt hat auf einer Liste
     * und kann sie je nach dem bearbeiten
     */
    public function actionFerienListe($id) {
        $this->h1 = Yii::t('app', 'Ferienliste');
        $sourceModel = Terminplan::model()->with('feriens')->findByPk($id);
        $alleFerien = $sourceModel->feriens;  
        foreach ($alleFerien as $ferien){
            if(isset($_POST['Ferien'.$ferien->id])){
                $ferien->attributes = $_POST['Ferien'.$ferien->id];
                if($ferien->save()){
                    Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                    //$this->redirect(array('ferienListe', 'id' => $id));
                }
            }
        }
        $this->render('ferienListe', array(
            'alleFerien' => $alleFerien,
        ));
    }
    
    /*Kisanth Ketheeswaran
     * Zeigt alle genommene Freietage die er ausgewählt hat auf einer Liste
     * und kann sie je nach dem bearbeiten
     */
    public function actionFreiertagListe($id) {
        $this->h1 = Yii::t('app', 'Frei genommene Tage');
        $sourceModel = Terminplan::model()->with('freiertags')->findByPk($id);
        $alleFreietage = $sourceModel->freiertags;  
        foreach ($alleFreietage as $frei){
            if(isset($_POST['Freiertag'.$frei->id])){
                $frei->attributes = $_POST['Freiertag'.$frei->id];
                if($frei->save()){
                    Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                    //$this->redirect(array('ferienListe', 'id' => $id));
                }
            }
        }
        $this->render('freiertagListe', array(
            'alleFreietage' => $alleFreietage,
        ));
    }
    
    /*Kisanth Ketheeswaran
     * Zeigt alle genommene Freietage die er ausgewählt hat auf einer Liste
     * und kann sie je nach dem bearbeiten
     */
    public function actionBehandlungsartListe($id) {
        $this->h1 = Yii::t('app', 'Behandlungsart');
        $sourceModel = Terminplan::model()->with('behandlungsarts')->findByPk($id);
        $alleBehandlungen = $sourceModel->behandlungsarts;  
        foreach ($alleBehandlungen as $behandlungsart){
            if(isset($_POST['Behandlungsart'.$behandlungsart->id])){
                $behandlungsart->attributes = $_POST['Behandlungsart'.$behandlungsart->id];
                if($behandlungsart->save()){
                    Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                    //$this->redirect(array('ferienListe', 'id' => $id));
                }
            }
        }
        $this->render('behandlungsartListe', array(
            'alleBehandlungen' => $alleBehandlungen,
        ));
    }
    
    
    /*Kisanth Ketheeswaran 
     * Löscht den ausgewählten Eintrag "Ferien" */
    public function actionEntferneFerien(){
        if (Yii::app()->request->isPostRequest) {
            $id = $_POST['id'];
            $model = Ferien::model()->findByPk($id);
            $terminplanId = $model->terminplan_idfs;
            $model->delete();
            if(!Ferien::model()->findAll('terminplan_idfs = :id',array(':id'=>$terminplanId))){
                 echo CJSON::encode("leer");
            }
        }
    }
    /*Kisanth Ketheeswaran 
     * Löscht den ausgewählten Eintrag "Freiertag"     */
    public function actionEntferneFreiertag(){
        if (Yii::app()->request->isPostRequest) {
            $id = $_POST['id'];
            $model = Freiertag::model()->findByPk($id);
            $terminplanId = $model->terminplan_idfs;
            $model->delete();
            if(!Freiertag::model()->findAll('terminplan_idfs = :id',array(':id'=>$terminplanId))){
                echo CJSON::encode("leer");
            }
        }
    }
    
    /*Kisanth Ketheeswaran 
     * Löscht den ausgewählten Eintrag "Behandlungsart"     */
    public function actionEntferneBehandlungsart(){
        if (Yii::app()->request->isPostRequest) {
            $id = $_POST['id'];
            $model = Behandlungsart::model()->findByPk($id);
            $terminplanId = $model->terminplan_idfs;
            $model->delete();
            if(!Behandlungsart::model()->findAll('terminplan_idfs = :id',array(':id'=>$terminplanId))){
                echo CJSON::encode("leer");
            }
        }
    }

}
