<?php

class IndexController extends BackendController
{
	public function actionCreate()
	{
	    $this->h1 = Yii::t('app', 'Job / Erfassung');
        
		$model=new Job;
        $sourceModel = $model;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Job']))
		{
			$model->attributes=$_POST['Job'];
            $model->sort_order = $model->sort_order ? $model->sort_order : 0;
            $model->date = $model->date ? $model->date : date('d.m.Y');
            
            $file = CUploadedFile::getInstance($model,'file');

            if($model->save()) {
                
                if (!empty($file)) {
                    $pathinfo = pathinfo($file);
                   
                    $filenameWithoutExtension = str_replace('.' . $pathinfo['extension'], '', $file->getName());
                    $name = $model->id . '_' . $filenameWithoutExtension . '.' . $pathinfo['extension'];
                    $model->file = $name;
                    $file->saveAs(Yii::app()->basePath . '/../images/job/' . $name);
                    $model->save();
                }
                
                
                Yii::app()->user->setFlash('success', Yii::t('app', 'Erfasst.'));
                $this->redirect(array('update','id'=>$sourceModel->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
			'sourceModel' => $sourceModel
		));
	}

	public function actionUpdate($id)
	{
	    $this->h1 = Yii::t('app', 'Job / Bearbeitung');
        
        $sourceModel=$this->loadModel($id);
        
        if (!isset($_GET['locale']) || empty($_GET['locale'])) {
            $localeId = LocaleModel::getDefault()->id;
        } else {
            $localeId = $_GET['locale'];
        }
        
        $model = Job::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $sourceModel->unit_hash, ':locale' => $localeId));
        if (empty($model)) {
            $model = new Job();
        }
        
        if ($model->isNewRecord) {
            $model->attributes = $sourceModel->attributes;
            $model->file = '';
            $model->locale_id = $localeId;   
        }

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
        
        $currentFile = $model->file;
        
		if(isset($_POST['Job']))
		{
			$model->attributes=$_POST['Job'];
            $model->sort_order = $model->sort_order ? $model->sort_order : 0;
            $model->date = $model->date ? date('Y-m-d', strtotime($model->date)) : date('d.m.Y');
            
            $file = CUploadedFile::getInstance($model,'file');            
            
			if($model->save()) {
			    
                
                if (!empty($file)) {
                    
                    App::deleteFile($currentFile, 'job');
                    $pathinfo = pathinfo($file);
                   
                    $filenameWithoutExtension = str_replace('.' . $pathinfo['extension'], '', $file->getName());
                    $name = $model->id . '_' . $filenameWithoutExtension . '.' . $pathinfo['extension'];
                    $model->file = $name;
                    $file->saveAs(Yii::app()->basePath . '/../images/job/' . $name);
                    $model->save();
                } else {
                    $model->file = $currentFile;
                    $model->save();
                }
                
                Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                $this->redirect(array('update','id'=>$sourceModel->id, 'locale' => $model->locale_id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'sourceModel' => $sourceModel
		));
	}
    
    public function actionDeleteFile()
    {
        if (Yii::app()->request->isPostRequest) {
            $id = $_POST['id'];
            $model = Job::model()->findByPk($id);
            App::deleteFile($model->file, 'job');
            $model->file = '';
            $model->save();
        }
    }

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->deleteModel();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionAdmin()
	{
	    $this->h1 = Yii::t('app', 'Jobs');
		$model=new Job('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Job']))
			$model->attributes=$_GET['Job'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=Job::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='job-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
