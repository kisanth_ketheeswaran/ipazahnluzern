<?php

class IndexController extends BackendController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
	    $this->h1 = Yii::t('app', 'Portfolio / Detail');
        
        $sourceModel = $this->loadModel($id);
        $model = $sourceModel->getModel($_GET['locale'], true);
        
		$this->render('view',array(
			'sourceModel'=>$sourceModel,
			'model' => $model
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Reference;
        $sourceModel = $model;
        
        $this->h1 = Yii::t('app', 'Portfolio / Erfassung');
        

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Reference']))
		{
		    
            $model->attributes=$_POST['Reference'];
            $model = $this->addReferenceTopics($model);
            $model = $this->addReferenceBusiness($model);
            $model = $this->addReferenceTechnology($model);
            
            $model->sort_order = $model->sort_order ? $model->sort_order : 0;

            

            if($model->save()) {
                Yii::app()->user->setFlash('success', Yii::t('app', 'Erfasst.'));
				$this->redirect(array('view','id'=>$model->id));
			}
            
		}

		$this->render('create',array(
			'model'=>$model,
			'sourceModel' => $sourceModel
		));
	}
    
    public function addReferenceTopics($model)
    {
        if (isset($_POST['ReferenceTopic'])) {
            $referenceTopics = $_POST['ReferenceTopic'];
            $topicsArray = array();
            foreach ($referenceTopics as $topic) {
                if (!empty($topic)) {
                    $topicModel = new ReferenceTopic();
                    $topicModel->name = $topic;
                    $topicModel->locale_id = LocaleModel::getDefault()->id;
                    $topicModel->unit_hash = uniqid();
                    $topicModel->save();
                    $topicsArray[] = $topicModel->unit_hash;
                }
            }
        }
        
        if (is_array($model->reference_topic_id))
            $model->reference_topic_id = array_merge($model->reference_topic_id, $topicsArray);
        else
            $model->reference_topic_id = $topicsArray;
        
        return $model;
    }
    
    public function addReferenceBusiness($model)
    {
        if (isset($_POST['ReferenceBusiness'])) {
            $referenceTopics = $_POST['ReferenceBusiness'];
            $topicsArray = array();
            foreach ($referenceTopics as $topic) {
                if (!empty($topic)) {
                    $topicModel = new ReferenceBusiness();
                    $topicModel->name = $topic;
                    $topicModel->locale_id = LocaleModel::getDefault()->id;
                    $topicModel->unit_hash = uniqid();
                    $topicModel->save();
                    $topicsArray[] = $topicModel->unit_hash;
                }
            }
        }
        
        if (is_array($model->reference_business_id))
            $model->reference_business_id = array_merge($model->reference_business_id, $topicsArray);
        else
            $model->reference_business_id = $topicsArray;
        
        return $model;
    }
    
    public function addReferenceTechnology($model)
    {
        if (isset($_POST['ReferenceTechnology'])) {
            $referenceTopics = $_POST['ReferenceTechnology'];
            $topicsArray = array();
            foreach ($referenceTopics as $topic) {
                if (!empty($topic)) {
                    $topicModel = new ReferenceTechnology();
                    $topicModel->name = $topic;
                    $topicModel->locale_id = LocaleModel::getDefault()->id;
                    $topicModel->unit_hash = uniqid();
                    $topicModel->save();
                    $topicsArray[] = $topicModel->unit_hash;
                }
            }
        }
        
        if (is_array($model->reference_technology_id))
            $model->reference_technology_id = array_merge($model->reference_technology_id, $topicsArray);
        else
            $model->reference_technology_id = $topicsArray;
        
        return $model;
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$sourceModel=$this->loadModel($id);
        
        if (!isset($_GET['locale']) || empty($_GET['locale'])) {
            $localeId = LocaleModel::getDefault()->id;
        } else {
            $localeId = $_GET['locale'];
        }
        
        $model = Reference::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $sourceModel->unit_hash, ':locale' => $localeId));
        if (empty($model)) {
            $model = new Reference();
        }
        
        if ($model->isNewRecord) {
            $model->attributes = $sourceModel->attributes;
            $model->locale_id = $localeId;   
        }

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

        $this->h1 = Yii::t('app', 'Portfolio / Bearbeitung');

		if(isset($_POST['Reference']))
		{
			$model->attributes=$_POST['Reference'];
            $model = $this->addReferenceTopics($model);
            $model = $this->addReferenceBusiness($model);
            $model = $this->addReferenceTechnology($model);
            
			if($model->save()) {
			    
                
			    Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
				$this->redirect(array('view','id'=>$sourceModel->id, 'locale' => $model->locale_id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'sourceModel' => $sourceModel
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->deleteModel();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
	    $this->h1 = Yii::t('app', 'Portfolio'); 
		$model=new Reference('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Reference']))
			$model->attributes=$_GET['Reference'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Reference::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
    
    public function actionCreateBusiness()
    {
        $categories = ReferenceBusiness::model()->findAll('locale_id = :locale ORDER BY name ASC', array(':locale' => LocaleModel::getDefault()->id));
        
        if (isset($_GET['locale']) && !empty($_GET['locale'])) {
            $locale = LocaleModel::model()->findByPk($_GET['locale']);
        } else {
            $locale = LocaleModel::getDefault();
        }
        
        $this->h1 = Yii::t('app', 'Portfolio-Branchen');
        
        if (Yii::app()->request->isPostRequest) {
        
            if (isset($_POST['ReferenceBusinessEdit']))
            {
                foreach ($_POST['ReferenceBusinessEdit'] as $key => $categoryEdit) {
                    foreach ($categoryEdit as $id => $value) {
                        $editCat = ReferenceBusiness::model()->findByPk($id);
                        
                        if (!empty($value)) {
                            $editCat->name = $value;
                            $editCat->save();
                        } else {
                            $editCat->deleteModel();
                        }
                    } 
                }            
            }

            if (isset($_POST['ReferenceBusinessTranslate']))
            {
                foreach ($_POST['ReferenceBusinessTranslate'] as $key => $categoryEdit) {
                    foreach ($categoryEdit as $hash => $value) {
                        if (!empty($value)) {
                            $transCat = new ReferenceBusiness();
                            $transCat->locale_id = $locale->id;
                            $transCat->unit_hash = $hash;
                            $transCat->name = $value;
                            $transCat->save();
                        }                        
                    } 
                }            
                
                
            }
            
            if (isset($_POST['ReferenceBusiness']))
            {
                foreach ($_POST['ReferenceBusiness'] as $key => $categoryNew) {
                    foreach ($categoryNew as $id => $value) {
                        if (!empty($value)) {
                            $newCat = new ReferenceBusiness();
                            $newCat->name = $value;
                            $newCat->unit_hash = uniqid();
                            $newCat->locale_id = $locale->id;
                            $newCat->save();
                        }
                    } 
                }            
            }
            
            
            Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
            $this->redirect(array('createBusiness', 'locale' => $locale->id));
        }
        
        
        $this->render('createBusiness', array(
            'categories' => $categories,
            'localeModel' => $locale
        ));
    }

    public function actionCreateTechnology()
    {
        $categories = ReferenceTechnology::model()->findAll('locale_id = :locale ORDER BY name ASC', array(':locale' => LocaleModel::getDefault()->id));
        
        if (isset($_GET['locale']) && !empty($_GET['locale'])) {
            $locale = LocaleModel::model()->findByPk($_GET['locale']);
        } else {
            $locale = LocaleModel::getDefault();
        }
        
        $this->h1 = Yii::t('app', 'Portfolio-Technologien');
        
        if (Yii::app()->request->isPostRequest) {
        
            if (isset($_POST['ReferenceTechnologyEdit']))
            {
                foreach ($_POST['ReferenceTechnologyEdit'] as $key => $categoryEdit) {
                    foreach ($categoryEdit as $id => $value) {
                        $editCat = ReferenceTechnology::model()->findByPk($id);
                        
                        if (!empty($value)) {
                            $editCat->name = $value;
                            $editCat->save();
                        } else {
                            $editCat->deleteModel();
                        }
                    } 
                }            
            }

            if (isset($_POST['ReferenceTechnologyTranslate']))
            {
                foreach ($_POST['ReferenceTechnologyTranslate'] as $key => $categoryEdit) {
                    foreach ($categoryEdit as $hash => $value) {
                        if (!empty($value)) {
                            $transCat = new ReferenceTechnology();
                            $transCat->locale_id = $locale->id;
                            $transCat->unit_hash = $hash;
                            $transCat->name = $value;
                            $transCat->save();
                        }                        
                    } 
                }            
                
                
            }
            
            if (isset($_POST['ReferenceTechnology']))
            {
                foreach ($_POST['ReferenceTechnology'] as $key => $categoryNew) {
                    foreach ($categoryNew as $id => $value) {
                        if (!empty($value)) {
                            $newCat = new ReferenceTechnology();
                            $newCat->name = $value;
                            $newCat->unit_hash = uniqid();
                            $newCat->locale_id = $locale->id;
                            $newCat->save();
                        }
                    } 
                }            
            }
            
            
            Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
            $this->redirect(array('createTechnology', 'locale' => $locale->id));
        }
        
        
        $this->render('createTechnology', array(
            'categories' => $categories,
            'localeModel' => $locale
        ));
    }

    public function actionCreateTopic()
    {
        $categories = ReferenceTopic::model()->findAll('locale_id = :locale ORDER BY name ASC', array(':locale' => LocaleModel::getDefault()->id));
        
        if (isset($_GET['locale']) && !empty($_GET['locale'])) {
            $locale = LocaleModel::model()->findByPk($_GET['locale']);
        } else {
            $locale = LocaleModel::getDefault();
        }
        
        $this->h1 = Yii::t('app', 'Portfolio-Themen');
        
        if (Yii::app()->request->isPostRequest) {
        
            if (isset($_POST['ReferenceTopicEdit']))
            {
                foreach ($_POST['ReferenceTopicEdit'] as $key => $categoryEdit) {
                    foreach ($categoryEdit as $id => $value) {
                        $editCat = ReferenceTopic::model()->findByPk($id);
                        
                        if (!empty($value)) {
                            $editCat->name = $value;
                            $editCat->save();
                        } else {
                            $editCat->deleteModel();
                        }
                    } 
                }            
                
                
            }

            if (isset($_POST['ReferenceTopicTranslate']))
            {
                foreach ($_POST['ReferenceTopicTranslate'] as $key => $categoryEdit) {
                    foreach ($categoryEdit as $hash => $value) {
                        if (!empty($value)) {
                            $transCat = new ReferenceTopic();
                            $transCat->locale_id = $locale->id;
                            $transCat->unit_hash = $hash;
                            $transCat->name = $value;
                            $transCat->save();
                        }                        
                    } 
                }            
                
                
            }
            
            if (isset($_POST['ReferenceTopic']))
            {
                foreach ($_POST['ReferenceTopic'] as $key => $categoryNew) {
                    foreach ($categoryNew as $id => $value) {
                        if (!empty($value)) {
                            $newCat = new ReferenceTopic();
                            $newCat->name = $value;
                            $newCat->unit_hash = uniqid();
                            $newCat->locale_id = $locale->id;
                            $newCat->save();
                        }
                    } 
                }            
            }
            
            
            Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
            $this->redirect(array('createTopic', 'locale' => $locale->id));
        }
        
        
        $this->render('createTopic', array(
            'categories' => $categories,
            'localeModel' => $locale
        ));
    }

    public function actionUploadPortfolio()
    {
        $folderUpload = Yii::app()->basePath . '/../images/upload/';
        $folderMedia = Yii::app()->basePath . '/../images/media/';
        
        $qqFile = $_GET['qqfile'];
        $portfolioHash = $_GET['portfolio'];
        
        Yii::import("ext.EAjaxUpload.qqFileUploader");
        
        $allowedExtensions = array('jpg', 'jpeg', 'gif', 'png');
        $sizeLimit = 7 * 1024 * 1024;

        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folderUpload);
        $filename = $result['filename'];
        $pathinfo = pathinfo($filename);
        $filenameWithoutExtension = str_replace('.' . $pathinfo['extension'], '', $filename);
        $newFilename = $filenameWithoutExtension . '-' . $portfolioHash . '-' . uniqid() . '.' . $pathinfo['extension'];
        
        
        if ($result['success']) {
            if (copy($folderUpload . $filename, $folderMedia . $newFilename))
            {
                @unlink($folderUpload . $filename);
            }
            
            $portfolioPic = new ReferencePic();
            $portfolioPic->portfolio_hash = $portfolioHash;
            $portfolioPic->pic = $newFilename;
            $portfolioPic->label = '';
            $portfolioPic->type = '';
            $portfolioPic->sort_order = 99;
            $portfolioPic->save();
        }
        

        $result=htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $result;// it's array
    }

    public function actionRefreshPic()
    {
        $portfolioHash = $_GET['portfolio'];
        $portfolio = Reference::model()->find('unit_hash = :hash AND locale_id = :locale', array(
            ':hash' => $portfolioHash,
            ':locale' => LocaleModel::getDefault()->id
        ));
        
        $this->renderPartial('refreshPic', array(
            'pics' => $portfolio->getPics()
        ));
        
    }
    
    public function actionDeletePic()
    {
        $portfolioPic = ReferencePic::model()->findByPk($_GET['pic']);
        App::deleteFile($portfolioPic->pic, 'media');
        $portfolioPic->delete();
    }
    
    public function actionUpdatePic()
    {
        $referencePic = ReferencePic::model()->findByPk($_GET['pic']);
        $referencePicPost = $_POST['ReferencePic'][$referencePic->id];
        
        $referencePic->label = $referencePicPost['label'];
        $referencePic->sort_order = $referencePicPost['sort_order'];
        
        if (isset($referencePicPost['type'])) {
            $referencePic->type = array();
            foreach ($referencePicPost['type'] as $type) {
                $referencePic->type = array_merge($referencePic->type, array($type));
            }
        }
        
        $referencePic->save();

    }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='reference-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
