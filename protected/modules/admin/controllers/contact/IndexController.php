<?php

class IndexController extends BackendController
{
	public function actionCreate()
	{
	    $this->h1 = Yii::t('app', 'Footerkontakt / Erfassung');
            $model=new Contact;
            $sourceModel = $model;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Contact']))
		{
			$model->attributes=$_POST['Contact'];
            
			if($model->save()) {
			    
                
                Yii::app()->user->setFlash('success', Yii::t('app', 'Erfasst.'));
                $this->redirect(array('update','id'=>$sourceModel->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'sourceModel' => $sourceModel
		));
	}

	public function actionUpdate($id)
	{
	    $this->h1 = Yii::t('app', 'Footerkontakt / Bearbeitung');
        
        $sourceModel=$this->loadModel($id);
        
        if (!isset($_GET['locale']) || empty($_GET['locale'])) {
            $localeId = LocaleModel::getDefault()->id;
        } else {
            $localeId = $_GET['locale'];
        }
        
        $model = Contact::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $sourceModel->unit_hash, ':locale' => $localeId));
        if (empty($model)) {
            $model = new Contact();
        }
        
        if ($model->isNewRecord) {
            $model->attributes = $sourceModel->attributes;
            $model->locale_id = $localeId;   
        }

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Contact']))
		{
			$model->attributes=$_POST['Contact'];
			if($model->save()) {
			    
                Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                $this->redirect(array('update','id'=>$sourceModel->id, 'locale' => $model->locale_id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'sourceModel' => $sourceModel
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->deleteModel();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionAdmin()
	{
        $this->h1 = Yii::t('app', 'Kontakt');
        
		$model=new Contact('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Contact']))
			$model->attributes=$_GET['Contact'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=Contact::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Contact-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
