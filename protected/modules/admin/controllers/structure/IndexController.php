<?php

class IndexController extends BackendController {
    
    public function actionIndex(){
        $asd = 23;
    }

    public function actionCreate() {
        $model = new Structure;
        $parent = null;
        $sibling = null;
        $type = null;

        if (isset($_GET['parent']))
            $parent = $_GET['parent'];
        if (isset($_GET['sibling']))
            $sibling = $_GET['sibling'];
        if (isset($_GET['type']))
            $type = $_GET['type'];

        $model->type = $type;

        $this->performAjaxValidation($model);

        if (isset($_POST['Structure'])) {

            if ($type == 'nav') {
                if (!empty($parent)) {
                    $model->parent_id = Structure::model()->findByPk((int) $parent)->id;
                    $lastChild = Structure::model()->find('parent_id = :parent ORDER BY position DESC', array(':parent' => $model->parent_id));
                    $model->position = empty($lastChild) ? 1 : $lastChild->position + 1;
                } elseif (!empty($sibling)) {
                    $siblingElement = Structure::model()->findByPk((int) $sibling);
                    $model->parent_id = $siblingElement->parent_id;
                    if (empty($siblingElement->parent_id))
                        $otherElements = Structure::model()->findAll('parent_id = 0 AND position > :position AND id != :id ORDER BY position ASC', array(':position' => $siblingElement->position, ':id' => $siblingElement->id));
                    else
                        $otherElements = Structure::model()->findAll('parent_id = :parent AND position > :position AND id != :id ORDER BY position ASC', array(':position' => $siblingElement->position, ':id' => $siblingElement->id, ':parent' => $siblingElement->parent_id));
                    foreach ($otherElements as $element) {
                        $element->position += 1;
                        $element->save();
                    }
                    $model->position = $siblingElement->position + 1;
                } else {
                    $otherElements = Structure::model()->findAll('parent_id = 0 ORDER BY position ASC');
                    foreach ($otherElements as $element) {
                        $element->position += 1;
                        $element->save();
                    }
                    $model->position = 1;
                }

                $model->attributes = $_POST['Structure'];
                $model->locale_id = LocaleModel::getDefault()->id;
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'Erfolgreich erfasst.'));
                    $this->redirect(array('structure/index/update', 'id' => $model->id));
                }
            } else {
                $model->parent_id = 0;
                if (!empty($sibling)) {
                    $siblingElement = Structure::model()->findByPk((int) $sibling);
                    $otherElements = Structure::model()->findAll('parent_id = 0 AND position > :position AND id != :id ORDER BY position ASC', array(':position' => $siblingElement->position, ':id' => $siblingElement->id));
                    foreach ($otherElements as $element) {
                        $element->position += 1;
                        $element->save();
                    }
                    $model->position = $siblingElement->position + 1;
                } else {
                    $otherElements = Structure::model()->findAll('parent_id = 0 ORDER BY position ASC');
                    foreach ($otherElements as $element) {
                        $element->position += 1;
                        $element->save();
                    }
                    $model->position = 1;
                }

                $model->attributes = $_POST['Structure'];
                $model->locale_id = LocaleModel::getDefault()->id;
                $model->in_main = 0;
                $model->in_header = 0;
                $model->in_footer = 0;
                $model->link_redirect_point = '';
                $model->target = '';
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'Erfolgreich erfasst.'));
                    $this->redirect(array('structure/index/admin'));
                }
            }
        }

        $localeObj = LocaleModel::getDefault();

        $this->h1 = Yii::t('app', 'Seitenstruktur / Erfassung');
        $this->render('create', array(
            'model' => $model,
            'localeObj' => $localeObj,
            'localeModel' => null,
        ));
    }

    public function actionUpdate($id) {
        $locale = empty($_GET['locale']) ? LocaleModel::getDefault() : LocaleModel::model()->find('id = :id', array(':id' => $_GET['locale']));
        $sourceModel = Structure::getSource($id);
        $model = Structure::getModel($sourceModel, $locale);
        $localeModel = $sourceModel->getLocale($locale);

        $this->performAjaxValidation($model);

        if (isset($_POST['Structure'])) {
            $model->attributes = $_POST['Structure'];

            if ($locale->id == LocaleModel::getDefault()->id) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'Erfolgreich gespeichert.'));
                    $this->redirect(array('update', 'id' => $sourceModel->id, 'locale' => $locale->id));
                }
            } else {
                $localeModel = $sourceModel->getLocale($locale);
                if (empty($localeModel)) {
                    $localeModel = new Structure();
                    $localeModel->type = 'nav';
                    $localeModel->site_id = $sourceModel->site_id;
                    $localeModel->locale_id = $locale->id;
                    $localeModel->source_id = $sourceModel->id;

                    if ($sourceModel->parent->id == $sourceModel->getSiteRoot()->id) {
                        $localeModel->parent_id = $sourceModel->getSiteRoot()->id;
                    } else {
                        $localeModel->parent_id = empty($sourceModel->parent) ? 0 : $sourceModel->parent->getLocale($locale)->id;
                    }


                    $localeModel->attributes = $_POST['Structure'];
                    $localeModel->position = $sourceModel->position;

                    if ($localeModel->save()) {
                        Yii::app()->user->setFlash('success', Yii::t('app', 'Erfolgreich gespeichert.'));
                        $this->redirect(array('update', 'id' => $sourceModel->id, 'locale' => $locale->id));
                    }
                } else {
                    if ($model->save()) {
                        Yii::app()->user->setFlash('success', Yii::t('app', 'Erfolgreich gespeichert.'));
                        $this->redirect(array('update', 'id' => $sourceModel->id, 'locale' => $locale->id));
                    }
                }
            }
        }

        $this->h1 = Yii::t('app', 'Seitenstruktur / Bearbeitung');
        $this->render('update', array(
            'model' => $model,
            'localeModel' => $localeModel,
            'localeObj' => $locale,
            'sourceModel' => $sourceModel
        ));
    }

    public function actionUpdatePage($id) {
        $model = Page::model()->with('emotionpics')->findByPk($id);
        $modelOption = $model->pageOption;
        //$emotionModel = Emotionpic::model()->find('page_idfs = :id', array(':id'=>$model->id));
        $emotionModel = new Emotionpic;

        $this->performAjaxValidation($model);

        if (empty($model->structure->source_id)) {
            $structureSource = $model->structure;
            $structureLocale = $structureSource;
        } else {
            $structureSource = Structure::model()->find('id = :id', array(':id' => $model->structure->source_id));
            $structureLocale = $model->structure;
        }

        if (isset($_POST['Page'])) {
            $model->attributes = $_POST['Page'];


            if ($model->save() && $this->updatePageOption($modelOption) && $this->updateEmotionPic($emotionModel, $model->id)) {

                $this->saveWidgets($model);

                Yii::app()->user->setFlash('success', Yii::t('app', 'Erfolgreich gespeichert.'));

                if (isset($_POST['saveandgo'])) {
                    $this->redirect($model->structure->getUrl(true));
                    exit;
                }

                $this->redirect(array('structure/index/updatePage', 'id' => $model->id, 'locale' => $structureLocale->locale_id));
            }
        }

        $this->h1 = Yii::t('app', 'Seite / Bearbeitung');
        $this->render('updatePage', array(
            'model' => $model,
            'modelOption' => $modelOption,
            'emotionModel' => $emotionModel,
            /* 'modelPortfolio' => $modelPortfolio,
              'modelSlider' => $modelSlider, */
            'structureSource' => $structureSource,
            'structureLocale' => $structureLocale
        ));
    }

    public function updateEmotionPic($model, $page_id) {
        if (!empty($_POST['Emotionpic'])) {
            $model->attributes = $_POST['Emotionpic'];

            $model->name = CUploadedFile::getInstance($model, 'name');
            if (!empty($model->name)) {
                $model->page_idfs = $page_id;
                $type = $model->name->getExtensionName();
                if ($model->save()) {
                    if (!empty($model->name)) {
                        $model->name->saveAs(Yii::app()->basePath . '/../images/option/emotion_pic_' . $model->id . '.' . $type);
                        $image = Yii::app()->image->load(Yii::app()->basePath . '/../images/option/emotion_pic_' . $model->id . '.' . $type);
                        $image->resize(300, 70);
                        $image->save(Yii::app()->basePath . '/../images/option/thumb/thumb_pic' . $model->id . '.' . $type);
                        $model->name = "emotion_pic_" . $model->id . '.' . $type;
                        $model->save(false);
                    }
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public function actionDeleteEmotion() {
        // we only allow deletion via POST request
        if (Yii::app()->request->isPostRequest) {
            $id = $_POST['id'];
            $model = Emotionpic::model()->findByPk($id);
            $teile = explode(".", $model->name);
            @unlink(Yii::app()->basePath . '/../images/option/' . $model->name);
            @unlink(Yii::app()->basePath . '/../images/option/thumb/thumb_pic' . $model->id . '.' . $teile[1]);
            $model->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function saveWidgets($model) {
        foreach ($model->getWidgets() as $widget) {
            $widgetModelName = 'CmsWidget' . $widget->name;
            $widgetModel = new $widgetModelName();
            $widgetModel->saveWidget($model, $_POST['Widget'][$widget->name]);
        }
    }

    public function updatePageOption($pageOption) {
        if (isset($_POST['PageOption'])) {
            $keyUpload = array();
            foreach (PageOption::fields() as $key => $field) {
                if ($field == 'fileField') {
                    $keyUpload[$key] = CUploadedFile::getInstance($pageOption, $key);
                    $pageOption->$key = $pageOption->$key;
                    unset($_POST['PageOption'][$key]);
                }
            }

            $pageOption->attributes = $_POST['PageOption'];

            if ($pageOption->save()) {
                foreach (PageOption::fields() as $key => $field) {
                    if ($field == 'fileField') {
                        if (!empty($keyUpload[$key])) {
                            $pathinfo = pathinfo($keyUpload[$key]->getName());
                            @unlink(Yii::app()->basePath . '/../images/option/' . $pageOption->$key);
                            $fileName = $key . '_' . $pageOption->id . '.' . $pathinfo['extension'];
                            $pageOption->$key = $fileName;
                            $pageOption->save();
                            $keyUpload[$key]->saveAs(Yii::app()->basePath . '/../images/option/' . $fileName);
                        }
                    }
                }

                return true;
            }
            return false;
        }
        return true;
    }

    public function actionDeleteOptionFile() {
        if (Yii::app()->request->isPostRequest) {
            $pageOption = PageOption::model()->findByPk($_POST['pageOptionId']);
            $fieldKey = $_POST['fieldKey'];
            @unlink(Yii::app()->basePath . '/../images/option/' . $pageOption->$fieldKey);
            $pageOption->$fieldKey = '';
            $pageOption->update();
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionAdmin() {
        $this->h1 = Yii::t('app', 'Seitenstruktur');
        $this->render('admin');
    }

    public function actionStructure($id) {
        if (!isset(Yii::app()->session['structure_' . $id])) {
            Yii::app()->session['structure_' . $id] = $id;
        } else {
            Yii::app()->session->remove('structure_' . $id);
        }
    }

    public function actionSort() {
        if (Yii::app()->request->isPostRequest) {
            $structures = $_POST['id'];
            $position = 1;
            foreach ($structures as $structureId) {
                Structure::model()->updateByPk($structureId, array('position' => $position));
                Structure::model()->updateAll(array('position' => $position), 'source_id = :source', array(':source' => $structureId));
                $position++;
            }
        }
    }

    public function loadModel($id) {
        $model = Structure::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'structure-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
