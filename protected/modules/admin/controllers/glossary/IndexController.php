<?php

class IndexController extends BackendController {

    public function actionCreate() {
        $this->h1 = Yii::t('app', 'Glossar / Erfassung');
        $model = new Glossary;
        $sourceModel = $model;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Glossary'])) {
            $model->attributes = $_POST['Glossary'];
            if ($model->save()) {

                Yii::app()->user->setFlash('success', Yii::t('app', 'Erfasst.'));
                $this->redirect(array('update', 'id' => $sourceModel->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'sourceModel' => $sourceModel
        ));
    }

    public function actionUpdate($id) {
        $this->h1 = Yii::t('app', 'Glossar / Bearbeitung');

        $sourceModel = $this->loadModel($id);

        if (!isset($_GET['locale']) || empty($_GET['locale'])) {
            $localeId = LocaleModel::getDefault()->id;
        } else {
            $localeId = $_GET['locale'];
        }

        $model = Glossary::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $sourceModel->unit_hash, ':locale' => $localeId));
        if (empty($model)) {
            $model = new Glossary();
        }

        if ($model->isNewRecord) {
            $model->attributes = $sourceModel->attributes;
            $model->locale_id = $localeId;
        }

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Glossary'])) {
            $model->attributes = $_POST['Glossary'];
            if ($model->save()) {

                Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                $this->redirect(array('update', 'id' => $sourceModel->id, 'locale' => $model->locale_id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'sourceModel' => $sourceModel
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->deleteModel();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionAdmin() {
        $this->h1 = Yii::t('app', 'Glossar');

        $model = new Glossary('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Glossary']))
            $model->attributes = $_GET['Glossary'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = Glossary::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'glossary-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
