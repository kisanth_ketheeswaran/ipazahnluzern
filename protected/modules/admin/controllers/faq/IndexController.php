<?php

class IndexController extends BackendController {

    public function actionCreate() {
        $this->h1 = Yii::t('app', 'FAQ / Erfassung');
        $model = new Faq;
        $sourceModel = $model;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Faq'])) {
            $model->attributes = $_POST['Faq'];
            $model->sort_order = $model->sort_order ? $model->sort_order : 0;

            if ($model->save()) {


                Yii::app()->user->setFlash('success', Yii::t('app', 'Erfasst.'));
                $this->redirect(array('update', 'id' => $sourceModel->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'sourceModel' => $sourceModel
        ));
    }

    public function actionUpdate($id) {
        $this->h1 = Yii::t('app', 'FAQ / Bearbeitung');

        $sourceModel = $this->loadModel($id);

        if (!isset($_GET['locale']) || empty($_GET['locale'])) {
            $localeId = LocaleModel::getDefault()->id;
        } else {
            $localeId = $_GET['locale'];
        }

        $model = Faq::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $sourceModel->unit_hash, ':locale' => $localeId));
        if (empty($model)) {
            $model = new Faq();
        }

        if ($model->isNewRecord) {
            $model->attributes = $sourceModel->attributes;
            $model->locale_id = $localeId;
        }

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Faq'])) {
            $model->attributes = $_POST['Faq'];
            $model->sort_order = $model->sort_order ? $model->sort_order : 0;
            if ($model->save()) {

                Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                $this->redirect(array('update', 'id' => $sourceModel->id, 'locale' => $model->locale_id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'sourceModel' => $sourceModel
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->deleteModel();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionAdmin() {
        $this->h1 = Yii::t('app', 'FAQ');

        $model = new Faq('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Faq']))
            $model->attributes = $_GET['Faq'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = Faq::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'faq-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionCreateGroup() {
        $categories = FaqGroup::model()->findAll('locale_id = :locale ORDER BY id, name ASC', array(':locale' => LocaleModel::getDefault()->id));

        if (isset($_GET['locale']) && !empty($_GET['locale'])) {
            $locale = LocaleModel::model()->findByPk($_GET['locale']);
        } else {
            $locale = LocaleModel::getDefault();
        }

        $this->h1 = Yii::t('app', 'FAQ-Kategorien');

        if (Yii::app()->request->isPostRequest) {

            if (isset($_POST['FaqGroupEdit'])) {
                foreach ($_POST['FaqGroupEdit'] as $key => $categoryEdit) {
                    foreach ($categoryEdit as $id => $value) {
                        $editCat = FaqGroup::model()->findByPk($id);

                        if (!empty($value)) {
                            $editCat->name = $value;
                            $editCat->save();
                        } else {
                            $editCat->deleteModel();
                        }
                    }
                }
            }

            if (isset($_POST['FaqGroupTranslate'])) {
                foreach ($_POST['FaqGroupTranslate'] as $key => $categoryEdit) {
                    foreach ($categoryEdit as $hash => $value) {
                        if (!empty($value)) {
                            $transCat = new FaqGroup();
                            $transCat->locale_id = $locale->id;
                            $transCat->unit_hash = $hash;
                            $transCat->name = $value;
                            $transCat->save();
                        }
                    }
                }
            }

            if (isset($_POST['FaqGroup'])) {
                foreach ($_POST['FaqGroup'] as $key => $categoryNew) {
                    foreach ($categoryNew as $id => $value) {
                        if (!empty($value)) {
                            $newCat = new FaqGroup();
                            $newCat->name = $value;
                            $newCat->unit_hash = uniqid();
                            $newCat->locale_id = $locale->id;
                            $newCat->save();
                        }
                    }
                }
            }


            Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
            $this->redirect(array('createGroup', 'locale' => $locale->id));
        }


        $this->render('createGroup', array(
            'categories' => $categories,
            'localeModel' => $locale
        ));
    }

}
