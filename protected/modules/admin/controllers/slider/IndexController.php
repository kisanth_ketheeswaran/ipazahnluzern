<?php

class IndexController extends BackendController
{
	public function actionCreate()
	{
        $this->h1 = Yii::t('app', 'Slider / Erfassung');
        
        $model=new Slider;
        $sourceModel = $model;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['Slider']))
        {
            $model->attributes=$_POST['Slider'];
            $model->sort_order = $model->sort_order ? $model->sort_order : 0;
            $model->type = 'default';
            
            $file = CUploadedFile::getInstance($model,'pic');

            if($model->save()) {
                
                if (!empty($file)) {
                    $pathinfo = pathinfo($file);
                   
                    $filenameWithoutExtension = str_replace('.' . $pathinfo['extension'], '', $file->getName());
                    $name = $model->id . '_' . $filenameWithoutExtension . '.' . $pathinfo['extension'];
                    $model->pic = $name;
                    $file->saveAs(Yii::app()->basePath . '/../images/media/' . $name);
                    $model->save();
                }
                
                
                Yii::app()->user->setFlash('success', Yii::t('app', 'Erfasst.'));
                $this->redirect(array('update','id'=>$sourceModel->id));
            }
        }

        $this->render('create',array(
            'model'=>$model,
            'sourceModel' => $sourceModel
        ));
	}

	public function actionUpdate($id)
	{
        $this->h1 = Yii::t('app', 'Slider / Bearbeitung');
        
        $sourceModel=$this->loadModel($id);
        
        if (!isset($_GET['locale']) || empty($_GET['locale'])) {
            $localeId = LocaleModel::getDefault()->id;
        } else {
            $localeId = $_GET['locale'];
        }
        
        $model = Slider::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $sourceModel->unit_hash, ':locale' => $localeId));
        if (empty($model)) {
            $model = new Slider();
        }
        
        if ($model->isNewRecord) {
            $model->attributes = $sourceModel->attributes;
            $model->pic = '';
            $model->locale_id = $localeId;   
        }

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        
        $currentFile = $model->pic;
        
        if(isset($_POST['Slider']))
        {
            $model->attributes=$_POST['Slider'];
            $model->sort_order = $model->sort_order ? $model->sort_order : 0;
            $model->type = 'default';
            
            $file = CUploadedFile::getInstance($model,'pic');            
            
            if($model->save()) {
                
                
                if (!empty($file)) {
                    
                    App::deleteFile($currentFile, 'slider');
                    $pathinfo = pathinfo($file);
                   
                    $filenameWithoutExtension = str_replace('.' . $pathinfo['extension'], '', $file->getName());
                    $name = $model->id . '_' . $filenameWithoutExtension . '.' . $pathinfo['extension'];
                    $model->pic = $name;
                    $file->saveAs(Yii::app()->basePath . '/../images/media/' . $name);
                    $model->save();
                } else {
                    $model->pic = $currentFile;
                    $model->save();
                }
                
                Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                $this->redirect(array('update','id'=>$sourceModel->id, 'locale' => $model->locale_id));
            }
        }

        $this->render('update',array(
            'model'=>$model,
            'sourceModel' => $sourceModel
        ));
	}

    public function actionDeleteFile()
    {
        if (Yii::app()->request->isPostRequest) {
            $id = $_POST['id'];
            $model = Slider::model()->findByPk($id);
            App::deleteFile($model->pic);
            $model->pic = '';
            $model->save();
        }
    }

    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->deleteModel();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

	public function actionAdmin()
	{
        $this->h1 = Yii::t('app', 'Slider');
        $model=new Slider('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Slider']))
            $model->attributes=$_GET['Slider'];

        $this->render('admin',array(
            'model'=>$model,
        ));
	}

	public function loadModel($id)
	{
		$model=Slider::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='slider-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
