<?php

class IndexController extends BackendController {

    public function actionCreate() {
        $this->h1 = Yii::t('app', 'Kunde / Erfassung');
        $model = new Kunde;
        $sourceModel = $model;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Kunde'])) {
            $model->attributes = $_POST['Kunde'];
            $file = CUploadedFile::getInstance($model, 'bild');
            $lca = Yii::app()->language;
            $model->position =(int) $model->checkPosition($model, $lca);
            if ($model->save()) {

                if (!empty($file)) {
                    $pathinfo = pathinfo($file);
                    $filenameWithoutExtension = str_replace('.' . $pathinfo['extension'], '', $file->getName());
                    $name = 'kunde_' . $model->id . '.' . $pathinfo['extension'];
                    $model->bild = $name;
                    $file->saveAs(Yii::app()->basePath . '/../images/media/kunde/' . $name);
                    $model->save();
                }

                Yii::app()->user->setFlash('success', Yii::t('app', 'Erfasst.'));
                $this->redirect(array('update', 'id' => $sourceModel->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'sourceModel' => $sourceModel
        ));
    }

    public function actionUpdate($id) {
        $this->h1 = Yii::t('app', 'Kunde / Bearbeitung');

        $sourceModel = $this->loadModel($id);

        if (!isset($_GET['locale']) || empty($_GET['locale'])) {
            $localeId = LocaleModel::getDefault()->id;
        } else {
            $localeId = $_GET['locale'];
        }

        $model = Kunde::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $sourceModel->unit_hash, ':locale' => $localeId));
        if (empty($model)) {
            $model = new Kunde();
        }

        if ($model->isNewRecord) {
            $model->attributes = $sourceModel->attributes;
            $model->locale_id = $localeId;
        }

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        $currentFile = $model->bild;
        if (isset($_POST['Kunde'])) {
            $model->attributes = $_POST['Kunde'];
            $file = CUploadedFile::getInstance($model, 'bild');
            $lca = Yii::app()->language;
            $model->position = $model->checkPosition($model, $lca);
            if ($model->save()) {
                if (!empty($file)) {
                    App::deleteFile($currentFile, 'media/kunde');
                    $pathinfo = pathinfo($file);
                    $filenameWithoutExtension = str_replace('.' . $pathinfo['extension'], '', $file->getName());
                    $name = 'kunde_' . $model->id . '.' . $pathinfo['extension'];
                    $model->bild = $name;
                    $file->saveAs(Yii::app()->basePath . '/../images/media/kunde/' . $name);
                    $model->save();
                } else {
                    $model->bild = $currentFile;
                    $model->save();
                }

                Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                $this->redirect(array('update', 'id' => $sourceModel->id, 'locale' => $model->locale_id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'sourceModel' => $sourceModel
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = $this->loadModel($id);
            $model->delete();
            App::deleteFile($model->bild,'media/kunde');
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }
    
    public function actionDeleteBild()
    {
        if (Yii::app()->request->isPostRequest) {
            $id = $_POST['id'];
            $model = Kunde::model()->findByPk($id);
            App::deleteFile($model->bild,'media/kunde');
            $model->bild = '';
            $model->save();
        }
    }

    public function actionAdmin() {
        $this->h1 = Yii::t('app', 'Kunde');

        $model = new Kunde('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Kunde']))
            $model->attributes = $_GET['Kunde'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = Kunde::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'kunde-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
