<?php

class IndexController extends BackendController
{
	public function actionCreate()
	{
	    $this->h1 = Yii::t('app', 'Gallery / Erfassung');
        
		$model=new Gallery;
        $sourceModel = $model;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Gallery']))
		{
			$model->attributes=$_POST['Gallery'];

            if($model->save()) {
                
                Yii::app()->user->setFlash('success', Yii::t('app', 'Erfasst.'));
                $this->redirect(array('update','id'=>$sourceModel->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
			'sourceModel' => $sourceModel
		));
	}

	public function actionUpdate($id)
	{
	    $this->h1 = Yii::t('app', 'Gallery / Bearbeitung');
        
        $sourceModel=$this->loadModel($id);
        
        if (!isset($_GET['locale']) || empty($_GET['locale'])) {
            $localeId = LocaleModel::getDefault()->id;
        } else {
            $localeId = $_GET['locale'];
        }
        
        $model = Gallery::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $sourceModel->unit_hash, ':locale' => $localeId));
        if (empty($model)) {
            $model = new Gallery();
        }
        
        if ($model->isNewRecord) {
            $model->attributes = $sourceModel->attributes;
            $model->locale_id = $localeId;   
        }

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
        
        
		if(isset($_POST['Gallery']))
		{
			$model->attributes=$_POST['Gallery'];
                  
            
			if($model->save()) {
                
                Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                $this->redirect(array('update','id'=>$sourceModel->id, 'locale' => $model->locale_id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'sourceModel' => $sourceModel
		));
	}

	public function actionDelete($id)
	{
		?>
			<script type="text/javascript">
				console.log('mauiwaui');
			</script>
		<?php
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->deleteModel();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionAdmin()
	{
	    $this->h1 = Yii::t('app', 'Galerie');
		$model=new Gallery('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Gallery']))
			$model->attributes=$_GET['Gallery'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionView($id)
	{
		$this->h1 = Yii::t('app', '');
		$model = $this->loadModel($id);
		$sourceModel = $model;
		$this->render('view', array(
			'model'=>$model,
			'sourceMOdel'=> $sourceModel,
		));
	}
	
	public function loadModel($id)
	{
		$model=Gallery::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function loadGalleryElements($id) {
		$results;
		foreach (Gallery_elements::model()->find('gallery_id = :id', array(':id = $id')) as $element) {
			$results[] = $element;
		}
		return $results;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gallery-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
		public function actionError()
{
    if($error=Yii::app()->errorHandler->error)
        $this->render('error', $error);
}

public function actionUpload()
{
        Yii::import("ext.EAjaxUpload.qqFileUploader");
        $folder='images/media/gallery/';// folder for uploaded files
        $allowedExtensions = array("jpg","gif", "png", "jpeg");//array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = 10 * 1024 * 1024;// maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
 
        $fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
        $fileName=$result['filename'];//GETTING FILE NAME
        
        if ($result['success']) {
	        $galleryElement = new GalleryElement;
	        $galleryElement->description = '';
	        $galleryElement->unit_hash = uniqid();
	        $galleryElement->locale_id = Yii::app()->language;
	        $galleryElement->position = '99';
	        $galleryElement->image = $fileName;
	        $galleryElement->gallery_id = $_GET['galleryID'];
	        $galleryElement->save();
        }
        echo $return;// it's array
        
}

public function actionRefreshPic() {
	$gallery_id = $_GET['galleryID'];
	$gallery = Gallery::model()->find('id = :id', array(':id' => $gallery_id));
	$this->renderPartial('refreshPic', array(
            'pics' => $gallery->getElements()
        ));
}

public function actionDeletePic()
    {
        $galleryPic = GalleryElement::model()->findByPk($_GET['pic']);
        App::deleteFile($galleryPic->image, 'media/gallery');
        $galleryPic->delete();
    }
    
    public function actionUpdatePic()
    {
        $galleryPic = GalleryElement::model()->findByPk($_GET['pic']);
        $galleryPicPost = $_POST['GalleryPic'][$galleryPic->id];
        
        $galleryPic->description = $galleryPicPost['description'];
        $galleryPic->position = $galleryPicPost['position'];
        
        $galleryPic->save();

    }

}