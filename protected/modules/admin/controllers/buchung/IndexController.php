<?php

class IndexController extends BackendController {

    private $valOk = true;

    public function actionCreate() {
        $this->h1 = Yii::t('app', 'Buchung / Erfassung');
        $model = new Buchung;
        $terminplan = Terminplan::model()->with('behandlungsarts')->findAll();
        $behandlungsart = Behandlungsart::model()->findAll(array('order' => 'terminplan_idfs'));
        $sourceModel = $model;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Buchung'])) {
            $model->attributes = $_POST['Buchung'];
            if ($model->save()) {

                Yii::app()->user->setFlash('success', Yii::t('app', 'Erfasst.'));
                $this->redirect(array('update', 'id' => $sourceModel->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'sourceModel' => $sourceModel,
            'terminplan' => $terminplan,
            'behandlungsart' => $behandlungsart,
        ));
    }

    public function actionUpdate($id) {
        $this->h1 = Yii::t('app', 'Buchung / Bearbeitung');

        $sourceModel = Buchung::model()->with('terminplanIdfs', 'behandlungsart')->findByPk($id);

        $terminplan = Terminplan::model()->findAll();
        $behandlungsart = Behandlungsart::model()->with('buchungbehandlungs')->findAll(array('order' => 'terminplan_idfs'));

        $model = Buchung::model()->with('terminplanIdfs', 'behandlungsart')->findByPk($sourceModel->id);
        if (empty($model)) {
            $model = new Buchung();
        }

        if ($model->isNewRecord) {
            $model->attributes = $sourceModel->attributes;
        }

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);

        if (isset($_POST['Buchung']) && !empty($_POST['Behandlungsart']) && isset($_POST['Terminplan'])) {
            $behandlung = $_POST['Behandlungsart'];
            $model->attributes = $_POST['Buchung'];
            $model->terminplan_idfs = $_POST['Terminplan']['nachname'];
            if ($model->save()) {
                if ($model->bestatigung == 1) {
                    if ($this->sendeMailAnPatient($model->id)) {
                        Yii::app()->user->setFlash('success', Yii::t('app', 'Die E-Mail an den Patienten wurde erfolgreich geschickt'));
                    }
                }
                if (is_array($behandlung)) {
                    Buchungbehandlung::model()->speicherBuchungBehandlung($model->id, $behandlung);
                }

                Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                $this->redirect(array('update', 'id' => $sourceModel->id));
            }
        }
        $behandlung = Buchungbehandlung::model()->with('behandlungsartIdfs', 'buchungIdfs')->findAll('buchung_idfs =:id', array(':id' => $model->id));

        $this->render('update', array(
            'model' => $model,
            'sourceModel' => $sourceModel,
            'terminplan' => $terminplan,
            'behandlungsart' => $behandlungsart,
            'buchBehandlung' => $behandlung,
        ));
    }

    public function actionTerminEintragen($id) {
        $status = $_POST['Buchung']['status'];
        $statusName = Buchung::buchungStatus($status);
        $buchung = Buchung::model()->findByPk($id);
        $termin = new Termin;
        $termin->attributes = $_POST['Buchung'];
        $termin->terminplan_idfs = $_POST['Terminplan']['nachname'];
        $behandlungen = $_POST['Behandlungsart'];
        if ($status == "bestaetigt") {
            if ($termin->save()) {
                foreach ($behandlungen as $behandlung) {
                    $buchungBehandlung = new Behandlung;
                    $buchungBehandlung->behandlungsart_idfs = $behandlung;
                    $buchungBehandlung->termin_idfs = $termin->id;
                    $buchungBehandlung->save();
                }
                if ($buchung->bestatigung == 1) {
                    if ($this->sendeBestatigung($buchung->id)) {
                        Yii::app()->user->setFlash('success', Yii::t('app', 'Die E-Mail an den Patienten wurde erfolgreich geschickt'));
                    }
                }
                //$buchung->delete();
                Yii::app()->user->setFlash('success', Yii::t('app', 'Die Buchung wurde als Termin gespeichert. Jetzt sind Sie auf der Terminansicht'));
                $this->redirect(array('/admin/termin/index/update', 'id' => $termin->id));
            }
        } elseif ($status == "abgesagt") {
            //$buchung->delete();
            $this->redirect(array('/admin/buchung/index/admin'));
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = Buchung::model()->findByPk($id);
            Buchungbehandlung::model()->deleteAll('buchung_idfs = :id', array(':id' => $model->id));
            $model->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionAdmin($aka = "leer") {
        $this->h1 = Yii::t('app', 'Buchung');
        $model = new Buchung('search');
        $model->unsetAttributes();  // clear any default values
        if (empty($_POST['Terminplan']['nachname'])) {
            $nachname = "";
        } else {
            $nachname = $_POST['Terminplan']['nachname'];
        }
        if (isset($_GET['Buchung']))
            $model->attributes = $_GET['Buchung'];

        $this->render('admin', array(
            'model' => $model,
            'nachname' => $nachname,
        ));
    }

    public function loadModel($id) {
        $model = Buchung::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'buchung-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function sendeMailAnPatient($buchungId) {
        require_once Yii::app()->basePath . '/extensions/PHPMailer/class.phpmailer.php';
        $mail = new PHPMailer;
        $buchung = Buchung::model()->with('terminplanIdfs', 'behandlungsart')->findByPk($buchungId);

        $mail->From = $buchung->terminplanIdfs->email;
        $mail->FromName = $buchung->terminplanIdfs->full_name;
        $mail->addAddress($buchung->email);
        //$mail->addAddress('m.schnueriger@firegroup.com');   // Add a recipient

        $mail->Subject = 'Terminbuchung' . $buchung->vorname . " " . $buchung->nachname;
        $mail->Body = "Gewünschte Behandlerr: " . $buchung->terminplanIdfs->full_name;
        $mail->Body .= " \n Gewünschte Behandlung: ";
        foreach ($buchung->behandlungsart as $behandlung) {
            $mail->Body .= $behandlung->behandlung . ", ";
        }

        $mail->Body .=" \n Gewünschter Termin: " . $buchung->datum . " \n " .
                "Persönliche Daten: " . $buchung->anrede . " " . $buchung->vorname . " " . $buchung->nachname . " ";
        if (isset($buchung->geburtsdatum)) {
            $mail->Body .= $buchung->geburtsdatum;
        }
        $mail->Body .= $buchung->strasse . ", " . $buchung->plz . " " . $buchung->ort . ", Tel: " . $buchung->telefon . "," .
                $buchung->email;
        $mail->Body .= " \n Bemerkung: " . $buchung->bemerkung;
        $mail->Body .=$buchung->terminerinnerung == 1 ? " \n Terminerinnerung: Ja" : " \n Terminerinnerung: Nein";
        $mail->Body .=$buchung->bestatigung == 1 ? " \n Bestätigungamail: Ja" : " \n Bestätigungamail: Nein";
        $mail->Body .= " \n Freundliche Grüsse \n Ihr Behandler " . $buchung->terminplanIdfs->full_name;

        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        if (!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }
    
    public function sendeBestatigung($buchungId) {
        require_once Yii::app()->basePath . '/extensions/PHPMailer/class.phpmailer.php';
        $mail = new PHPMailer;
        $buchung = Buchung::model()->with('terminplanIdfs', 'behandlungsart')->findByPk($buchungId);

        $mail->From = $buchung->terminplanIdfs->email;
        $mail->FromName = $buchung->terminplanIdfs->full_name;
        $mail->addAddress($buchung->email);
        //$mail->addAddress('m.schnueriger@firegroup.com');   // Add a recipient

        $mail->Subject = 'Terminbestätgung' . $buchung->vorname . " " . $buchung->nachname;
         $mail->Body .= "Gerne bestätigen Ich Ihre Buchungsanfrage.";
        $mail->Body .= "Gewünschte Behandlerr: " . $buchung->terminplanIdfs->full_name;
        $mail->Body .= " \n Gewünschte Behandlung: ";
        foreach ($buchung->behandlungsart as $behandlung) {
            $mail->Body .= $behandlung->behandlung . ", ";
        }

        $mail->Body .=" \n Gewünschter Termin: " . $buchung->datum . " \n " .
                "Persönliche Daten: " . $buchung->anrede . " " . $buchung->vorname . " " . $buchung->nachname . " ";
        if (isset($buchung->geburtsdatum)) {
            $mail->Body .= $buchung->geburtsdatum;
        }
        $mail->Body .= $buchung->strasse . ", " . $buchung->plz . " " . $buchung->ort . ", Tel: " . $buchung->telefon . "," .
                $buchung->email;
        $mail->Body .= " \n Bemerkung: " . $buchung->bemerkung;
        $mail->Body .=$buchung->terminerinnerung == 1 ? " \n Terminerinnerung: Ja" : " \n Terminerinnerung: Nein";
        $mail->Body .=$buchung->bestatigung == 1 ? " \n Bestätigungamail: Ja" : " \n Bestätigungamail: Nein";
        $mail->Body .= " \n Ich freue mich Sie zu treffen. \n Freundliche Grüsse \n Ihr Behandler " . $buchung->terminplanIdfs->full_name;

        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        if (!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }

}
