<?php

class IndexController extends BackendController {

    public function actionCreate() {
        $this->h1 = Yii::t('app', 'Homeslider / Erfassung');
        $model = new Homeslider;
        $sourceModel = $model;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Homeslider'])) {
            $model->attributes = $_POST['Homeslider'];
            $model->bild = CUploadedFile::getInstance($model, 'bild');
            $imgName = $model->bild;
            $lca = Yii::app()->language;
            $model->position= $model->checkPosition($model,$lca);
            if ($model->save()) {
                if (!empty($model->bild))
                    $model->bild->saveAs(Yii::app()->basePath . '/../images/media/homeslider/' . $imgName);

                Yii::app()->user->setFlash('success', Yii::t('app', 'Erfasst.'));
                $this->redirect(array('update', 'id' => $sourceModel->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'sourceModel' => $sourceModel
        ));
    }

    public function actionUpdate($id) {
        $this->h1 = Yii::t('app', 'Homeslider / Bearbeitung');

        $sourceModel = $this->loadModel($id);

        if (!isset($_GET['locale']) || empty($_GET['locale'])) {
            $localeId = LocaleModel::getDefault()->id;
        } else {
            $localeId = $_GET['locale'];
        }

        $model = Homeslider::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $sourceModel->unit_hash, ':locale' => $localeId));
        if (empty($model)) {
            $model = new Homeslider();
        }

        if ($model->isNewRecord) {
            $model->attributes = $sourceModel->attributes;
            $model->locale_id = $localeId;
        }

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Homeslider'])) {
            $model->attributes = $_POST['Homeslider'];
            $model->bild = CUploadedFile::getInstance($model, 'bild');
            $imgName = $model->bild;
            $i = true;
            if (empty($imgName)) {
                if ($sourceModel->bild != 'noPicture') {
                    $hello = $sourceModel->bild;
                    $model->bild = $hello;
                    $i = false;
                }
            }
            $model->position= $model->checkPosition($model,$model->locale_id);
            if ($model->save()) {
                if ($i == true)
                    $model->bild->saveAs(Yii::app()->basePath . '/../images/media/homeslider/' . $imgName);

                Yii::app()->user->setFlash('success', Yii::t('app', 'Gespeichert.'));
                $this->redirect(array('update', 'id' => $sourceModel->id, 'locale' => $model->locale_id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'sourceModel' => $sourceModel
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = $this->loadModel($id);
            $model->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionDeleteFile() {
        if (Yii::app()->request->isPostRequest) {
            $id = $_POST['id'];
            $model = Homeslider::model()->findByPk($id);
            App::deleteFile($model->bild, 'media/homeslider');
            $model->bild = 'noPicture';
            $model->save();
        }
    }

    public function actionAdmin() {
        $this->h1 = Yii::t('app', 'Homeslider');

        $model = new Homeslider('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Homeslider']))
            $model->attributes = $_GET['Homeslider'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = Homeslider::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'homeslider-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
