<?php 

class WidgetAdminContactForm extends CWidget
{
  public $localeId;
  public $target;
  public $cmsWidget;
  public function init()
  {
    $this->cmsWidget = CmsWidgetContactForm::model()->find('target_id = :target', array(':target' => $this->target->id));
  }
  
  public function run()
  {
    $this->render('adminContactForm');
  }
}
