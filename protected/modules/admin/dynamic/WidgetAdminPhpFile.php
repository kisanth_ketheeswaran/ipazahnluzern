<?php 

class WidgetAdminPhpFile extends CWidget
{
  public $localeId;
  public $target;
  public $cmsWidget;
  public function init()
  {
    $this->cmsWidget = CmsWidgetPhpFile::model()->find('target_id = :target', array(':target' => $this->target->id));
  }
  
  public function run()
  {
    $this->render('adminPhpFile');
  }
}
