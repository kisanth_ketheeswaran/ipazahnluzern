<div id="container-<?php echo get_class($this); ?>" style="display: none;">
<?php 
	if(is_object($this->cmsWidget))
  		$isChecked = $this->cmsWidget->value ? true : false;
	else 
		$isChecked = false;
	
	$position = is_object($this->cmsWidget) ? $this->cmsWidget->position : null;
?>
		<div class="widgetrow">
				<?php echo CHtml::checkBox('Widget[ContactForm][value]', $isChecked, array('value' => '1')); ?>
				<label for="Widget_ContactForm_value"><?php echo Yii::t('app', 'Aktivieren'); ?></label>
		</div>
		<div class="clearBoth"></div>
		<div class="widgetrow">
		    <label for="Widget_ContactForm_position"><?php echo Yii::t('app', 'Position'); ?></label>
        <?php echo CHtml::textField('Widget[ContactForm][position]', $position); ?>		    
		</div>
</div>