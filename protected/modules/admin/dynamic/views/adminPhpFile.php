<?php
	$value = is_object($this->cmsWidget) ? $this->cmsWidget->value : null;
	$position = is_object($this->cmsWidget) ? $this->cmsWidget->position : null;
?>
<div id="container-<?php echo get_class($this); ?>" style="display: none;">
    <div class="widgetrow">
        <label for="Widget_PhpFile_value"><?php echo Yii::t('app', 'Datei'); ?></label>
        <?php echo CHtml::textField('Widget[PhpFile][value]', $value); ?>
    </div>
    <div class="widgetrow">
        <label for="Widget_PhpFile_position"><?php echo Yii::t('app', 'Position'); ?></label>
        <?php echo CHtml::textField('Widget[PhpFile][position]', $position); ?>        
    </div>
</div>