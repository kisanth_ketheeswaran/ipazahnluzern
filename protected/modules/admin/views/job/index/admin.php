<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="root">
  <?php echo CHtml::link(Yii::t('app', 'Job hinzufügen'), array('/admin/job/index/create'), array('class' => 'add')); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'job-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'summaryText' => '',
	'columns'=>array(
		'label',
		array(
            'name' => 'sort_order',
        ),
        array(
            'name' => 'status',
            'filter' => Job::listDataStatus(),
            'value' => 'Job::listDataStatus($data->status)'
        ),
		/*
		'status',
		'locale_id',
		'unit_hash',
		*/
        array(
            'class'=>'CButtonColumn',
            'template' => '{delete} {update}'
        ),
	),
)); ?>
