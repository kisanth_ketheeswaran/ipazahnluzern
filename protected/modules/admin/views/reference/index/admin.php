<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="root">
  <?php echo CHtml::link(Yii::t('app', 'Portfolio hinzufügen'), array('/admin/reference/index/create'), array('class' => 'add')); ?>
  <?php echo CHtml::link(Yii::t('app', 'Thema hinzufügen'), array('/admin/reference/index/createTopic'), array('class' => 'add')); ?>
  <?php echo CHtml::link(Yii::t('app', 'Branche hinzufügen'), array('/admin/reference/index/createBusiness'), array('class' => 'add')); ?>
  <?php echo CHtml::link(Yii::t('app', 'Technologie hinzufügen'), array('/admin/reference/index/createTechnology'), array('class' => 'add')); ?>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'reference-grid',
	'dataProvider'=>$model->search($_GET['switchlang']),
	'filter'=>$model,
	'summaryText' => '',
	'columns'=>array(
		'title',
		'company',
		array(
            'name' => 'sort_order',
            'headerHtmlOptions' => array(
                'style' => 'width: 70px'
            ),
            'htmlOptions' => array(
                'style' => 'text-align: center'
            )
        ),
		array(
            'name' => 'status',
            'value' => 'Reference::statusDataList($data->status)',
            'headerHtmlOptions' => array(
                'style' => 'width: 70px'
            ),
            'htmlOptions' => array(
                'style' => 'text-align: center'
            ),            
            'filter' => Reference::statusDataList()
        ),
		/*
		'reference_business_id',
		'technology',
		'website_url',
		'website_url_title',
		'facebook_url',
		'googleplus_url',
		'sort_order',
		'date',
		'url',
		'background_color',
		'font_color',
		*/
        array(
            'class'=>'CButtonColumn',
            'template' => '{delete} {update} {view}'
        ),
	),
)); ?>
