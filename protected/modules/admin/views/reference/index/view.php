<div class="toolbar">
  <?php echo CHtml::link(Yii::t('app', 'Zurück'), array('admin'), array('class' => 'back'));  ?>
  <?php echo CHtml::link(Yii::t('app', 'Bearbeiten'), array('update', 'id' => $sourceModel->id, 'locale' => $model->locale_id), array('class' => 'edit'));  ?>
</div>



<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>


<div class="floatLeft" style="width: 450px;">
    <div class="form">
        <div class="locale">
          <ul>
              <?php foreach (LocaleModel::model()->findAll(array('order' => 'position ASC')) as $locale) : ?>
                  <?php if ($model->getModel($locale->id)) : ?>
                  <li class="<?php echo $locale->cssStatus($_GET['locale']); ?>"><?php echo CHtml::link($locale->label, array('reference/index/view', 'id' => $sourceModel->id, 'locale' => $locale->id)); ?></li>
                  <?php endif; ?>
            <?php endforeach; ?>
            </ul>
            <div class="line"></div>
        </div>
    </div>
    
    
    <?php $this->widget('zii.widgets.CDetailView', array(
    	'data'=>$model,
    	'attributes'=>array(
    		'sort_order',
    		'date',
    		'title',
    		'company',
    		'description',
    		array(
                'name' => 'reference_topic_id',
                'value' => $model->getTopicsString()
            ),
            array(
                'name' => 'reference_business_id',
                'value' => $model->getBusinessesString()
            ),
            array(
                'name' => 'reference_technology_id',
                'value' => $model->getTechnologiesString()
            ),
            'links',            
    		array(
                'name' => 'status',
                'value' => Faq::listDataStatus(),
            )
    	),
    )); ?>
</div>

<div class="floatRight" style="width: 505px;">
    
    <?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
          array(
            'id' => 'uploadPortfolio',
            'config' => array(
                'action' => '/admin/reference/index/uploadPortfolio?portfolio=' . $sourceModel->unit_hash,
                'allowedExtensions' => array('jpg', 'jpeg', 'gif', 'png'),
                'sizeLimit' => 7*1024*1024,
                'minSizeLimi' => 0,
                'multiple' => true,
                'buttonLabel' => Yii::t('app', 'Bilder hochladen'),
                'onComplete' => 'js:function() { app_refreshPics(); }'
            )
    )); ?>
    
    <div class="clearBoth"></div>
    
    <div id="picsContainer"></div>
</div> 


<script type="text/javascript">
<!--
    app_refreshPics();
    
    function app_refreshPics()
    {
        jQuery.ajax({
           url: '/admin/reference/index/refreshPic?portfolio=<?php echo $sourceModel->unit_hash; ?>',
           type: 'get',
           success: function(data) {
                jQuery('#picsContainer').html(data);               
           } 
        });
    }
    
//-->
</script>
