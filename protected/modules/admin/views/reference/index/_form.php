<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reference-form',
	'enableAjaxValidation'=>true,
)); ?>


    <?php if (!$sourceModel->isNewRecord) : ?>
    <div class="locale">
      <ul>
          <?php foreach (LocaleModel::model()->findAll(array('order' => 'position ASC')) as $locale) : ?>
              <li class="<?php echo $locale->cssStatus($_GET['locale']); ?>"><?php echo CHtml::link($locale->label, array('reference/index/update', 'id' => $sourceModel->id, 'locale' => $locale->id)); ?></li>
          <?php endforeach; ?>
        </ul>
        <div class="line"></div>
    </div>
    <?php endif; ?>

    <?php if ($sourceModel->isNewRecord) : ?>
    <div class="row">
        <?php echo $form->labelEx($model,'locale_id'); ?>
        <?php
            if ($sourceModel->isNewRecord) {
                //$model->locale_id = $_GET['lang'];
                $model->locale_id = LocaleModel::getDefault()->id;               
            }
        ?>
        <p class="left" style="margin: 0; padding-top: 3px; font-weight: bold;"><?php echo LocaleModel::model()->findByPk($model->locale_id)->label; ?></p>
    </div>
    <?php endif; ?>

    <div class="row">
        <?php echo $form->hiddenField($model,'locale_id',array('value' => $model->locale_id)); ?>
        <?php echo $form->hiddenField($model,'unit_hash',array('value' => $sourceModel->isNewRecord ? uniqid() : $sourceModel->unit_hash)); ?>
        <?php echo $form->labelEx($model,'sort_order'); ?>
        <?php echo $form->textField($model,'sort_order'); ?>
        <?php echo $form->error($model,'sort_order'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'date'); ?>
        <?php           
            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'model'=>$model,
                'attribute'=>'date',
                'options'=>array('changeMonth' => true, 'changeYear' => true, 'dateFormat' => 'dd.mm.yy'),
            ));
        ?>
        <?php echo $form->error($model,'date'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'company'); ?>
		<?php echo $form->textField($model,'company',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'company'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<div class="floatLeft">
		    <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
            <script type="text/javascript">
                CKEDITOR.replace('Reference[description]', { 
                    'height': 240,
                    'width': 780
                });
            </script>
		</div>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'reference_topic_id'); ?>
        <div class="checkboxRadio">
            <?php echo $form->checkBoxList($model,'reference_topic_id', ReferenceTopic::listData($model)); ?>
            <?php echo $form->error($model,'reference_topic_id'); ?>
        </div>
        
        <?php if ($sourceModel->id == $model->id) : ?>
            <div class="clearBoth"></div>
            <div class="reference_new_topic" style="margin-left: 155px; margin-top: 5px;">
                <?php echo CHtml::textField('ReferenceTopic[]', null, array('style' => 'clear: both; display: block; margin-bottom: 5px;', 'id' => 'Reference_new_topic_' . uniqid())); ?>
            </div>
       <?php endif; ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'reference_business_id'); ?>
        <div class="checkboxRadio">
            <?php echo $form->checkBoxList($model,'reference_business_id', ReferenceBusiness::listData($model)); ?>
            <?php echo $form->error($model,'reference_business_id'); ?>
        </div>
        
        <?php if ($sourceModel->id == $model->id) : ?>
            <div class="clearBoth"></div>
            <div class="reference_new_business" style="margin-left: 155px; margin-top: 5px;">
                <?php echo CHtml::textField('ReferenceBusiness[]', null, array('style' => 'clear: both; display: block; margin-bottom: 5px;', 'id' => 'Reference_new_business_' . uniqid())); ?>
            </div>
       <?php endif; ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'reference_technology_id'); ?>
        <div class="checkboxRadio">
            <?php echo $form->checkBoxList($model,'reference_technology_id', ReferenceTechnology::listData($model)); ?>
            <?php echo $form->error($model,'reference_technology_id'); ?>
        </div>
        
        <?php if ($sourceModel->id == $model->id) : ?>
            <div class="clearBoth"></div>
            <div class="reference_new_technology" style="margin-left: 155px; margin-top: 5px;">
                <?php echo CHtml::textField('ReferenceTechnology[]', null, array('style' => 'clear: both; display: block; margin-bottom: 5px;', 'id' => 'Reference_new_technology_' . uniqid())); ?>
            </div>
       <?php endif; ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'links'); ?>
        <?php echo $form->textArea($model,'links'); ?>
        <p class="hint"><?php echo Yii::t('app', 'Pro Zeile ein Link im Format: Name__www.url.com'); ?></p>
        <?php echo $form->error($model,'links'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status', Reference::statusDataList()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>

	<div class="row buttons">
	    <label for=""></label>
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Erfassen') : Yii::t('app', 'Speichern')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
<!--
    jQuery('input[name="ReferenceTopic[]"]').live('blur', function(event) {
       var $this = jQuery(this);
       var $lastElement = $this.parent().children('input[name="ReferenceTopic[]"]:last-child');
       var countElements = $this.parent().children().length;
       if ($this.val() == '' && countElements > 1 && $this.attr('id') != $lastElement.attr('id')) {
           $this.remove();
       }
       
       if ($this.val() != '' && $this.attr('id') == $lastElement.attr('id')) {
           var uniqid = Math.floor(Math.random() * 1000);
           var $newInput = jQuery('<input style="display: block; clear: both; margin-bottom: 5px;" type="text" name="ReferenceTopic[]" id="Reference_new_topic_' + uniqid + '" />');
           $this.parent().append($newInput);
           $newInput.focus();
       }
       
    });
    
    jQuery('input[name="ReferenceBusiness[]"]').live('blur', function(event) {
       var $this = jQuery(this);
       var $lastElement = $this.parent().children('input[name="ReferenceBusiness[]"]:last-child');
       var countElements = $this.parent().children().length;
       if ($this.val() == '' && countElements > 1 && $this.attr('id') != $lastElement.attr('id')) {
           $this.remove();
       }
       
       if ($this.val() != '' && $this.attr('id') == $lastElement.attr('id')) {
           var uniqid = Math.floor(Math.random() * 1000);
           var $newInput = jQuery('<input style="display: block; clear: both; margin-bottom: 5px;" type="text" name="ReferenceBusiness[]" id="Reference_new_business_' + uniqid + '" />');
           $this.parent().append($newInput);
           $newInput.focus();
       }
       
    });
//-->
</script>
