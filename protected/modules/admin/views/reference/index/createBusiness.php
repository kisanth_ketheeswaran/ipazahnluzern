<div class="toolbar">
  <?php echo CHtml::link(Yii::t('app', 'Zurück'), array('admin'), array('class' => 'back'));  ?>
</div>

<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>


<div class="form">
        <div class="locale">
          <ul>
              <?php foreach (LocaleModel::model()->findAll(array('order' => 'position ASC')) as $locale) : ?>
                  <li class="<?php echo $locale->cssStatus($_GET['locale']); ?>"><?php echo CHtml::link($locale->label, array('reference/index/createBusiness', 'locale' => $locale->id)); ?></li>
            <?php endforeach; ?>
            </ul>
            <div class="line"></div>
        </div>
        
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'media-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data')
        )); ?>
        
        <?php foreach ($categories as $category) : ?>
        <?php
            $localeCat = $category->getModel($localeModel->id, false);
        ?>    
        
        <?php if ($localeCat) : ?>   
        <div class="row">
            <?php echo $form->textField($localeCat,'name', array('name' => 'ReferenceBusinessEdit[name][' . $localeCat->id . ']', 'style' => 'width: 943px;')); ?>
        </div>
        <?php else : ?>
        <div class="row">
            <?php echo $form->textField(new ReferenceBusiness(),'name', array('value' => $category->name, 'name' => 'ReferenceBusinessTranslate[name][' . $category->unit_hash . ']', 'style' => 'width: 943px;')); ?>
        </div>            
        <?php endif; ?>
        
        
        <?php endforeach; ?>
        
        <?php if ($localeModel->id == LocaleModel::getDefault()->id) : ?>
        <div class="row">
            <?php echo $form->textField(new ReferenceBusiness(),'name', array('name' => 'ReferenceBusiness[name][]', 'style' => 'width: 943px;')); ?>
        </div>
        <?php endif; ?>
        
        <div class="row buttons">
            <?php echo CHtml::submitButton(Yii::t('app', 'Speichern')); ?>
        </div>
            
        <?php $this->endWidget(); ?>
</div>
