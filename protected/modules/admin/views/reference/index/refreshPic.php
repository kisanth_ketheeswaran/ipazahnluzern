<?php if (count($pics)) : ?>
    <table class="referencePic" cellpadding="0" cellspacing="0">
        <tr>
            <th class="name"><?php echo Yii::t('app', 'Name'); ?></th>
            <?php foreach (Reference::getTypes() as $type) : ?>
                <th><?php echo $type; ?></th>
            <?php endforeach; ?>
            <th style="text-align: center;"><?php echo Yii::t('app', 'Pos'); ?></th>
            <th class="delete">D</th>
        </tr>        
        
        <?php foreach ($pics as $pic) : ?>
        <tr class="row" data-id="<?php echo $pic->id; ?>">
            <td class="name">
                <?php $picImage = App::getPic($pic, 'pic', 'media', 145, null, $pic->label); ?>
                <a><?php echo $picImage; ?></a>
                <input type="text" name="ReferencePic[<?php echo $pic->id; ?>][label]" value="<?php echo $pic->label; ?>" />
            </td>
            <td><input type="checkbox" name="ReferencePic[<?php echo $pic->id; ?>][type][]" value="mini" <?php echo in_array('mini', $pic->type) ? 'checked="checked"' : ''; ?>" /></td>
            <td><input type="checkbox" name="ReferencePic[<?php echo $pic->id; ?>][type][]" value="thumbnail" <?php echo in_array('thumbnail', $pic->type) ? 'checked="checked"' : ''; ?>" /></td>
            <td><input type="checkbox" name="ReferencePic[<?php echo $pic->id; ?>][type][]" value="portfolio" <?php echo in_array('portfolio', $pic->type) ? 'checked="checked"' : ''; ?>" /></td>
            <td><input type="checkbox" name="ReferencePic[<?php echo $pic->id; ?>][type][]" value="background" <?php echo in_array('background', $pic->type) ? 'checked="checked"' : ''; ?>" /></td>
            <td><input type="checkbox" name="ReferencePic[<?php echo $pic->id; ?>][type][]" value="press" <?php echo in_array('press', $pic->type) ? 'checked="checked"' : ''; ?>" /></td>
            <td class="pos"><input type="text" name="ReferencePic[<?php echo $pic->id; ?>][sort_order]" value="<?php echo $pic->sort_order; ?>" /></td>
            <td><input type="radio" data-id="<?php echo $pic->id; ?>" class="deletePic" /></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>

<script type="text/javascript">
<!--
    jQuery('input[type="radio"].deletePic').click(function() {
       var $this = jQuery(this);
       
       if (confirm('<?php echo Yii::t('app', 'Wirklich löschen?'); ?>')) {
            jQuery.ajax({
                  url: '/admin/reference/index/deletePic?pic=' + jQuery(this).attr('data-id'),
                  type: 'post',
                  success: function() {
                      $this.parents('tr.row').fadeOut(150, function() {
                          jQuery(this).remove();
                      });
                  }
            });
       } else {
           $this.attr('checked', false);
       }
    });
    
    
    jQuery('.referencePic input[type="text"]').blur(function() {
       var $this = jQuery(this);
       var $trRow = $this.parents('tr.row');
       var data = 'ajax=1';
       
       $trRow.find(':input').each(function() {
           if (jQuery(this).attr('type') == 'checkbox' && jQuery(this).is(':checked')) {
               data += '&' + jQuery(this).attr('name') + '=' + jQuery(this).val();
           } else if (jQuery(this).attr('type') == 'text') {
               data += '&' + jQuery(this).attr('name') + '=' + jQuery(this).val();
           }
       });
       
       jQuery.ajax({
          url: '/admin/reference/index/updatePic?pic=' + $trRow.attr('data-id'),
          type: 'post',
          data: data,
          success: function() {
              $trRow.effect('highlight', { }, 2000);
          } 
           
       });
       
    });
    
    jQuery('.referencePic input[type="checkbox"]').click(function() {
       var $this = jQuery(this);
       var $trRow = $this.parents('tr.row');
       var data = 'ajax=1';
       
       $trRow.find(':input').each(function() {
           if (jQuery(this).attr('type') == 'checkbox' && jQuery(this).is(':checked')) {
               data += '&' + jQuery(this).attr('name') + '=' + jQuery(this).val();
           } else if (jQuery(this).attr('type') == 'text') {
               data += '&' + jQuery(this).attr('name') + '=' + jQuery(this).val();
           }
       });
       
       jQuery.ajax({
          url: '/admin/reference/index/updatePic?pic=' + $trRow.attr('data-id'),
          type: 'post',
          data: data,
          success: function() {
          } 
           
       });
       
    });
    
//-->
</script>
