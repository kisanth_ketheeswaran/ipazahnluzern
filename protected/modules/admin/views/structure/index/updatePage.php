<div class="toolbar">
  <?php echo CHtml::link(Yii::t('app', 'Zurück'), array('structure/index/update', 'id' => $structureSource->id, 'locale' => $structureLocale->locale_id), array('class' => 'back'));  ?>
</div>
<?php /* WENN SEITE LANDINGPAGE IST, DANN WIRD LANDINGPAGE FORM GELADEN*/ ?>
<?php 
	if($structureLocale->is_landing){
		$this->renderPartial('_formPage_landing', array('model'=>$model, 'modelOption' => $modelOption, 'structureSource' => $structureSource, 'structureLocale' => $structureLocale));
	}else{
		$this->renderPartial('_formPage', array('model'=>$model,'emotionModel'=>$emotionModel, 'modelOption' => $modelOption, 'structureSource' => $structureSource, /*'modelSlider' => $modelSlider,*/ 'structureLocale' => $structureLocale));
	}
?>