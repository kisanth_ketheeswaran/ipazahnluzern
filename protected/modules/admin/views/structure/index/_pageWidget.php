<h3 id="dynamicsettings" class="fields"><?php echo Yii::t('app', 'Dynamische Elemente'); ?></h3>
<div id="container-dynamicsettings" style="display: none;">
  <?php 
    foreach ($model->getWidgets() as $cmsWidget)
    {
        echo '<div class="row widget">';
        echo '<h4 id="' . $cmsWidget->name . '">' . $cmsWidget->label . '</h4>';
        $this->widget('application.modules.admin.dynamic.WidgetAdmin' . $cmsWidget->name, array('target' => $model, 'localeId' => $model->structure->locale_id));
        echo '</div>';
    }
  ?>
</div>  