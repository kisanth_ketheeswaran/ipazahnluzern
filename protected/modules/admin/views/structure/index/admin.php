<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="root">
  <p class="label"><?php echo Yii::t('app', 'Root'); ?></p>
  <?php echo CHtml::link(Yii::t('app', 'Webseite hinzufügen'), array('create', 'type' => 'site'), array('class' => 'down')); ?>
</div>

<?php echo Structure::getTreeView(Structure::getTreeStructure(0, LocaleModel::getDefault()->id)); ?>
<?php echo CHtml::script('app_structureTreeView();');