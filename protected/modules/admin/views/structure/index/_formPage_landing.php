<?php /* LANDINGPAGE */ ?>
<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="form">
  <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'page-form',
		    'htmlOptions' => array('enctype' => 'multipart/form-data')
    )); ?>

  <h3 id="sitefields" class="fields"><?php echo Yii::t('app', 'Seitenfelder'); ?></h3>
  <div id="container-sitefields">
	  <div class="row">
	    <?php echo $form->labelEx($model,'cms_h1'); ?>
	    <?php echo $form->textField($model,'cms_h1'); ?>
	    <?php echo $form->error($model,'cms_h1'); ?>
	  </div>
	
	  <div class="row">
	    <?php echo $form->labelEx($model,'cms_content'); ?>
	    <div style="float: left;"><?php echo $form->textArea($model,'cms_content'); ?></div>
	        <script type="text/javascript">
	        CKEDITOR.replace('Page[cms_content]', { 
		        'height': 240,
		        'width': 780
	      });
	    </script>
	  </div>
    <div class="row">
      <?php echo $form->labelEx($model,'cms_content_position'); ?>
      <?php echo $form->textField($model,'cms_content_position'); ?>
      <?php echo $form->error($model,'cms_content_position'); ?>
    </div>
  </div>
  
  <div class="clearBoth"></div>
  <?php $this->renderPartial('_pageOption', array('modelOption' => $modelOption, 'form' => $form)); ?>

 
  <h3 id="seooptimation" class="fields"><?php echo Yii::t('app', 'Suchmaschinenoptimierung'); ?></h3>
  <div id="container-seooptimation" style="display: none;">
	  <div class="row">
	    <?php echo $form->labelEx($model,'meta_title'); ?>
	    <?php echo $form->textField($model,'meta_title'); ?>
	    <?php echo $form->error($model,'meta_title'); ?>
	  </div>
	
	  <div class="row">
	    <?php echo $form->labelEx($model,'meta_keywords'); ?>
	    <?php echo $form->textArea($model,'meta_keywords'); ?>
	    <?php echo $form->error($model,'meta_keywords'); ?>
	  </div>
	    
	  <div class="row">
	    <?php echo $form->labelEx($model,'meta_description'); ?>
	    <?php echo $form->textArea($model,'meta_description'); ?>
	    <?php echo $form->error($model,'meta_description'); ?>
	  </div>
  </div>
	
  <h3 id="expertsettings" class="fields"><?php echo Yii::t('app', 'Experteneinstellungen'); ?></h3>
	<div id="container-expertsettings" style="display: none;">
	  <div class="row">
	    <?php echo $form->labelEx($model,'meta_tags'); ?>
	    <?php echo $form->textArea($model,'meta_tags'); ?>
	    <?php echo $form->error($model,'meta_tags'); ?>
	  </div>
	  
	  <div class="row">
	    <?php echo $form->labelEx($model,'layout'); ?>
	    <?php echo $form->textField($model,'layout'); ?>
	    <?php echo $form->error($model,'layout'); ?>
	  </div>
  </div>  
  
  <?php $this->renderPartial('_pageWidget', array('model' => $model, 'form' => $form)); ?>    
  
  <div class="row buttons" style="margin-top: 50px;">
      <label for="">&nbsp;</label>
      <div style="text-align: right;">
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Erfassen') :  Yii::t('app', 'Speichern')); ?>
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Erfassen') :  Yii::t('app', 'Speichern und anschauen'), array('name' => 'saveandgo')); ?>
      </div>
  </div>    

  <?php $this->endWidget(); ?>
</div><!-- form -->

<?php echo CHtml::script('app_formPage();'); ?>