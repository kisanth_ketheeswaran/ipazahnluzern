<h3 class="fields" id="extrafields"><?php echo Yii::t('app', 'Zusatzfelder'); ?></h3>
<div id="container-extrafields" style="display: none;">
<?php foreach (PageOption::fields() as $key => $field) : ?>
    <div class="row">
        <?php echo $form->labelEx($modelOption,$key); ?>
        <?php if ($key == 'footer_text') : ?>
        <div class="floatLeft">
        <?php endif; ?>
            <?php echo $form->$field($modelOption,$key); ?>
        <?php if ($key == 'footer_text') : ?>
        </div>
        <?php endif; ?>
        
        <?php if ($field == 'fileField' && !empty($modelOption->$key)) : ?>
        <div class="previewPic">
          <?php
            $optionPath = Yii::app()->basePath . '/../images/option';
            $cFile = Yii::app()->file->set($optionPath . '/' . $modelOption->$key, true);
            if (strpos($cFile->getMimeType(), 'image') !== false) {
	            $loadImage = Yii::app()->image->load($optionPath . '/' . $modelOption->$key);
	            $loadImage->resize(300, 70);
	            $loadImage->save($optionPath . '/thumb/70_' . $modelOption->$key);
	            echo CHtml::image(Yii::app()->baseUrl . '/images/option/thumb/70_' . $modelOption->$key, '', array('class' => 'floatLeft'));
            } else {
              echo CHtml::link($modelOption->$key, Yii::app()->baseUrl . '/images/option/' . $modelOption->$key, array('class' => 'floatLeft', 'target' => '_blank'));
            }
            echo CHtml::link('', array('structure/index/deleteOptionFile'), array('class' => 'delete', 'onclick' => 'return app_deleteOptionFile(this, ' . $modelOption->id . ', "' . $key . '");'));
          ?>
        </div>
        <?php endif; ?>
        <?php echo $form->error($modelOption,$key); ?>
    </div>  
<?php endforeach; ?>
</div>

<script type="text/javascript">
<!--
      
//-->
</script>
