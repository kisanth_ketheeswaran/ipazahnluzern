<div class="form">
	<?php if(!isset($_GET['locale'])): ?>
		<?php $_GET['locale'] = ''; ?>
	<?php endif; ?>
	<?php if (!$model->isNewRecord && $model->type != 'site') : ?>
	<div class="locale">
	  <ul>
	      <?php foreach (LocaleModel::model()->findAll(array('order' => 'position ASC')) as $locale) : ?>
	          <li class="<?php echo $locale->cssStatus($_GET['locale']); ?>"><?php echo CHtml::link($locale->label, array('structure/index/update', 'id' => $sourceModel->id, 'locale' => $locale->id)); ?></li>
	    <?php endforeach; ?>
	    </ul>
	    <div class="line"></div>
	</div>
	<?php endif; ?>
	
	<?php if ($localeModel === false && $localeObj->id != LocaleModel::getDefault()->id && $sourceModel->parent->hasParent() == true && $model->type != 'site') : ?>
    <p><?php echo Yii::t('app', 'Sie müssen zuerst das Struktur-Element höher in dieser Sprache erfassen.'); ?> <?php echo CHtml::link(Yii::t('app', 'Jetzt erfassen'), array('structure/index/update', 'id' => $sourceModel->parent->id, 'locale' => $localeObj->id)); ?></p>
	<?php else : ?>
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'structure-form',
		'enableAjaxValidation'=> true,
        'htmlOptions' => array('onSubmit' => 'return app_saveForm(this)', 'enctype' => 'multipart/form-data')
	)); ?>

  <?php if ((($localeObj->id == LocaleModel::getDefault()->id) || (!empty($localeModel))) && $model->type != 'site' && is_object($model->page)) : ?>
  <div class="row">
    <label for=""><?php echo Yii::t('app', 'Seite'); ?></label>
    <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl . '/images/icon/16x16/edit_page.png'), 
    	array('updatePage', 'id' => $model->page->id, 'locale' => $localeObj->id)); ?>&nbsp;&nbsp;
        <?php //echo CHtml::link(CHtml::image(Yii::app()->baseUrl . '/images/icons/page_world.png'),
                // array('updatePage', 'id' => $model->page->id, 'locale' => $localeObj->id)); ?>
  </div>    
  <?php endif; ?>

	<div class="row">
		<?php echo $form->labelEx($model,'label'); ?>
		<?php echo $form->textField($model,'label',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'label'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'link'); ?>
		<?php echo $form->textField($model,'link',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'link'); ?>
	</div>
	
    <?php if ($model->type == 'nav') : ?>

	<div class="row">
		<?php echo $form->labelEx($model,'is_landing'); ?>
		<?php echo $form->checkbox($model,'is_landing'); ?>
		<?php echo $form->error($model,'is_landing'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'in_main'); ?>
		<?php echo $form->checkbox($model,'in_main'); ?>
		<?php echo $form->error($model,'in_main'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'in_header'); ?>
		<?php echo $form->checkbox($model,'in_header'); ?>
		<?php echo $form->error($model,'in_header'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'in_footer'); ?>
		<?php echo $form->checkbox($model,'in_footer'); ?>
		<?php echo $form->error($model,'in_footer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_home'); ?>
		<?php echo $form->checkbox($model,'is_home'); ?>
		<?php echo $form->error($model,'is_home'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_extern'); ?>
		<?php echo $form->checkbox($model,'is_extern'); ?>
		<?php echo $form->error($model,'is_extern'); ?>
	</div>

  <div class="row">
    <?php echo $form->labelEx($model,'target'); ?>
    <?php echo $form->dropDownList($model,'target', Structure::getTargetTypes(), array('prompt' => '')); ?>
    <?php echo $form->error($model,'target'); ?>
  </div>

	<div class="row">
		<?php echo $form->labelEx($model,'link_redirect'); ?>
		<?php echo $form->checkbox($model,'link_redirect'); ?>
		<div class="expand" style="display: <?php echo $model->link_redirect == 1 ? 'block' : 'none'; ?>">
		  <?php echo $form->dropDownList($model, 'link_redirect_point', Structure::getRedirectData()); ?>
		  <?php $displayLinkRedirectPoint = $model->link_redirect_point == 'other' ? 'block' : 'none'; ?>
		  <?php echo $form->textField($model,'point_link', array('style' => 'margin-left: 10px; display: ' . $displayLinkRedirectPoint)); ?>
		</div>
		<?php echo $form->error($model,'link_redirect'); ?>
	</div>

  <div class="row">
    <?php echo $form->labelEx($model,'is_protected'); ?>
    <?php echo $form->checkbox($model,'is_protected'); ?>
    <div class="expand" style="margin-left: 170px; display: <?php echo $model->is_protected == 1 ? 'block' : 'none'; ?>">
      
      <?php echo $form->checkBoxList($model,'protected_role', Role::getRoleData(), array('checkAll' => Yii::t('app', 'Alle Benutzergruppen'), 'separator' => '<div class="clearBoth"></div>')); ?>
    </div>
    <?php echo $form->error($model,'is_protected'); ?>
  </div>
  
  <?php else : ?>
    <div class="row">
        <?php echo $form->labelEx($model,'is_default'); ?>
        <?php echo $form->checkbox($model,'is_default'); ?>
        <?php echo $form->error($model,'is_default'); ?>
    </div>    
    
    <div class="row">
        <?php echo $form->labelEx($model,'domain'); ?>
        <?php echo $form->textArea($model,'domain'); ?>
        <div class="clearBoth"></div>
        <p class="hint"><?php echo Yii::t('app', 'Pro Zeile eine Domain eintragen.'); ?></p>
        <?php echo $form->error($model,'domain'); ?>
    </div>    
    
    <div class="row">
        <?php echo $form->labelEx($model,'layout'); ?>
        <?php echo $form->textField($model,'layout'); ?>
        <?php echo $form->error($model,'layout'); ?>
    </div>
  <?php endif; ?>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',Structure::getStatusData(), array('prompt' => '')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
	  <label for=""></label>
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Erfassen') : Yii::t('app', 'Speichern')); ?>
	</div>
    <?php $this->endWidget(); ?>
	<?php endif; ?>

</div><!-- form -->

<?php echo CHtml::script('app_structureForm();')?>