<div class="toolbar">
  <?php echo CHtml::link(Yii::t('app', 'Zurück'), array('admin'), array('class' => 'back'));  ?>
</div>

<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'localeModel' => $localeModel, 'localeObj' => $localeObj, 'sourceModel' => $sourceModel)); ?>