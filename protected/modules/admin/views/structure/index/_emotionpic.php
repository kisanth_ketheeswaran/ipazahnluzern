<h3 class="fields" id="extrafields"><?php echo Yii::t('app', 'Emotionsbilder'); ?></h3>
<div id="container-extrafields" style="/*display: none;*/">
    <div class="row">
        <?php echo $form->labelEx($emotionModel, 'name'); ?>
        <?php echo $form->fileField($emotionModel, 'name'); ?>        
        <?php echo $form->error($emotionModel, 'name'); ?>
    </div>
    <div class="previewPics">
        <?
        $pics = Emotionpic::model()->findAll('page_idfs = :pageid', array(':pageid' => $model->id));
        if (!empty($pics)):
            foreach ($pics as $pic):
            $teile = explode(".", $pic->name);
                ?>
                <div class="vorschauBild">
                    <img src="/images/option/thumb/thumb_pic<? echo $pic->id?>.<? echo $teile[1] ?>"/>
                    <a class="delete deleteEm" data-id='<? echo $pic->id;?>'></a>
                </div>
                <?
            endforeach;
        endif;
        ?>
    </div>

</div>

<script type="text/javascript">
<!--
    jQuery('a.deleteEm').click(function() {
        if (confirm('<?php echo Yii::t('app', 'Wirklich löschen'); ?>')) {
            var $this = jQuery(this);
            var dataid = $(this).attr('data-id');
            jQuery.ajax({
                'url': '/admin/structure/index/deleteEmotion',
                'type': 'post',
                'data': {id:dataid},
                'success': function() {
                    $this.parent().siblings('p').remove();
                    $this.parent().remove();
                }
            });
        }
    });
//-->
</script>
