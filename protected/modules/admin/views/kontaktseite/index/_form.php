<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'kontaktseite-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php if (!$sourceModel->isNewRecord) : ?>
        <div class="locale">
            <ul>
                <?php foreach (LocaleModel::model()->findAll(array('order' => 'position ASC')) as $locale) : ?>
                    <li class="<?php echo $locale->cssStatus($_GET['locale']); ?>"><?php echo CHtml::link($locale->label, array('kontaktseite/index/update', 'id' => $sourceModel->id, 'locale' => $locale->id)); ?></li>
                <?php endforeach; ?>
            </ul>
            <div class="line"></div>
        </div>
    <?php endif; ?>

    <?php if ($sourceModel->isNewRecord) : ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'locale_id'); ?>
            <?php
            if ($sourceModel->isNewRecord) {
                //$model->locale_id = $_GET['lang'];
                $model->locale_id = LocaleModel::getDefault()->id;
            }
            ?>
            <p class="left" style="margin: 0; padding-top: 3px; font-weight: bold;"><?php echo LocaleModel::model()->findByPk($model->locale_id)->label; ?></p>
        </div>
    <?php endif; ?>

    <div class="row">
        <?php echo $form->hiddenField($model, 'locale_id', array('value' => $model->locale_id)); ?>
        <?php echo $form->hiddenField($model, 'unit_hash', array('value' => $sourceModel->isNewRecord ? uniqid() : $sourceModel->unit_hash)); ?>

        <?php echo $form->labelEx($model, 'titel'); ?>
        <?php echo $form->textField($model, 'titel'); ?>
        <?php echo $form->error($model, 'titel'); ?> 
    </div>


    <div class="row">
        <?php echo $form->labelEx($model, 'text'); ?>
        <div style="float: left;"><?php echo $form->textArea($model, 'text'); ?></div>
        <script type="text/javascript">
            CKEDITOR.replace('Kontaktseite[text]', {
                'height': 240,
                'width': 610
            });
        </script>
    </div>

    <h3 id="boxen" class="fields"><?php echo Yii::t('app', 'Kontaktboxen'); ?></h3>
    <div id="container-boxen">
        <div class="row">
            <div class="boxCont">
                <div class="box">
                    <?php echo $form->labelEx($model, 'titel1'); ?>
                    <?php echo $form->textField($model, 'titel1'); ?>
                    <?php echo $form->error($model, 'titel1'); ?> 
                    <?php echo $form->labelEx($model, 'anfuehrungstext1'); ?>
                    <?php echo $form->textArea($model, 'anfuehrungstext1'); ?>
                    <?php echo $form->error($model, 'anfuehrungstext1'); ?> 
                </div>
                <div class="box">
                    <?php echo $form->labelEx($model, 'titel2'); ?>
                    <?php echo $form->textField($model, 'titel2'); ?>
                    <?php echo $form->error($model, 'titel2'); ?> 
                    <?php echo $form->labelEx($model, 'telefon'); ?>
                    <?php echo $form->textField($model, 'telefon'); ?>
                    <?php echo $form->error($model, 'telefon'); ?> 
                    <?php echo $form->labelEx($model, 'fax'); ?>
                    <?php echo $form->textField($model, 'fax'); ?>
                    <?php echo $form->error($model, 'fax'); ?> 
                    <?php echo $form->labelEx($model, 'email'); ?>
                    <?php echo $form->textField($model, 'email'); ?>
                    <?php echo $form->error($model, 'email'); ?> 
                </div>
                <div class="box">
                    <?php echo $form->labelEx($model, 'titel3'); ?>
                    <?php echo $form->textField($model, 'titel3'); ?>
                    <?php echo $form->error($model, 'titel3'); ?> 
                    <?php echo $form->labelEx($model, 'anfuehrungstext3'); ?>
                    <?php echo $form->textArea($model, 'anfuehrungstext3'); ?>
                    <?php echo $form->error($model, 'anfuehrungstext3'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Erfassen') : Yii::t('app', 'Speichern')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php echo CHtml::script('app_formPage();'); ?>