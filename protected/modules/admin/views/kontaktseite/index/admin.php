<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<? $genugBox = $model->getAnzDaten();
?>
<div class="root" style=" display: <? echo $genugBox ? 'block' : 'none'; ?>">
    <?php echo CHtml::link(Yii::t('app', 'Kontaktseite hinzufügen'), array('/admin/kontaktseite/index/create'), array('class' => 'add')); ?>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'kontaktseite-grid',
    'rowCssClassExpression' => $genugBox ? 'lok' : 'nix',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => '',
    'columns' => array(
        'text',
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete} {update}'
        ),
    ),
));
?>

<script>
    function pruefeKontakt(){
        $( "#kontaktseite-grid" ).has( "td.empty" ).length ? $('.root').show() : console.log("NO");      
    }
    $('.delete').on('click',function(){
        setTimeout(pruefeKontakt,2500);
    });
</script>
