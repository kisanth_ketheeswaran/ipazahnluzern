<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="root">
    <?php echo CHtml::link(Yii::t('app', 'Angebot hinzufügen'), array('/admin/angebot/index/create'), array('class' => 'add')); ?>
</div>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'angebot-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => '',
    'columns' => array(
        'titel',
        'text',
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete} {update}'
        ),
    ),
));
?>
