<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="root">
  <?php echo CHtml::link(Yii::t('app', 'Slider hinzufügen'), array('/admin/slider/index/create'), array('class' => 'add')); ?>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'slider-grid',
	'dataProvider'=>$model->search('default'),
	'filter'=>$model,
    'summaryText' => '',
	'columns'=>array(
	   'title',
	   'summary',
		array(
            'name' => 'pic',
            'filter' => false,
            'value' => 'App::getPic($data, "pic", "slider", null, 100, $data->title)',
            'type' => 'raw'
        ),
        array(
            'name' => 'status',
            'filter' => Slider::listDataStatus(),
            'value' => 'Slider::listDataStatus($data->status)'
        ),
        'sort_order',
		/*
		'sort_order',
		'locale_id',
		'unit_hash',
		'status',
		*/
        array(
            'class'=>'CButtonColumn',
            'template' => '{delete} {update}'
        ),
	),
)); ?>
