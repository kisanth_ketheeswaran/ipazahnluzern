<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="root">
  <?php echo CHtml::link(Yii::t('app', 'Glossar-Begriff hinzufügen'), array('/admin/glossary/index/create'), array('class' => 'add')); ?>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'glossary-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'summaryText' => '',
	'columns'=>array(
		'word',
		array(
            'name' => 'status',
            'filter' => Glossary::listDataStatus(),
            'value' => 'Glossary::listDataStatus($data->status)'
        ),
        array(
            'class'=>'CButtonColumn',
            'template' => '{delete} {update}'
        ),
    ),
)); ?>
