<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="root">
    <?php echo CHtml::link(Yii::t('app', 'FAQ hinzufügen'), array('/admin/faq/index/create'), array('class' => 'add')); ?>
    <?php echo CHtml::link(Yii::t('app', 'Kategorie hinzufügen'), array('/admin/faq/index/createGroup'), array('class' => 'add')); ?>
</div>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'glossary-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => '',
    'columns' => array(
    array(
            'name' => 'answer',
            //'filter' => 'answer',
            'value' => 'App::cutText(strip_tags($data->answer), 230)'
        ),
        array(
            'name' => 'status',
            'filter' => Faq::listDataStatus(),
            'value' => 'Faq::listDataStatus($data->status)'
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete} {update}'
        ),
    ),
));
?>
