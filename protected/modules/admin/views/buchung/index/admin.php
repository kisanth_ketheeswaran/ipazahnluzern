<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

    <div class="root">
    <?php
   /* $form = $this->beginWidget('CActiveForm', array(
        'id' => 'smallBuchung-form',
        'enableAjaxValidation' => false,
        'method' => 'post',
        'action'=>'/admin/buchung/index/admin',
        'htmlOptions'=>array(
            'style'=>'float:left',
        ),
    ));
    $lol = Terminplan::model()->findAll();
    $list =  CHtml::listData($lol, 'id', 'nachname');
    ?>
    <?php echo $form->dropDownList(new Terminplan, 'nachname',$list, array('class' => 'zeit')); ?> 
        <?php // echo $form->radioButtonList(new Terminplan, 'nachname',$list); ?>   
    <div class="row buttons" style="display: inline;">
        <label for=""></label>
        <?php echo CHtml::submitButton(Yii::t('app', 'Senden')); ?>
    </div>

    <?php $this->endWidget();*/ ?>
    <?php //echo CHtml::link(Yii::t('app', 'Termin hinzufügen'), array('/admin/buchung/index/create'), array('class' => 'add')); ?>
</div>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'buchung-grid',
    'dataProvider' => $model->search($nachname),
    'filter' => $model,
    'summaryText' => '',
    'columns' => array(
        'nachname', 'datum', 'buchungsdatum', 'status',
       array(
            'name' => 'terminplanIdfs.nachname',
            'filter' => CHtml::textField('terminplan_nachname', Yii::app()->request->getParam('terminplan_nachname')),
            'value' => '$data->terminplanIdfs->nachname'
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete} {update}'
        ),
    ),
));
?>
