<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>


<div class="floatLeft" style="width: 450px;">
    <div class="form">
        <div class="locale">
          <ul>
              <?php foreach (LocaleModel::model()->findAll(array('order' => 'position ASC')) as $locale) : ?>
                  <?php if ($model->getModel($locale->id)) : ?>
                  <li class="<?php echo $locale->cssStatus($_GET['locale']); ?>"><?php echo CHtml::link($locale->label, array('reference/index/view', 'id' => $sourceModel->id, 'locale' => $locale->id)); ?></li>
                  <?php endif; ?>
            <?php endforeach; ?>
            </ul>
            <div class="line"></div>
        </div>
    </div>
    
    
    <?php $this->widget('zii.widgets.CDetailView', array(
      'data'=>$model,
      'attributes'=>array(
        'title',
        array(
                'name' => 'status',
                'value' => Faq::listDataStatus(),
            )
      ),
    )); ?>
</div>

	<div class="floatRight" style="width: 505px;">
	
	<? $this->widget('ext.EAjaxUpload.EAjaxUpload',
array(
        'id'=>'upload',
        'config'=>array(
               'action'=>'/admin/gallery/index/upload?galleryID='.$model->id,
               'allowedExtensions'=>array("jpg", "png", "jpeg"),//array("jpg","jpeg","gif","exe","mov" and etc...
               'sizeLimit'=>10 * 2048 * 2048,// maximum file size in bytes
               'minSizeLimit'=>0,// minimum file size in bytes
               'multiple'=> true,
               'buttonLabel' => Yii::t('app', 'Bilder hochladen'),
               'onComplete'=>"js:function(){ app_refreshPics(); }",
               //'onComplete'=>"js:function(id, fileName, responseJSON){ alert(fileName); }",
               //'messages'=>array(
               //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
               //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
               //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
               //                  'emptyError'=>"{file} is empty, please select files again without it.",
               //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
               //                 ),
               //'showMessage'=>"js:function(message){ alert(message); }"
              )
)); ?>

<div class="clearBoth"></div>
    
    <div id="picsContainer"></div>
</div> 


<script type="text/javascript">
<!--
    app_refreshPics();
    
    function app_refreshPics()
    {
    	$.ajaxSetup({ cache: false });
        jQuery.ajax({
           url: '/admin/gallery/index/refreshPic?galleryID=<?php echo $model->id; ?>',
           type: 'get',
           success: function(data) {
                jQuery('#picsContainer').html(data);               
           } 
        });
    }
    
//-->
</script>