<div class="toolbar">
  <?php echo CHtml::link(Yii::t('app', 'Zurück'), array('admin'), array('class' => 'back'));  ?>
  <?php echo CHtml::link(Yii::t('app', 'Bearbeiten'), array('update', 'id' => $model->id, 'locale' => $model->locale_id), array('class' => 'edit'));  ?>
</div>
<?php echo $this->renderPartial('_formElements', array('model'=>$model, 'sourceModel' => $sourceModel)); ?>