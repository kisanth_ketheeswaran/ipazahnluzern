<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="root">
  <?php echo CHtml::link(Yii::t('app', 'Galerie hinzufügen'), array('/admin/gallery/index/create'), array('class' => 'add')); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gallery-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'summaryText' => '',
	'columns'=>array(
		'title',
        array(
            'name' => 'status',
            'filter' => Gallery::listDataStatus(),
            'value' => 'Gallery::listDataStatus($data->status)'
        ),
		/*
		'status',
		'locale_id',
		'unit_hash',
		*/
        array(
            'class'=>'CButtonColumn',
            'template' => '{delete} {update} {view}'
        ),
	),
)); ?>
