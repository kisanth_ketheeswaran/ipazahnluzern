<?php if (count($pics)) : ?>
    <table class="referencePic" cellpadding="0" cellspacing="0">
        <tr>
            <th class="name"><?php echo Yii::t('app', 'Name'); ?></th>
            <?php /*foreach (Gallery::getTypes() as $type) : ?>
                <th><?php echo $type; ?></th>
            <?php endforeach; */?>
            <th style="text-align: center;"><?php echo Yii::t('app', 'Pos'); ?></th>
            <th class="delete">D</th>
        </tr>        
        
        <?php foreach ($pics as $pic) : ?>
        <tr class="row" data-id="<?php echo $pic->id; ?>">
            <td class="name">
                <?php $picImage = App::getPic($pic, 'image', 50, null, $pic->description,false,'media/gallery'); ?>
                <a><?php echo $picImage; ?></a>
                <input type="text" name="GalleryPic[<?php echo $pic->id; ?>][description]" value="<?php echo $pic->description; ?>" />
            </td>
            <td class="pos"><input type="text" name="GalleryPic[<?php echo $pic->id; ?>][position]" value="<?php echo $pic->position; ?>" /></td>
            <td class="delete"><input type="radio" data-id="<?php echo $pic->id; ?>" class="deletePic" /></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>

<script type="text/javascript">
<!--
    jQuery('input[type="radio"].deletePic').click(function() {
       var $this = jQuery(this);
       
       if (confirm('<?php echo Yii::t('app', 'Wirklich löschen?'); ?>')) {
            jQuery.ajax({
                  url: '/admin/gallery/index/deletePic?pic=' + jQuery(this).attr('data-id'),
                  type: 'post',
                  success: function() {
                      $this.parents('tr.row').fadeOut(150, function() {
                          jQuery(this).remove();
                      });
                  }
            });
       } else {
           $this.attr('checked', false);
       }
    });
    
    
    jQuery('.referencePic input[type="text"]').blur(function() {
       var $this = jQuery(this);
       var $trRow = $this.parents('tr.row');
       var data = 'ajax=1';
       
       $trRow.find(':input').each(function() {
           //alert(jQuery(this).attr('name'));
           if (jQuery(this).attr('type') == 'checkbox' && jQuery(this).is(':checked')) {
               data += '&' + jQuery(this).attr('name') + '=' + jQuery(this).val();
           } else if (jQuery(this).attr('type') == 'text') {
               data += '&' + jQuery(this).attr('name') + '=' + jQuery(this).val();
           }
       });
       //console.log($trRow.attr('data-id'));
       console.log(data);
       jQuery.ajax({
          url: '/admin/gallery/index/updatePic?pic=' + $trRow.attr('data-id'),
          type: 'post',
          data: data,
          success: function(output) {
              $trRow.effect('highlight', { }, 2000);
              console.log(output);
          } 
           
       });
       
    });
    
    jQuery('.referencePic input[type="checkbox"]').click(function() {
       var $this = jQuery(this);
       var $trRow = $this.parents('tr.row');
       var data = 'ajax=1';
       
       $trRow.find(':input').each(function() {
           if (jQuery(this).attr('type') == 'checkbox' && jQuery(this).is(':checked')) {
               data += '&' + jQuery(this).attr('name') + '=' + jQuery(this).val();
           } else if (jQuery(this).attr('type') == 'text') {
               data += '&' + jQuery(this).attr('name') + '=' + jQuery(this).val();
           }
       });
       
       jQuery.ajax({
          url: '/admin/gallery/index/updatePic?pic=' + $trRow.attr('data-id'),
          type: 'post',
          data: data,
          success: function(output) {
          	console.log(output);
          } 
           
       });
       
    });
    
//-->
</script>