<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="root">
  <?php echo CHtml::link(Yii::t('app', 'Presse hinzufügen'), array('/admin/press/index/create'), array('class' => 'add')); ?>
</div>




<?php $this->widget('zii.widgets.grid.CGridView', array(

	'id'=>'slider-grid',

	'dataProvider'=>$model->search(),

	'filter'=>$model,
    'summaryText' => '',

	'columns'=>array(
	   'date',
	   'title',
		array(
            'name' => 'logo',
            'filter' => false,
            'value' => 'App::getPic($data, "logo", null, 70, $data->title, false)',
            'type' => 'raw'
        ),
        array(
            'name' => 'status',
            'filter' => Slider::listDataStatus(),
            'value' => 'Slider::listDataStatus($data->status)'
        ),
		/*
		'sort_order',
		'locale_id',
		'unit_hash',
		'status',
		*/
        array(
            'class'=>'CButtonColumn',
            'template' => '{delete} {update}'
        ),

	),

)); ?>

