<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="root">
    <?php echo CHtml::link(Yii::t('app', 'Kunde hinzufügen'), array('/admin/kunde/index/create'), array('class' => 'add')); ?>
</div>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'kunde-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => '',
    'columns' => array(
        'nachname',
        'firma',
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete} {update}'
        ),
    ),
));
?>
