<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="toolbar">
    <?php echo CHtml::link(Yii::t('app', 'Zurück'), '/admin/terminplan/index/update/id/'.$alleFerien[0]->terminplan_idfs, array('class' => 'back')); ?>
</div>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'ferienliste-form',
        'enableAjaxValidation' => false,
    ));
    ?>

<div class="ferienListe"> 

    <? if (!empty($alleFerien)): ?>
        <ol>
            <? foreach ($alleFerien as $fer):
                ?>
                <li>                   
                    <span class="tagName">Von</span>

                    <?
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        /* 'model' => $fer,
                          'attribute' => 'von_datum', */
                        'id' => 'von_datum_edit-' . $fer->id,
                        'name' => 'Ferien' . $fer->id . '[von_datum]',
                        'value' => $fer->von_datum,
                        // additional javascript options for the date picker plugin
                        'options' => array(
                            'showAnim' => 'fold',
                            'dateFormat' => 'dd-mm-yy',
                            'minDate'=>'0d',
                        ),
                        'htmlOptions' => array(
                            'style' => 'height:20px;',
                            'class' => 'datePicker'
                        ),
                    ));
                    ?> 
                    <?php echo $form->dropDownList($fer, 'von_zeit', Standardkalender::getZeitSlots(), array('class' => 'zeit','name' => 'Ferien' . $fer->id . '[von_zeit]')); ?> 
                    <span class="tagName bis">Bis</span>
                    <?
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        /* 'model' => $fer,
                          'attribute' => 'bis_datum', */
                        'id' => 'bis_datum_edit-' . $fer->id,
                        'name' => 'Ferien' . $fer->id . '[bis_datum]',
                        'value' => $fer->bis_datum,
                        // additional javascript options for the date picker plugin
                        'options' => array(
                            'showAnim' => 'fold',
                            'dateFormat' => 'dd-mm-yy',
                            'altFormat' => 'dd-mm-yy', // show to user format
                        ),
                        'htmlOptions' => array(
                            'style' => 'height:20px;',
                            'class' => 'datePicker'
                        ),
                    ));
                    ?> 
                    <?php echo $form->dropDownList($fer, 'bis_zeit', Standardkalender::getZeitSlots(), array('class' => 'zeit', 'name' => 'Ferien' . $fer->id . '[bis_zeit]')); ?> 
                    <? echo CHtml::link('',"", array('class'=>'delete','data-id'=>$fer->id)); ?>
                    <div class="fehelermeldung">
                        <?php echo $form->error($fer, 'von_datum'); ?>          
                        <?php echo $form->error($fer, 'von_zeit'); ?>
                        <?php echo $form->error($fer, 'bis_datum'); ?>
                        <?php echo $form->error($fer, 'bis_zeit'); ?>
                    </div>
                </li>
            <? endforeach; ?>
        </ol>
        <div class="clearBoth"></div>
    <? endif; ?>

</div>
        <div class="row buttons" style="margin-top:20px;">
        <label for=""></label>
<?php echo CHtml::submitButton(Yii::t('app', 'Speichern')); ?>
    </div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
<!--
    jQuery('a.delete').click(function() {
        if (confirm('<?php echo Yii::t('app', 'Wirklich löschen'); ?>')) {
            var $this = jQuery(this);
            var ferienId = $this.attr('data-id');
            jQuery.ajax({
                'url': '/admin/terminplan/index/entferneFerien',
                'type': 'post',
                dataType: 'json',
                'data': {id:ferienId},
                'success': function(dataa) {
                    $this.parent().siblings('p').remove();
                    $this.parent().remove();
                    console.log(dataa);
                    if(dataa=="leer"){
                        $("form").hide();
                    }
                }
            });
        }
    });
//-->
</script>