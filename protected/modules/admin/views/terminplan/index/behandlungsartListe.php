<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="toolbar">
    <?php echo CHtml::link(Yii::t('app', 'Zurück'), '/admin/terminplan/index/update/id/' . $alleBehandlungen[0]->terminplan_idfs, array('class' => 'back')); ?>
</div>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'behandlungsart-form',
    'enableAjaxValidation' => false,
        ));
?>

<div class="ferienListe form"> 

    <? if (!empty($alleBehandlungen)): ?>
    <ol>
        <? foreach ($alleBehandlungen as $behandlung):
            ?>
    <li>
            <div class="behandlungCont">
                <div class="row">
                    <?php echo $form->labelEx($behandlung, 'behandlung'); ?>
                    <?php echo $form->textField($behandlung, 'behandlung', array('name' => 'Behandlungsart' . $behandlung->id . '[behandlung]')); ?>
                    <?php echo $form->error($behandlung, 'behandlung'); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($behandlung, 'bemerkung'); ?>
                    <?php echo $form->textArea($behandlung, 'bemerkung', array('name' => 'Behandlungsart' . $behandlung->id . '[bemerkung]')); ?>
                    <?php echo $form->error($behandlung, 'bemerkung'); ?>
                </div>
                <div class="row">
                    <?php echo $form->labelEx($behandlung, 'position'); ?>
                    <?php echo $form->textField($behandlung, 'position', array('name' => 'Behandlungsart' . $behandlung->id . '[position]')); ?>
                    <?php echo $form->error($behandlung, 'position'); ?>
                </div>
                <? echo CHtml::link('', "", array('class' => 'delete', 'data-id' => $behandlung->id)); ?>
            </div>
    </li>
        <? endforeach; ?>
    </ol>
    </div> 
    <div class="clearBoth"></div>
<? endif; ?>


<div class="row buttons" style="margin-top:20px;">
    <label for=""></label>
    <?php echo CHtml::submitButton(Yii::t('app', 'Speichern')); ?>
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
<!--
    jQuery('a.delete').click(function () {
        if (confirm('<?php echo Yii::t('app', 'Wirklich löschen'); ?>')) {
            var $this = jQuery(this);
            var ferienId = $this.attr('data-id');
            jQuery.ajax({
                'url': '/admin/terminplan/index/entferneBehandlungsart',
                'type': 'post',
                dataType: 'json',
                'data': {id: ferienId},
                'success': function (dataa) {
                    $this.parent().siblings('p').remove();
                    $this.parents('li').remove();
                    console.log(dataa);
                    if (dataa == "leer") {
                        $("form").hide();
                    }
                }
            });
        }
    });
//-->
</script>