<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'terminplan-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php if (!$sourceModel->isNewRecord) : ?>
        <div class="locale">
            <ul>
                <?php foreach (LocaleModel::model()->findAll(array('order' => 'position ASC')) as $locale) : ?>
                    <li class="<?php echo $locale->cssStatus(empty($_GET['locale']) ? '' : $_GET['locale']); ?>"><?php echo CHtml::link($locale->label, array('terminplan/index/update', 'id' => $sourceModel->id, 'locale' => $locale->id)); ?></li>
                <?php endforeach; ?>
            </ul>
            <div class="line"></div>
        </div>
    <?php endif; ?>

    <?php if ($sourceModel->isNewRecord) : ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'locale_id'); ?>
            <?php
            if ($sourceModel->isNewRecord) {
                //$model->locale_id = $_GET['lang'];
                $model->locale_id = LocaleModel::getDefault()->id;
            }
            ?>
            <p class="left" style="margin: 0; padding-top: 3px; font-weight: bold;"><?php echo LocaleModel::model()->findByPk($model->locale_id)->label; ?></p>
        </div>
    <?php endif; ?>

    <div class="row">
        <?php echo $form->hiddenField($model, 'locale_id', array('value' => $model->locale_id)); ?>
        <?php echo $form->hiddenField($model, 'unit_hash', array('value' => $sourceModel->isNewRecord ? uniqid() : $sourceModel->unit_hash)); ?>
        <?php echo $form->labelEx($model, 'anrede'); ?>
        <?php echo $form->textField($model, 'anrede'); ?>
        <?php echo $form->error($model, 'anrede'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'vorname'); ?>
        <?php echo $form->textField($model, 'vorname'); ?>
        <?php echo $form->error($model, 'vorname'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'nachname'); ?>
        <?php echo $form->textField($model, 'nachname'); ?>
        <?php echo $form->error($model, 'nachname'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email'); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'bemerkung'); ?>
        <?php echo $form->textArea($model, 'bemerkung'); ?>
        <?php echo $form->error($model, 'bemerkung'); ?>
    </div>

    <h3 id="wochentag" class="fields"><?php echo Yii::t('app', 'Wochentage'); ?></h3>
    <div id="container-wochentag" style="">
        <div class="wochenZeit">
            <div class="tag">
                <p class="zeit">
                    <span class="tagName">Montag</span> 
                    <?php echo $form->labelEx($montag, 'von'); ?>
                    <?php echo $form->dropDownList($montag, 'von', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($montag, 'bis'); ?>
                    <?php echo $form->dropDownList($montag, 'bis', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($montag, 'frei', array('style' => 'margin-left:20px;')); ?>
                    <?php echo $form->checkbox($montag, 'frei'); ?>
                </p>
                <div class="tagError">
                    <?php echo $form->error($montag, 'von'); ?>
                    <?php echo $form->error($montag, 'bis'); ?>
                </div>
            </div>
            <div class="tag">
                <p class="zeit">
                    <span class="tagName">Dienstag</span> 
                    <?php echo $form->labelEx($dienstag, 'von'); ?>
                    <?php echo $form->dropDownList($dienstag, 'von', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($dienstag, 'bis'); ?>
                    <?php echo $form->dropDownList($dienstag, 'bis', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($dienstag, 'frei', array('style' => 'margin-left:20px;')); ?>
                    <?php echo $form->checkbox($dienstag, 'frei'); ?>
                </p>
                <div class="tagError">
                    <?php echo $form->error($dienstag, 'von'); ?>
                    <?php echo $form->error($dienstag, 'bis'); ?>
                </div>
            </div>
            <div class="tag">
                <p class="zeit">
                    <span class="tagName">Mittwoch</span> 
                    <?php echo $form->labelEx($mittwoch, 'von'); ?>
                    <?php echo $form->dropDownList($mittwoch, 'von', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($mittwoch, 'bis'); ?>
                    <?php echo $form->dropDownList($mittwoch, 'bis', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($mittwoch, 'frei', array('style' => 'margin-left:20px;')); ?>
                    <?php echo $form->checkbox($mittwoch, 'frei'); ?>
                </p>
                <div class="tagError">
                    <?php echo $form->error($mittwoch, 'von'); ?>
                    <?php echo $form->error($mittwoch, 'bis'); ?>
                </div>
            </div>
            <div class="tag">
                <p class="zeit">
                    <span class="tagName">Donnerstag</span> 
                    <?php echo $form->labelEx($donnerstag, 'von'); ?>
                    <?php echo $form->dropDownList($donnerstag, 'von', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($donnerstag, 'bis'); ?>
                    <?php echo $form->dropDownList($donnerstag, 'bis', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($donnerstag, 'frei', array('style' => 'margin-left:20px;')); ?>
                    <?php echo $form->checkbox($donnerstag, 'frei'); ?>
                </p>
                <div class="tagError">
                    <?php echo $form->error($donnerstag, 'von'); ?>
                    <?php echo $form->error($donnerstag, 'bis'); ?>
                </div>
            </div>
            <div class="tag">
                <p class="zeit">
                    <span class="tagName">Freitag</span> 
                    <?php echo $form->labelEx($freitag, 'von'); ?>
                    <?php echo $form->dropDownList($freitag, 'von', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($freitag, 'bis'); ?>
                    <?php echo $form->dropDownList($freitag, 'bis', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($freitag, 'frei', array('style' => 'margin-left:20px;')); ?>
                    <?php echo $form->checkbox($freitag, 'frei'); ?>
                </p>
                <div class="tagError">
                    <?php echo $form->error($freitag, 'von'); ?>
                    <?php echo $form->error($freitag, 'bis'); ?>
                </div>
            </div>
            <div class="tag">
                <p class="zeit">
                    <span class="tagName">Samstag</span> 
                    <?php echo $form->labelEx($samstag, 'von'); ?>
                    <?php echo $form->dropDownList($samstag, 'von', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($samstag, 'bis'); ?>
                    <?php echo $form->dropDownList($samstag, 'bis', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($samstag, 'frei', array('style' => 'margin-left:20px;')); ?>
                    <?php echo $form->checkbox($samstag, 'frei'); ?>
                </p>
                <div class="tagError">
                    <?php echo $form->error($samstag, 'von'); ?>
                    <?php echo $form->error($samstag, 'bis'); ?>
                </div>
            </div>
            <div class="tag">
                <p class="zeit">
                    <span class="tagName">Sonntag</span> 
                    <?php echo $form->labelEx($sonntag, 'von'); ?>
                    <?php echo $form->dropDownList($sonntag, 'von', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($sonntag, 'bis'); ?>
                    <?php echo $form->dropDownList($sonntag, 'bis', Standardkalender::getZeitSlots(), array('style' => '')); ?> 
                    <?php echo $form->labelEx($sonntag, 'frei', array('style' => 'margin-left:20px;')); ?>
                    <?php echo $form->checkbox($sonntag, 'frei'); ?>
                </p>
                <div class="tagError">
                    <?php echo $form->error($sonntag, 'von'); ?>
                    <?php echo $form->error($sonntag, 'bis'); ?>
                </div>
            </div>
        </div>
    </div> 

    <h3 id="behandlungsart" class="fields"><?php echo Yii::t('app', 'Behandlungsart'); ?></h3>
    <div id="container-behandlungsart" style="">
        <div class="row">
            <?php echo $form->labelEx($behandlungsart, 'behandlung'); ?>
            <?php echo $form->textField($behandlungsart, 'behandlung'); ?>
            <?php echo $form->error($behandlungsart, 'behandlung'); ?>
            <?
            if (Behandlungsart::model()->findAll('terminplan_idfs = :id', array(':id' => $model->id))) {
                echo CHtml::link('Ihre Behanlungsarten', '/admin/terminplan/index/behandlungsartListe/id/' . $model->id, array('class' => 'linkListe', 'target' => '_blank','style'=>'margin-left:20px;'));
            }
            ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($behandlungsart, 'bemerkung'); ?>
            <?php echo $form->textArea($behandlungsart, 'bemerkung'); ?>
            <?php echo $form->error($behandlungsart, 'bemerkung'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($behandlungsart, 'position'); ?>
            <?php echo $form->textField($behandlungsart, 'position'); ?>
            <?php echo $form->error($behandlungsart, 'position'); ?>
        </div>
        <?
        if (Behandlungsart::model()->findAll('terminplan_idfs = :id', array(':id' => $model->id))) {
            echo CHtml::link('Ihre Behanlungsarten', '/admin/terminplan/index/behandlungsartListe/id/' . $model->id, array('class' => 'linkListe', 'target' => '_blank'));
        }
        ?>
    </div>


    <h3 id="freitage" class="fields"><?php echo Yii::t('app', 'Ferien & Feiertage'); ?></h3>
    <div id="container-freitage" style="">
        <div class="row ferienCont" id="freiertagCont">
            <div class="ferienBox">
                <h4>Ferien</h4>
                <span class="tagName">Von</span>
                <?
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $ferien,
                    'attribute' => 'von_datum',
                    // additional javascript options for the date picker plugin
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => 'dd-mm-yy',
                        'minDate' => '0d',
                    ),
                    'htmlOptions' => array(
                        'style' => 'height:20px;',
                        'class' => 'datePicker'
                    ),
                ));
                ?> 
                <?php echo $form->dropDownList($ferien, 'von_zeit', Standardkalender::getZeitSlots(), array('class' => 'zeit')); ?> 
                <span class="tagName">Bis</span>
                <?
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $ferien,
                    'attribute' => 'bis_datum',
                    // additional javascript options for the date picker plugin
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => 'dd-mm-yy',
                        'altFormat' => 'dd-mm-yy', // show to user format
                        'minDate' => '0d',
                    ),
                    'htmlOptions' => array(
                        'style' => 'height:20px;',
                        'class' => 'datePicker'
                    ),
                ));
                ?> 
                <?php echo $form->dropDownList($ferien, 'bis_zeit', Standardkalender::getZeitSlots(), array('class' => 'zeit')); ?> 
                <?
                if (Ferien::model()->findAll('terminplan_idfs = :id', array(':id' => $model->id))) {
                    echo CHtml::link('Ihre Ferienliste', '/admin/terminplan/index/ferienListe/id/' . $model->id, array('class' => 'linkListe', 'target' => '_blank'));
                }
                ?>
                <div class="fehelermeldung">
                    <?php echo $form->error($ferien, 'von_datum'); ?>          
                    <?php echo $form->error($ferien, 'von_zeit'); ?>
                    <?php echo $form->error($ferien, 'bis_datum'); ?>
                    <?php echo $form->error($ferien, 'bis_zeit'); ?>
                </div>

            </div>
            <div class="ferienBox" id="freiertagBox">
                <h4>Freiertag</h4>
                <span class="tagName">Von</span>
                <?
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $freiertag,
                    'attribute' => 'datum',
                    // additional javascript options for the date picker plugin
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => 'dd-mm-yy',
                        'minDate' => '0d',
                    ),
                    'htmlOptions' => array(
                        'style' => 'height:20px;',
                        'class' => 'datePicker'
                    ),
                ));
                ?> 
                <?php echo $form->dropDownList($freiertag, 'von', Standardkalender::getZeitSlots(), array('class' => 'zeit')); ?> 
                <span class="tagName">Bis</span>
                <?php echo $form->dropDownList($freiertag, 'bis', Standardkalender::getZeitSlots(), array('class' => 'zeit')); ?> 
                <?
                if (Freiertag::model()->findAll('terminplan_idfs = :id', array(':id' => $model->id))) {
                    echo CHtml::link('Ihre Freietage', '/admin/terminplan/index/freiertagListe/id/' . $model->id, array('class' => 'linkListe', 'target' => '_blank'));
                }
                ?>
                <div class="fehelermeldung">
                    <?php echo $form->error($freiertag, 'datum'); ?>          
                    <?php echo $form->error($freiertag, 'von'); ?>
                    <?php echo $form->error($freiertag, 'bis'); ?>
                </div>

            </div>

        </div>

    </div>

    <div class="row buttons" style="margin-top:20px; margin-bottom: 30px;">
        <label for=""></label>
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Erfassen') : Yii::t('app', 'Speichern')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<?php echo CHtml::script('app_formPage();'); ?>