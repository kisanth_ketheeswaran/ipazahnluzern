<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="toolbar">
    <?php echo CHtml::link(Yii::t('app', 'Zurück'), '/admin/terminplan/index/update/id/'.$alleFreietage[0]->terminplan_idfs, array('class' => 'back')); ?>
</div>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'ferienliste-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <div class="ferienListe form" id="freiertag"> 

    <? if (!empty($alleFreietage)): ?>
        <ol>
            <? foreach ($alleFreietage as $frei):
                ?>
                <li>
                     <span class="tagName">Datum</span>
                    <?
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        /* 'model' => $fer,
                          'attribute' => 'von_datum', */
                        'id' => 'datum_edit-' . $frei->id,
                        'name' => 'Freiertag' . $frei->id . '[datum]',
                        'value' => $frei->datum,
                        // additional javascript options for the date picker plugin
                        'options' => array(
                            'showAnim' => 'fold',
                            'dateFormat' => 'dd-mm-yy',
                            'minDate'=>'0d',
                        ),
                        'htmlOptions' => array(
                            'style' => 'height:20px;',
                            'class' => 'datePicker'
                        ),
                    ));
                    ?> 
                     <span class="tagName bis">Von</span>
                    <?php echo $form->dropDownList($frei, 'von', Standardkalender::getZeitSlots(), array('class' => 'zeit','name' => 'Freiertag' . $frei->id . '[von]')); ?> 
                    <span class="tagName bis">Bis</span>
                    <?php echo $form->dropDownList($frei, 'bis', Standardkalender::getZeitSlots(), array('class' => 'zeit', 'name' => 'Freiertag' . $frei->id . '[bis]')); ?> 
                    <? echo CHtml::link('',"", array('class'=>'delete','data-id'=>$frei->id)); ?>
                    <div class="fehelermeldung">
                        <?php echo $form->error($frei, 'datum'); ?>          
                        <?php echo $form->error($frei, 'von'); ?>
                        <?php echo $form->error($frei, 'bis'); ?>
                    </div>
                </li>
            <? endforeach; ?>
        </ol>
        <div class="clearBoth"></div>
    <? endif; ?>

</div>
        <div class="row buttons" style="margin-top:20px;">
        <label for=""></label>
<?php echo CHtml::submitButton(Yii::t('app', 'Speichern')); ?>
    </div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
<!--
    jQuery('a.delete').click(function() {
        if (confirm('<?php echo Yii::t('app', 'Wirklich löschen'); ?>')) {
            var $this = jQuery(this);
            var freiertagId = $this.attr('data-id');
            jQuery.ajax({
                'url': '/admin/terminplan/index/entferneFreiertag',
                'type': 'post',
                dataType: 'json',
                'data': {id:freiertagId},
                'success': function(dataa) {
                    $this.parent().siblings('p').remove();
                    $this.parent().remove();
                    if(dataa=="leer"){
                        $("form").hide();
                    }
                }
            });
        }
    });
//-->
</script>