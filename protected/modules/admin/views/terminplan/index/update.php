<div class="toolbar">
    <?php echo CHtml::link(Yii::t('app', 'Zurück'), array('admin'), array('class' => 'back')); ?>
</div>


<?php
echo $this->renderPartial('_form', array(
    'model' => $model,
    'sourceModel' => $sourceModel,
    'montag' => $montag,
    'dienstag' => $dienstag,
    'mittwoch' => $mittwoch,
    'donnerstag' => $donnerstag,
    'freitag' => $freitag,
    'samstag' => $samstag,
    'sonntag' => $sonntag,
    'ferien' => $ferien,
    'freiertag' => $freiertag,
    'behandlungsart'=>$behandlungsart,
        )
);
?>