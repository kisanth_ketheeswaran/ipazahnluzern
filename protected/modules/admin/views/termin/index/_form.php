<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'termin-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <div class="buchungCont" id="patient"> 
        <h3>Patient</h3>
        <div class="row">
            <?php echo $form->labelEx($model, 'anrede'); ?>
            <?php echo $form->dropDownList($model, 'anrede', Buchung::listAnrede()); ?>
            <?php echo $form->error($model, 'anrede'); ?>
        </div>


        <div class="row">
            <?php echo $form->labelEx($model, 'nachname'); ?>
            <?php echo $form->textField($model, 'nachname', array('size' => 60, 'maxlength' => 250)); ?>
            <?php echo $form->error($model, 'nachname'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'vorname'); ?>
            <?php echo $form->textField($model, 'vorname', array('size' => 60, 'maxlength' => 250)); ?>
            <?php echo $form->error($model, 'vorname'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'geburtsdatum'); ?>
            <?php echo $form->textField($model, 'geburtsdatum', array('size' => 60, 'maxlength' => 250)); ?>
            <?php echo $form->error($model, 'geburtsdatum'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'strasse'); ?>
            <?php echo $form->textField($model, 'strasse', array('size' => 60, 'maxlength' => 250)); ?>
            <?php echo $form->error($model, 'strasse'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'plz'); ?>
            <?php echo $form->textField($model, 'plz', array('size' => 60, 'maxlength' => 250)); ?>
            <?php echo $form->error($model, 'plz'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'ort'); ?>
            <?php echo $form->textField($model, 'ort', array('size' => 60, 'maxlength' => 250)); ?>
            <?php echo $form->error($model, 'ort'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'telefon'); ?>
            <?php echo $form->textField($model, 'telefon', array('size' => 60, 'maxlength' => 250)); ?>
            <?php echo $form->error($model, 'telefon'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'email'); ?>
            <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 250)); ?>
            <?php echo $form->error($model, 'email'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'bemerkung'); ?>
            <?php echo $form->textArea($model, 'bemerkung'); ?>
            <?php echo $form->error($model, 'bemerkung'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'terminerinnerung'); ?>
            <?php echo $form->checkBox($model, 'terminerinnerung'); ?>
            <?php echo $form->error($model, 'terminerinnerung'); ?>
        </div>
    </div>

    <div class="buchungCont" id="termin" style="margin-left: 10px">
        <h3>Terminlpan</h3>
        <div class="row">
            <?php echo $form->labelEx(new Terminplan, 'nachname', array('label' => 'Terminplan')); ?>
            <?php
            $bestehtArzt = false;
            $list = CHtml::listData($terminplan, 'id', 'nachname');
            if ($model->isNewRecord) {
                echo $form->dropDownList(new Terminplan, 'nachname', $list, array('selected' => ""));
            } else {
                $bestehtArzt = true;
                echo $form->dropDownList($model->terminplanIdfs, 'nachname', $list, array('options' => array($model->terminplanIdfs->id => array('selected' => true))));
            }
            ?>
            <?php echo $form->error(new Terminplan, 'nachname'); ?>
        </div>
        <div class="fragenCont row">
            <h4 style="">Behandlungen <span style="color: red;" class="required">*</span></h4>
            <?
            $i = 0;
            foreach ($terminplan as $plan) {
                foreach ($behandlungsart as $behand):
                    if ($plan->id == $behand->terminplan_idfs):
                        ?>
                        <div class="planFragenCont row" data-id="<? echo $behand->terminplan_idfs ?>" 
                        <? if ($model->isNewRecord): ?>
                                 style="display: <? echo $i == 0 ? "block" : "none"; ?>"

                             <? elseif ($bestehtArzt && $model->terminplanIdfs->id == $plan->id): ?>
                                 style="display: block"
                             <? endif; ?>
                             >
                            <label><? echo $behand->behandlung; ?></label>
                            <?php echo $form->checkbox(new Behandlungsart(), 'id', array('uncheckValue'=>null,'value' => $behand->id, 'name' => 'Behandlungsart[id' . $behand->id . ']', 'checked' => Behandlung::model()->pruefObBehandlungAusgewaehlt($behand, $model->id) ? true : false));
                            ?>
                        </div>
                        <?
                    endif;

                endforeach;
                $i++;
            }
            ?>
            <?php echo $form->error(new Behandlungsart, 'behandlung'); ?>
            <div class="errorBehandlung"></div>
        </div>
        <div class="terminCont">
            <h3>Termin</h3>
            <div class="row">
                <?php echo $form->labelEx($model, 'dauer'); ?>
                <?php echo $form->dropDownList($model, 'dauer', Buchung::getTerminDauer(), array('style' => 'width:253px;')); ?><span style="display: inline-block;padding-left: 10px;padding-top: 3px;">Minuten</span>
                <?php echo $form->error($model, 'dauer'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model, 'zeit'); ?>
                <?php echo $form->dropDownList($model, 'zeit', Standardkalender::getZeitSlots() /*,array('value' => Standardkalender::getArrayWert(Standardkalender::getZeitSlots(), $model->zeit))*/); ?>
                <?php echo $form->error($model, 'zeit'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model, 'datum'); ?>               
                <?
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'datum',
                    // additional javascript options for the date picker plugin
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => 'dd-mm-yy',
                        'minDate' => '0d',
                    ),
                    'htmlOptions' => array(
                        //'style' => 'height:20px;',
                        'class' => 'datePicker'
                    ),
                ));
                ?> 
                <?php echo $form->error($model, 'datum'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model, 'bestatigung'); ?>
                <?php echo $form->checkBox($model, 'bestatigung'); ?>
                <?php echo $form->error($model, 'bestatigung'); ?>
            </div>
        </div>

    </div>

    <div class="clearBoth"></div>
    <div class="row buttons" style="margin-top: 20px;">
        <label for=""></label>
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Erfassen') : Yii::t('app', 'Speichern')); ?>
        <? if ($model->isNewRecord): ?>
            <?php echo CHtml::submitButton(Yii::t('app', 'Termin eintragen'), array('submit' => 'other/action')); ?>
        <? endif; ?>
    </div>

    <?php $this->endWidget(); ?>
    <script>
        /* Kisanth Ketheeswaran
         * Anzeigen der dazugehörigen Checkboxliste */
        var previous
        $("#Terminplan_nachname").change(function() {
            var thisOption = $('option:selected').attr('value');
            console.log(thisOption);
            var option = $('option:selected', this);
            var optionId = option.attr('value');
            console.log(optionId);
            if (confirm('<?php echo Yii::t('app', 'Beim wechseln werden die gehakten Boxen abgehakt'); ?>')) {
                $('.fragenCont').find('input[type=checkbox]:checked').removeAttr('checked');
                $('.planFragenCont').each(function() {
                    var contId = $(this).attr('data-id');
                    if (contId == optionId) {
                        $(this).fadeIn();
                    } else {
                        $(this).fadeOut();
                    }
                });
                $('.fragenCont').fadeIn();
            }
        });
        jQuery("form").submit(function (e) {
            var self = this;
            e.preventDefault();
            if(pruefeObBehandlungGewaehlt()){
                self.submit();
            }else{
                $('.errorBehandlung').text("Bitte wählen Sie mindestens eine Behandlung aus.");
            }
            return false; //is superfluous, but I put it here as a fallback
        });

        function pruefeObBehandlungGewaehlt() {
            var valid = false
            var i = 0;
            $('.fragenCont').find('input:checkbox').each(function () {
                if ($(this).is(':checked')) {
                    valid = true;
                    return valid;
                }
            });
            return valid;
        }
    </script>
</div><!-- form -->