<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'galerie-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

    <?php if (!$sourceModel->isNewRecord) : ?>
    <div class="locale">
      <ul>
          <?php foreach (LocaleModel::model()->findAll(array('order' => 'position ASC')) as $locale) : ?>
              <li class="<?php echo $locale->cssStatus($_GET['locale']); ?>"><?php echo CHtml::link($locale->label, array('galerie/index/update', 'id' => $sourceModel->id, 'locale' => $locale->id)); ?></li>
        <?php endforeach; ?>
        </ul>
        <div class="line"></div>
    </div>
    <?php endif; ?>

    <?php if ($sourceModel->isNewRecord) : ?>
    <div class="row">
        <?php echo $form->labelEx($model,'locale_id'); ?>
        <?php
            if ($sourceModel->isNewRecord) {
                //$model->locale_id = $_GET['lang'];
                $model->locale_id = LocaleModel::getDefault()->id;               
            }
        ?>
        <p class="left" style="margin: 0; padding-top: 3px; font-weight: bold;"><?php echo LocaleModel::model()->findByPk($model->locale_id)->label; ?></p>
    </div>
    <?php endif; ?>

	<div class="row">
            <?php echo $form->hiddenField($model,'locale_id',array('value' => $model->locale_id)); ?>
            <?php echo $form->hiddenField($model,'unit_hash',array('value' => $sourceModel->isNewRecord ? uniqid() : $sourceModel->unit_hash)); ?>
        </div>
		<!--<?php echo $form->labelEx($model,'titel'); ?>
		<?php echo $form->textField($model,'titel',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'titel'); ?>
	
    
        <div class="row">
		<?php echo $form->labelEx($model,'untertitel'); ?>
		<?php echo $form->textField($model,'untertitel'); ?>
		<?php echo $form->error($model,'untertitel'); ?>
	</div>-->
    
        <div class="row">
		<?php echo $form->labelEx($model,'bild'); ?>
		<?php echo $form->fileField($model,'bild'); ?>
            <?php if (!empty($model->bild)) : ?>
            <p style="float: left; margin-top: 4px; margin-left: 5px;"><a href="/images/media/galerie/<?php echo $model->bild; ?>" target="_blank" /><?php echo $model->bild; ?></a></p>
            <p style="float: left; margin-top: 5px; margin-left: 10px; font-size: 0.9em; color: red;"><a class="deletefile" style="color: red;" href="javascript: void(0);"><?php echo Yii::t('app', 'löschen'); ?></a></p>
            <?php endif; ?>
		<?php echo $form->error($model,'bild'); ?>
	</div>
    
        <div class="row">
		<?php echo $form->labelEx($model,'position'); ?>
		<?php echo $form->textField($model,'position',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'position'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',Kunde::listDataStatus()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
	    <label for=""></label>
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Erfassen') : Yii::t('app', 'Speichern')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
<!--
    jQuery('a.deletefile').click(function() {
        if (confirm('<?php echo Yii::t('app', 'Wirklich löschen'); ?>')) {
            var $this = jQuery(this);
            jQuery.ajax({
                'url': '/admin/galerie/index/deleteBild',
                'type': 'post',
                'data': 'id=<?php echo $model->id; ?>',
                'success': function() {
                    $this.parent().siblings('p').remove();
                    $this.parent().remove();
                }
            });
        }
    });
//-->
</script>