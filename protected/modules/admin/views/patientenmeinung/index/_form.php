<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'patientenmeinung-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data'
    )
)); ?>

    
    <div class="row">
            <?php echo $form->labelEx($model,'bild'); ?>
            <?php echo $form->fileField($model,'bild'); ?>

            <?php if (!empty($model->bild)) : ?>
                <? if($model->bild != "noPicture"): ?>
                    <p style="float: left; margin-top: 4px; margin-left: 5px;"><a href="/images/media/patientenmeinung/<?php echo $model->bild; ?>" target="_blank" /><?php echo $model->bild; ?></a></p>
                    <!--<p style="float: left; margin-top: 5px; margin-left: 10px; font-size: 0.9em; color: red;"><a data-pic="bild" class="deletefile" style="color: red;" href="javascript: void(0);"><?php echo Yii::t('app', 'löschen'); ?></a></p>-->
                <?php endif; ?>
            <?php endif; ?>
            <?php echo $form->error($model,'bild'); ?>
        </div>
    
        <div class="row">
		<?php echo $form->labelEx($model,'position'); ?>
		<?php echo $form->textField($model,'position'); ?>
		<?php echo $form->error($model,'position'); ?>
	</div>
    
	<div class="row buttons">
	    <label for=""></label>
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Erfassen') : Yii::t('app', 'Speichern')); ?>
	</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
<!--
    jQuery('a.deletefile').click(function() {
        if (confirm('<?php echo Yii::t('app', 'Wirklich löschen'); ?>')) {
            var $this = jQuery(this);
            jQuery.ajax({
                'url': '/admin/patientenmeinung/index/deleteFile',
                'type': 'post',
                'data': 'id=<?php echo $model->id; ?>',
                'success': function() {
                    $this.parent().siblings('p').remove();
                    $this.parent().remove();
                }
            });
        }
    });
//-->
</script>
</div><!-- form -->