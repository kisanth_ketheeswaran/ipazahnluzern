<?php if (Yii::app()->user->hasFlash('success')) : ?>
<div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<?php //echo $this->renderPartial('_form', array('model'=>$model, 'sourceModel' => $sourceModel)); ?>

<div class="root">
  <?php echo CHtml::link(Yii::t('app', 'Kontakt hinzufügen'), array('/admin/contact/index/create'), array('class' => 'add')); ?>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'glossary-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'summaryText' => '',
	'columns'=>array(
            'company','address',
		array(
            'name' => 'status',
            'filter' => Contact::listDataStatus(),
            'value' => 'Contact::listDataStatus($data->status)'
        ),
        array(
            'class'=>'CButtonColumn',
            'template' => '{delete} {update}'
        ),
    ),
));?>
