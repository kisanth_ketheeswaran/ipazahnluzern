<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="successMsg"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php endif; ?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'contact-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php if (!$sourceModel->isNewRecord) : ?>
        <div class="locale">
            <ul>
                <?php foreach (LocaleModel::model()->findAll(array('order' => 'position ASC')) as $locale) : ?>
                    <li class="<?php echo $locale->cssStatus($_GET['locale']); ?>"><?php echo CHtml::link($locale->label, array('contact/index/update', 'id' => $sourceModel->id, 'locale' => $locale->id)); ?></li>
                <?php endforeach; ?>
            </ul>
            <div class="line"></div>
        </div>
    <?php endif; ?>

    <?php if ($sourceModel->isNewRecord) : ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'locale_id'); ?>
            <?php
            if ($sourceModel->isNewRecord) {
                //$model->locale_id = $_GET['lang'];
                $model->locale_id = LocaleModel::getDefault()->id;
            }
            ?>
            <p class="left" style="margin: 0; padding-top: 3px; font-weight: bold;"><?php echo LocaleModel::model()->findByPk($model->locale_id)->label; ?></p>
        </div>
    <?php endif; ?>
    <div class="row">
                <?php echo $form->hiddenField($model, 'locale_id', array('value' => $model->locale_id)); ?>
        <?php echo $form->hiddenField($model, 'unit_hash', array('value' => $sourceModel->isNewRecord ? uniqid() : $sourceModel->unit_hash)); ?>
		<?php echo $form->labelEx($model,'description'); ?>
		<div class="floatLeft">
		  <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
            <script type="text/javascript">
              CKEDITOR.replace('Contact[description]', { 
                'height': 240,
                'width': 780
              });
            </script>
		</div>
		<?php echo $form->error($model,'description'); ?>
    </div>

    <div class="row buttons">
        <label for=""></label>
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Erfassen') : Yii::t('app', 'Speichern')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->