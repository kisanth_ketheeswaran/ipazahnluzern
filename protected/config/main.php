<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=> Yii::t('app', 'Websitename'),
  'sourceLanguage' => 'de',
  'defaultController' => 'website/index',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
    'application.models.*',
    'application.forms.*',
    'application.components.*',
    'application.helpers.*',
    'application.vendors.*'
	),

	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'gii99',
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('*','::1'),
		),
		'website',
		'admin' => array(
		  'defaultController' => 'structure/index',
		)
	),

	// application components
	'components'=>array(
    'user'=>array(
      'class' => 'WebUser',
      // enable cookie-based authentication
      'allowAutoLogin'=>true,
      'loginUrl' => '/website/index/login'
    ),

    'image' => array(
      'class' => 'ext.image.CImageComponent',
      'driver' => 'GD',
      'params' => array('directory' => Yii::app()->basePath . '/../images/thumb'),
    ),
    
    'file' => array(
      'class' => 'application.extensions.cfile.CFile',
    ),
    
    'urlManager'=>array(
      'class' => 'UrlManager',
      'urlFormat'=>'path',
      'rules'=>array(
        'logout' => 'website/index/logout',
        'login' => 'website/index/login',
        'jobs' => 'website/index/job',
        '<controller:\w+>/<id:\d+>'=>'<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
        '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
      ),
      'showScriptName' => false
    ),

    'db'=>array(
      'connectionString' => 'mysql:host=localhost;dbname=zahnluzern',
      'emulatePrepare' => true,
      'username' => 'root',
      'password' => '',
      'charset' => 'utf8',
    ),
    
    'session' => array(
      'class' => 'CDbHttpSession',
      'sessionTableName' => 'session',
      'connectionID' => 'db'
    ),
    
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'website/index/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'kk@firegroup.com',
		'sitemapUrl' => 'http://www.website.com'
	),
);