<?php 

class WidgetSubNavigation extends CWidget
{
  public $items = array();
  public function init()
  {
     $currentLevel = 1;
     $cNav = Structure::getCurrentNavigation();
     if (empty($cNav)) {
       $this->items = array();
     } else {
       $this->items = Structure::getMenuItems($cNav->getTopLevelNavigation()->id, Yii::app()->language, false, 'in_main = 1 AND status = "active"', 1);
     }
  }
  
  public function run()
  {
    $this->render('subNavigation');
  }
}
