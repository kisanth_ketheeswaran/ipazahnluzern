<?php 

class WidgetLanguage extends CWidget
{
  public $items = array();
  public function init()
  {
    $cNav = Structure::getCurrentNavigation();
    if ($cNav->source_id == 0)
        $sourceNav = $cNav;
    else
        $sourceNav = Structure::getSource($cNav->source_id);
    
    foreach (LocaleModel::model()->findAll(array('condition' => 'status = "active"', 'order' => 'position ASC')) as $locale) {
      $label = empty($locale->shortcut) ? $locale->label : $locale->shortcut;
      $active = $locale->id == Yii::app()->language ? true : false;
      
      if ($locale->id == LocaleModel::getDefault()->id) {
      	if (!empty($sourceNav)) {
      	     $localeUrl = $sourceNav->getUrl() . '/?tolang=' . $locale->id;
      	} else {
      		$localeUrl = '/?tolang=' . $locale->id;
      	}
      } else {
        $localeModel = Structure::model()->find('source_id = :source AND locale_id = :locale', array(':source' => $sourceNav->id, ':locale' => $locale->id));
        if (!empty($sourceNav)) {
            $localeUrl = empty($localeModel) ? '/?tolang=' . $locale->id : $localeModel->getUrl() . '/?tolang=' . $locale->id;
        } else {
        	$localeUrl = '/?tolang=' . $locale->id;
        }
      }

      
      $this->items[] = array('label'=> $label, 'url' => $localeUrl, 'active' => $active, 'linkOptions' => array('title' => $locale->label));
    }
  }
  
  public function run()
  {
    $this->render('language');
  }
}
