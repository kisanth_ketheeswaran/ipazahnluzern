<?php 

class WidgetMainNavigation extends CWidget
{
  public $items = array();
  public function init()
  {
     $currentLevel = 1;
     $this->items = Structure::getMenuItems(0, Yii::app()->language, false, 'in_main = 1 AND status = "active"', 3);
  }
  
  public function run()
  {
    $this->render('mainNavigation');
  }
}
