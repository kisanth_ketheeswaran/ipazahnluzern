<?php $this->widget('zii.widgets.CMenu',array(
        'items' => $this->items,
        'firstItemCssClass' => 'first',
        'lastItemCssClass' => 'last',
        'encodeLabel' => false,
            'htmlOptions'=>array('class'=>'mainmenu nav navbar-nav pull-right'),
    'submenuHtmlOptions' => array('class' => 'dropdown-menu'),
        )
      );
?>