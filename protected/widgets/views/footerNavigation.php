<?php $colCounter = 1; ?>
<?php /* foreach ($this->items as $item) : ?>
  <div class="column column_<?php echo $colCounter; ?>">
  <h4><?php echo CHtml::link($item['label'], $item['url'], array('title' => $item['label'])); ?></h4>
  <?php
  $this->widget('zii.widgets.CMenu',array(
  'items' => Structure::getMenuItems($item['id'], Yii::app()->language, false, 'in_footer = 1', 1, 1),
  'firstItemCssClass' => 'first',
  'lastItemCssClass' => 'last',
  'encodeLabel' => false
  )
  );
  ?>
  </div>
  <?php $colCounter++; ?>
  <?php endforeach; */ ?>
<?php

$this->widget('zii.widgets.CMenu', array(
    'items' => $this->items,
    'firstItemCssClass' => 'first',
    'lastItemCssClass' => 'last',
    'encodeLabel' => false,
    'htmlOptions'=>array('class'=>'flat'),
        )
);
?>
