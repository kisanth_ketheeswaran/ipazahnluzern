<?php 

class WidgetHeaderNavigation extends CWidget
{
  public $items = array();
  
  public function init()
  {
     $this->items = Structure::getMenuItems(0, Yii::app()->language, false, 'in_header = 1 AND status = "active"', 1);
  }
  
  public function run()
  {
    $this->render('headerNavigation');
  }
}
