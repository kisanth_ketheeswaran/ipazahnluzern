<?php $this->beginContent('//layouts/main'); ?>
<div class="leftbar">
  LEFT
</div>
<div class="main main-left">
  <?php if (!empty($this->h1)) : ?>
  <h1><?php echo $this->h1; ?></h1>
  <?php endif; ?>
  <div class="view-content">
    <?php echo $content; ?>
  </div>
</div>
<?php $this->endContent(); ?>
