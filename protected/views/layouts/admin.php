<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::app()->language; ?>" lang="<?php echo Yii::app()->language; ?>">
  <head>
    <?php $this->renderPartial('application.views.layouts.partials.adminHead'); ?>
  </head>
  <body>
    <?php $this->renderPartial('application.views.layouts.partials.adminNavigation'); ?>   
    <div id="adminPage">
      <?php $this->renderPartial('application.views.layouts.partials.adminBody', array('content' => $content)); ?>
    </div>
  </body>
</html>