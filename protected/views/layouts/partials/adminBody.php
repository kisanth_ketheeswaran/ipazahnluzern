<div id="adminBody">
  <div class="inner">
    <?php if (!empty($this->h1)) : ?>
    <h1><?php echo $this->h1; ?></h1>
    <?php endif; ?>
    <?php echo $content; ?>
  </div>
</div>