<?
$pageHeader = Page::model()->getHeaderLayout($this->cPage->id);
?>

<div id="l_header" class="header header-<? echo $pageHeader ?  'big' : 'small'; ?>">
    <div id="l_navlogo">
        <div class="inner">
            <div id="l_logo">
                <?php //$logo = CHtml::image('/images/own/logo-company.png', Yii::t('site', 'Site Name'), array('title' => Yii::t('site', 'Company Name'))); ?>
                <?php //echo CHtml::link($logo, '/', array('site', 'Site Name')); ?>
            </div>

            <div id="l_navigation">
                <nav class="navbar-fixed-top navbar" data-offset-top="84" data-spy="affix">
                    <div class="container">
                        <a class="navbar-brand" id="logoHeader" href="/">Zahnarzt in Luzern.</a>
                         <button class="navbar-toggle menu-toggle" type="button">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
                        <div class="navbar-menu">
                            <button class="close-menu menu-toggle"></button>
                            <?php $this->widget('application.widgets.WidgetMainNavigation'); ?>
                        </div>
                    </div>
                </nav>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>

</div>
