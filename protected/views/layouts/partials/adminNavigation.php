<?php if (App::hasAccess('admin')) : ?>
    <div id="adminNavigation">
        <?php
        $isSome = Contact::model()->find();
        $urlContact = 'create';
        if ($isSome == NULL || $isSome == 0 || $isSome == '') {
            echo '';
        } else {
            $urlContact = 'update/id/' . $isSome->id . '/locale/de';
        }
        $this->widget('zii.widgets.CMenu', array(
            'items' => array(
                array('label' => Yii::t('app', 'Website'), 'url' => '/', 'active' => App::isMenuItemActive('website', 'index')),
                array('label' => Yii::t('app', 'Seitenstruktur'), 'visible' => true, 'url' => array('/admin/structure/index'), 'active' => App::isMenuItemActive('admin', 'structure/index')),
                array('label' => Yii::t('app', 'Homeslider'), 'visible' => true, 'url' => array('/admin/homeslider/index'), 'active' => App::isMenuItemActive('admin', 'homeslider/index')),
                array('label' => Yii::t('app', 'Team'), 'visible' => true, 'url' => array('/admin/team/index'), 'active' => App::isMenuItemActive('admin', 'team/index')),
                array('label' => Yii::t('app', 'Kunde'), 'visible' => true, 'url' => array('/admin/kunde/index'), 'active' => App::isMenuItemActive('admin', 'kunde/index')),
                array('label' => Yii::t('app', 'Angebot'), 'visible' => true, 'url' => array('/admin/angebot/index'), 'active' => App::isMenuItemActive('admin', 'angebot/index')),
                array('label' => Yii::t('app', 'Galerie'), 'visible' => true, 'url' => array('/admin/galerie/index'), 'active' => App::isMenuItemActive('admin', 'galerie/index')),
                array('label' => Yii::t('app', 'Mehrere Galerien'), 'visible' => true, 'url' => array('/admin/gallery/index'), 'active' => App::isMenuItemActive('admin', 'gallery/index')),
                array('label' => Yii::t('app', 'Kontaktseite'), 'visible' => true, 'url' => array('/admin/kontaktseite/index'), 'active' => App::isMenuItemActive('admin', 'kontaktseite/index')),
                array('label' => Yii::t('app', 'Footerkontakt'), 'visible' => true, 'url' => array('/admin/contact/index/' . $urlContact . ''), 'active' => App::isMenuItemActive('admin', 'contact/index')),
                array('label' => Yii::t('app', 'Terminplan'), 'visible' => true, 'url' => array('/admin/terminplan/index'), 'active' => App::isMenuItemActive('admin', 'terminplan/index')),
                //array('label' => Yii::t('app', 'Buchungs Übersicht'), 'visible' => true, 'url' => array('/admin/buchung/index'), 'active' => App::isMenuItemActive('admin', 'terminplan/index')),
                //array('label' => Yii::t('app', 'Portfolio'), 'visible' => true, 'url' => array('/admin/reference/index'), 'active' => App::isMenuItemActive('admin', 'reference/index')),
                //array('label' => Yii::t('app', 'Jobs'), 'visible' => true, 'url' => array('/admin/job/index'), 'active' => App::isMenuItemActive('admin', 'job/index')),
                //array('label' => Yii::t('app', 'Slider'), 'visible' => true, 'url' => array('/admin/slider/index'), 'active' => App::isMenuItemActive('admin', 'slider/index')),
                //array('label' => Yii::t('app', 'Presse'), 'visible' => true, 'url' => array('/admin/press/index'), 'active' => App::isMenuItemActive('admin', 'press/index')),
                //array('label' => Yii::t('app', 'Glossar'), 'visible' => true, 'url' => array('/admin/glossary/index'), 'active' => App::isMenuItemActive('admin', 'glossary/index')),
                array('label' => Yii::t('app', 'FAQ'), 'visible' => true, 'url' => array('/admin/faq/index'), 'active' => App::isMenuItemActive('admin', 'faq/index')),
                array('label' => Yii::t('app', 'Zähler'), 'visible' => true, 'url' => array('/admin/zaehler/index'), 'active' => App::isMenuItemActive('admin', 'zaehler/index')),
                array('label' => Yii::t('app', 'Patientenmeinung'), 'visible' => true, 'url' => array('/admin/patientenmeinung/index'), 'active' => App::isMenuItemActive('admin', 'patientenmeinung/index')),
            ),
            'htmlOptions' => array('style' => 'float: left; overflow: hidden;')
        ));
        ?>
        <ul class="logout">
            <li><a href="/logout">Logout</a></li>
        </ul>
    </div>
<?php endif; ?>

<?php if (Yii::app()->user->role == 'behandler') : ?>
    <div id="adminNavigation">
        <?php
        $isSome = Contact::model()->find();
        $urlContact = 'create';
        if ($isSome == NULL || $isSome == 0 || $isSome == '') {
            echo '';
        } else {
            $urlContact = 'update/id/' . $isSome->id . '/locale/de';
        }
        $this->widget('zii.widgets.CMenu', array(
            'items' => array(
                array('label' => Yii::t('app', 'Terminplan'), 'visible' => true, 'url' => array('/admin/terminplan/index'), 'active' => App::isMenuItemActive('admin', 'terminplan/index')),
                array('label' => Yii::t('app', 'Buchungs Übersicht'), 'visible' => true, 'url' => array('/admin/buchung/index'), 'active' => App::isMenuItemActive('admin', 'terminplan/index')),
                array('label' => Yii::t('app', 'Termin Übersicht'), 'visible' => true, 'url' => array('/admin/termin/index'), 'active' => App::isMenuItemActive('admin', 'terminplan/index')),
            ),
            'htmlOptions' => array('style' => 'float: left; overflow: hidden;')
        ));
        ?>
        <ul class="logout">
            <li><a href="/logout">Logout</a></li>
        </ul>
    </div>
<?php endif; ?>