<?php echo CHtml::metaTag('text/html; charset=utf-8', null, 'content-type'); ?>
<?php echo CHtml::metaTag(Yii::app()->language, 'language'); ?>

<!-- blueprint CSS framework -->
<!--[if lt IE 8]>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/ie.css', 'screen, projection'); ?>
<![endif]-->

<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/admin/admin.css', 'screen, projection'); ?>

<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/ckeditor/ckeditor.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/admin/admin.js'); ?>

<?php echo CHtml::tag('title', array(), $this->pageTitle); ?>
