<?php
	$localId = $this->cPage->structure->locale_id;
	$conditions = "type='nav' AND locale_id = '".$localId."' AND status='active' ORDER BY position";
	$navigationElements=Structure::model()->findAll($conditions);
	
	$site_record = Structure::model()->find('type = "site" AND parent_id=0');
    echo '<div class="rowcontainer">';
	$counter = 1;
	$rowamount = count(Structure::model()->findAll('type = "nav" AND in_footer=1 AND status = "active" AND parent_id=' . $site_record->id . ' AND locale_id="'.$localId.'" ORDER BY position ASC'));
    foreach (Structure::model()->findAll('type = "nav" AND in_footer=1 AND status = "active" AND parent_id=' . $site_record->id . ' AND locale_id="'.$localId.'" ORDER BY position ASC') as $structure) {
		if($counter == 11){
			break 1;
		}
		if($rowamount == $counter || $counter == 5 || $counter == 10){
			echo '<div class="row floatLeft row-'.$counter.' last">';
		}else{
			echo '<div class="row floatLeft row-'.$counter.'">';
		}
		/* Ausgabe von Elternelement */
		echo '<h3>';
		echo CHtml::link($structure->label, '/'.$localId.'/'.$structure->link.'.html');
		echo '</h3>';
		echo '<ul class="row-'.$counter.'">';
		/* Ausgabe von Kindelementen */
		foreach(Structure::model()->findAll("type = 'nav' AND in_footer=1 AND parent_id = '".$structure->id."' AND status = 'active'") as $subElement){
			echo '<li>';
			echo CHtml::link($subElement->label, '/'.$localId.'/'.$structure->link.  '/' . $subElement->link .'.html');	
			echo '</li>';
		}
		echo '</ul>';
		echo '</div>';
		$counter++;
	}
	echo '</div>';	
?>
