<?php echo CHtml::metaTag('text/html; charset=utf-8', null, 'content-type'); ?>
<?php echo CHtml::metaTag(Yii::app()->language, 'language') . "\n"; ?>
<?php if(is_object($this->cPage)): ?>
	<?php echo CHtml::metaTag($this->cPage->meta_keywords, 'keywords') . "\n"; ?>
	<?php echo CHtml::metaTag($this->cPage->meta_description, 'description') . "\n"; ?>
	<?php echo CHtml::metaTag($this->cPage->meta_title, 'title') . "\n"; ?>
	<?php echo $this->cPage->meta_tags; ?>
<?php endif; ?>
<meta name="viewport" content="width=device-width, initial-scale=1"/>

<!--
    
====================================================================================================
<?php echo Yii::t('site', 'Website von www.firegroup.com - 044 534 6666 - info@firegroup.com'); ?>

====================================================================================================
<?php echo Yii::t('site', 'Beratung - Konzept - Design - Programmierung - Unterhalt - Marketing'); ?>

====================================================================================================
<?php echo Yii::t('site', 'Websites - Webapplikationen - Onlineshops - Mobile - Social Media'); ?>

====================================================================================================

//-->

<!-- blueprint CSS framework -->
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/screen.css', 'screen, projection'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/print.css', 'print'); ?>
<!--[if lt IE 8]>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/ie.css', 'screen, projection'); ?>
<![endif]-->
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/form.css', 'screen, projection'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/layout.css', 'screen, projection'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/terminanfrage.css', 'screen, projection'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/jquery.bxslider.css', 'screen, projection'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/js/website/fancybox/jquery.fancybox.css', 'screen, projection'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/own.css', 'screen, projection'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/bootstrap-lightbox.min.css', 'screen, projection'); ?>
<!-- CSS von Denver Template -->
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/animate.css', 'screen, projection'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/bootstrap.min.css', 'screen, projection'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/font-awesome.min.css', 'screen, projection'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/style.css', 'screen, projection'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/responsive.css', 'screen, projection'); ?>


<?php //echo CHtml::cssFile(Yii::app()->baseUrl . '/css/website/onepage-scroll.css', 'screen, projection'); ?>

<?php //Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<?php //Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<!-- NEW jQUERY + jQUERY UI -->
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/website/jquery.min.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/website/jquery-ui.min.js'); ?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/website/jquery.bxslider.min.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/website/jquery.scrollTo.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/website/fancybox/jquery.fancybox.pack.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/website/jquery.livequery.min.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/website/website.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/website/own.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/website/bootstrap-lightbox.min.js'); ?>
<?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/website/jquery.onepage-scroll.min.js'); ?>

<?php 
  if (empty($this->browserTitle)) {
    $this->browserTitle = $this->h1;
  }
?>

<?php echo CHtml::tag('title', array(), CHtml::encode(Yii::app()->name . ': ' . $this->browserTitle)); ?>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->