<div class="clear"></div>
<div id="l_footer">
    <footer class="footer">
        <div class="container irow">
            <div class="col-sm-4">
                <p class="copyright">&copy; 2015. All Rights Reserved</p>
            </div>
            <div class="col-md-2 col-sm-2">
                <?php $this->widget('application.widgets.WidgetFooterNavigation'); ?>
            </div>
            <div class="col-sm-3 col-sm-offset-1 hidden-simple">
                <address>
                    <? $kontakt = Contact::model()->find('locale_id=:locale',array(':locale'=>  Yii::app()->language));
                        echo $kontakt->description;
                    ?> 
                </address>
            </div>
            <div class="col-sm-2 hidden-simple text-right">
                <nav class="social">
                    <a href="https://www.facebook.com/pages/ZZL-Zahnarzt-Luzern/374853345976062" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="https://plus.google.com/113255623282152961954/posts" target="_blank">
                        <i class="fa fa-google-plus"></i>
                    </a>
                    <a href="https://www.xing.com/companies/zzl-zahngesundheitszentrumluzern" target="_blank">
                        <i class="fa fa-xing"></i>
                    </a>
                    <a href="https://www.linkedin.com/in/zahngesundheitszentrumluzern" target="_blank">
                        <i class="fa fa-linkedin"></i>
                    </a>
                </nav>
            </div>
        </div>
    </footer>
</div>

