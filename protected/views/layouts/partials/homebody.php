<?
$homesliders = Homeslider::model()->findAll('locale_id = :locale AND status="active" order by position asc', array(':locale' => Yii::app()->language))
?>
<div id="l_body" class="<?php echo Yii::app()->language; ?>">
    <div class="bodyContainer" style="">
        <div class="slider oneslider fullscreen">
            <ul>
                <? foreach ($homesliders as $slid): ?>
                    <li class="bg-slide bg-top" style="background-image: url('/images/media/homeslider/<? echo $slid->bild ?>');">
                        <div class="container fullheight">
                            <div class="bus-descr animate left-top-pos homeBox" data-animate="fadeInLeft">
                                <a class="homeLink" href="<? echo $slid->link; ?>"></a>
                                <h4><? echo $slid->titel; ?></h4>
                                <p><? echo $slid->text; ?></p>
                            </div>
                        </div>
                    </li>
                <? endforeach; ?>
            </ul>
            <div class="slider-nav">
                <a href="" class="prev square">&larr;</a>
                <nav class="nav-pages"></nav>
                <a href="" class="next square">&rarr;</a>
            </div>
        </div>
        <div class="inner container">
            <?php echo $content; ?>
        </div>
    </div>

</div>

<script type="text/javascript">
<!--

//-->
</script>
