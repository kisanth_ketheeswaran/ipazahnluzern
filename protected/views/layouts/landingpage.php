<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::app()->language; ?>" lang="<?php echo Yii::app()->language; ?>">
	<head>
		<?php $this->renderPartial('application.views.layouts.partials.head'); ?>
	</head>
	<body class="<?php echo Yii::app()->language; ?>">
	  <?php if (Yii::app()->user->role == 'admin') : ?>
	      <?php $this->renderPartial('application.views.layouts.partials.adminNavigation'); ?> 
	  <?php endif; ?>
		<div id="l_page">
		    <?php $this->renderPartial('application.views.layouts.partials.header'); ?>
			<?php $this->renderPartial('application.views.layouts.partials.landing_body', array('content' => $content)); ?>
			<?php $this->renderPartial('application.views.layouts.partials.footer'); ?>
		</div>
	</body>
</html>