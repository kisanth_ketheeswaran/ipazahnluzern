<?php $this->beginContent('//layouts/main'); ?>
<div class="leftbar">
  LEFT
</div>
<div class="main main-all">
  <?php if (!empty($this->h1)) : ?>
  <h1 class="pageTitle"><?php echo $this->h1; ?></h1>
  <?php endif; ?>
  <div class="view-content">
    <?php echo $content; ?>
  </div>
</div>
<div class="rightbar">
  RIGHT
</div>
<?php $this->endContent(); ?>