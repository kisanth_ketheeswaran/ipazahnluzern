<?php
$this->beginContent('//layouts/main');
$pageHeader = Page::model()->getHeaderLayout($this->cPage->id);
?>
<div class="main main-none">
    <?php if (!empty($this->h1)) : ?>
        <h1 class="pageTitle"><?php echo $this->h1; ?></h1>
    <?php endif; ?>
    <div class="breadcrumbCont">
        <div class="pull-right search">
            <button data-target=".search-form" data-toggle="collapse" type="button" class="fa    fa-search"></button>
            <form class="search-form fade collapse" action="/website/index/search" method="get">
                <input name="sucher" type="text" class="form-control" placeholder="Tippen und Eingabetaste drücken">
                <span class="fa fa-times" data-toggle="collapse" data-target=".search-form"></span>
            </form>
        </div>
        <ol class="breadcrumb pull-right">
            <li><a href="#"><? echo $this->h1 ?></a></li>
        </ol>
    </div>
    <div class="clearBoth"></div>
    <div class="view-content <? echo $pageHeader ? '' : 'noBigHeader' ?>">
        <?php echo $content; ?>

        <?
        $hasEmotion = false;
        $sliders = Emotionpic::model()->findAll('page_idfs = :pageid', array(':pageid' => $this->cPage->id));
        if (!empty($sliders)):
            $hasEmotion = true;
            ?>
            <div class="row slider oneslider magnific-wrap">
                <ul>
                    <?
                    foreach ($sliders as $slid):
                        ?>
                        <li>
                            <a href="/images/option/<? echo $slid->name ?>" class="magnific">
                                <img src="/images/option/<? echo $slid->name ?>" alt="">
                            </a>
                        </li>
                    <? endforeach; ?>
                </ul>
                <div class="slider-nav">
                    <a href="" class="prev">&larr;</a>
                    <nav class="nav-pages"></nav>
                    <a href="" class="next">&rarr;</a>
                </div>
                <?
            elseif (count($sliders) == 1):
                foreach ($sliders as $slid):
                    ?>
                    <img src="/images/option/<? echo $slid->name ?>" alt="">
                <? endforeach; ?>
            </div>
        </div>
    <? endif; ?>
    <!-- /.slider -->
    <? if (!empty($this->cPage->obertitel) || !empty($this->cPage->oberleadtext) || !empty($this->cPage->linkertext) || !empty($this->cPage->rechtertext)): ?>
        <div class="row irow <? $hasEmotion ?>">
            <div class="col-sm-4">
                <? if(!empty($this->cPage->obertitel)): ?>
                    <h3><? echo $this->cPage->obertitel ?><small>&mdash;</small></h3>
                <? endif; ?>
            </div>
            <div class="col-sm-8">
                <div class="lead"><? echo $this->cPage->oberleadtext ?></div>
                <div class="row">
                    <div class="col-sm-6">
                        <? echo $this->cPage->linkertext ?>
                    </div>
                    <div class="col-sm-6">
                        <? echo $this->cPage->rechtertext ?>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
    <? endif; ?>

    <? if (!empty($this->cPage->untertitel) || !empty($this->cPage->unterleadtext) || !empty($this->cPage->cms_content)): ?>

        <h2><? echo $this->cPage->untertitel ?></h2>
        <div class="extra"><? echo $this->cPage->unterleadtext ?></div>
<? if (!empty($this->cPage->untertitel) && !empty($this->cPage->unterleadtext)): ?>
        <hr class="sm-margin"/>
<? endif; ?>
        <p><? echo $this->cPage->cms_content ?></p>
    <? endif; ?>



</div>
</div>
<?php $this->endContent(); ?>