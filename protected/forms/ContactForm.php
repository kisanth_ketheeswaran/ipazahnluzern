<?php


class ContactForm extends CFormModel
{
 
  public $message;
  public $first_name;
  public $last_name;
  public $company;
  public $phone;
  public $email;
  public $accessible;


  public function rules()
  {
    return array(
      array('message, last_name, email', 'required'),
      array('company, accessible', 'length'),
      array('email', 'email'),
        array('message, phone,first_name, last_name, email','safe'),
    );
  }

  /**
   * Declares attribute labels.
   */
  public function attributeLabels()
  {
    return array(
        'message' => Yii::t('site', 'Ihre Nachricht'),
        'first_name' => Yii::t('site', 'Vorname'),
        'last_name' => Yii::t('site', 'Nachname'),
        'company' => Yii::t('site', 'Firma'),
        'phone' => Yii::t('site', 'Telefon'),
        'email' => Yii::t('site', 'E-Mail'),
        'accessible' => Yii::t('site', 'Erreichbar')
    );
  }
}
