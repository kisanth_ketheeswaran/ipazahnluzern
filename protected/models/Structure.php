<?php

class Structure extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Structure the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'structure';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('label, type, link, status', 'required'),
			array('in_main, in_header, in_footer, is_home, is_extern, is_protected, link_redirect, position, source_id, parent_id, is_default, site_id, is_landing', 'numerical', 'integerOnly'=>true),
			array('label, link, point_link, target', 'length', 'max'=>250),
			array('status, target, link_redirect_point', 'length', 'max'=>45),
			array('locale_id', 'length', 'max'=>10),
            array('protected_role, domain, layout', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, label, site_id, layout, type, link, in_main, in_header, in_footer, is_home, is_extern, is_protected, is_landing, protected_role, link_redirect, point_link, target, position, status, source_id, parent_id, locale_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		  'children' => array(self::HAS_MANY, __CLASS__, 'parent_id'),
      'parent' => array(self::BELONGS_TO, __CLASS__, 'parent_id'),
      'page' => array(self::HAS_ONE, 'Page', 'structure_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'label' => Yii::t('app', 'Bezeichnung'),
			'link' => Yii::t('app', 'Link'),
			'is_default' => Yii::t('app', 'Standard-Website'),
			'domain' => Yii::t('app', 'Domain'),
			'in_main' => Yii::t('app', 'In Hauptnavigation'),
			'in_header' => Yii::t('app', 'In Kopfnavigation'),
			'in_footer' => Yii::t('app', 'In Fussnavigation'),
			'is_home' => Yii::t('app', 'Ist Startseite'),
			'is_extern' => Yii::t('app', 'Ist externe Seite'),
            'is_protected' => Yii::t('app', 'Geschützter Bereich'),
		    'protected_role' => Yii::t('app', 'Rollen'),
			'link_redirect' => Yii::t('app', 'Weiterleitung'),
            'point_link' => Yii::t('app', 'Weiterleitungsseite'),
             'target' => Yii::t('app', 'Verhalten'),
			'position' => Yii::t('app', 'Position'),
			'layout' => Yii::t('app', 'Layout'),
			'status' => Yii::t('app', 'Status'),
			'source_id' => Yii::t('app', 'Quellenavigation'),
			'parent_id' => Yii::t('app', 'Elternnavigation'),
			'locale_id' => Yii::t('app', 'Sprache'),
			'is_landing' => Yii::t('app', 'Ist Landingpage'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('type',$this->type);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('in_main',$this->in_main);
		$criteria->compare('in_header',$this->in_header);
		$criteria->compare('in_footer',$this->in_footer);
		$criteria->compare('is_home',$this->is_home);
		$criteria->compare('is_extern',$this->is_extern);
    	$criteria->compare('is_protected',$this->is_protected);
    	$criteria->compare('protected_role',$this->protected_role);
		$criteria->compare('link_redirect',$this->link_redirect);
    	$criteria->compare('point_link',$this->point_link);
    	$criteria->compare('target',$this->target);
		$criteria->compare('position',$this->position);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('source_id',$this->source_id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('locale_id',$this->locale_id,true);
		$criteria->compare('is_landing',$this->is_landing,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	
	public function beforeSave()
	{
	    if ($this->type == 'nav') {
             $roles = '';
        	   if ($this->is_protected == 1 && is_array($this->protected_role)) {
        	     foreach ($this->protected_role as $role) {
        	       $roles .= $role . ',';
        	     }
        	   } else {
        	     $this->is_protected = 0;
        	   }
             $this->protected_role = $roles;
         }

		if($this->link){
			$this->cleanUrl();
		}
	     return parent::beforeSave();
	}
	
	public function afterFind()
	{
	   if ($this->type == 'nav') {
	       $this->protected_role = $this->getProtectedRoles();
       }
	}
	
	
  public function afterSave()
  {
    $siteRoot = $this->getSiteRoot();
    if ($this->type == 'nav') {
        if ($this->is_home == 1) {
          Structure::model()->updateAll(array('is_home' => 0), 'id != :id AND locale_id = :locale AND site_id = :site', array(':site' => $siteRoot->id, ':locale' => $this->locale_id, ':id' => $this->id));  
        }
        
        $this->createPage();
    } else {
        if ($this->is_default == 1) {
          Structure::model()->updateAll(array('is_default' => 0), 'id != :id', array(':id' => $this->id));  
        }
    }
    
    Structure::model()->updateAll(array('site_id' => $siteRoot->id), 'id = :id', array(':id' => $this->id));
    
    unset($_SESSION['piisite']);
    unset($_SESSION['piidomain']);
    App::writeSitemap();
    
    return parent::afterSave(); 
  }

  private function getProtectedRoles()
  {
    return App::removeEmptyValue(explode(',', $this->protected_role));
  }
  
  public function createPage()
  {
    if ($this->isNewRecord) {
      $page = new Page();
      $page->structure_id = $this->id;
      $page->cms_h1 = $this->label;
      $page->cms_content = $this->label;
      $page->meta_keywords = $this->label;
      $page->meta_description = $this->label;
      $page->meta_title = $this->label;
      $page->meta_title = $this->label;
      $page->cms_content_position = 1;
      if ($page->save()) {
        $pageOption = new PageOption();
        $pageOption->page_id = $page->id;
        $pageOption->insert();        
      }
    } 
  }
  
  public function beforeDelete()
  {
    
    if ($this->type == 'nav') {
        $this->page->delete();
    }   
     
    foreach ($this->children as $child) {
      $child->delete(); 
    }
    
    $localeModels = self::model()->findAll('source_id = :source', array(':source' => $this->id));
    foreach ($localeModels as $localeModel) {
      $localeModel->delete(); 
    }
    return parent::beforeDelete();  
  }
  
  public static function getTreeStructure($parentId = 0, $localeId, $condition = null, $orderBy = null)
  {
    $node = array();
    foreach (self::getLevel($parentId, $localeId, $condition, $orderBy) as $structure) {
        $node[$structure->id] = array('navigation' => $structure, 'children' => null);
      if (count($structure->children) > 0) {
        $node[$structure->id]['children'] = self::getTreeStructure($structure->id, $localeId);
      }
    }
    return $node;
  }
  
  private static function getLevel($parentId, $localeId, $condition, $orderBy = null)
  {
    $orderBy = $orderBy ? $orderBy : 'position';
    $condition = empty($condition) ? $condition : $condition . ' AND';
    
    if (Yii::app()->controller->module->id != 'admin')
        $condition .= ' status = "active" AND';
    else
        $condition .= ' id != 0 AND';
    
    return self::model()->findAll($condition . ' parent_id = :parent_id AND locale_id = :locale_id ORDER BY ' . $orderBy .' ASC', array(':parent_id' => $parentId, ':locale_id' => $localeId));
  }
  
  public static function getTreeView($nodes, $hide = false)
  {   
      $html = '';
      if ($hide) $html .= '<ol style="display: none;">';
      else       $html .= '<ol class="treeView">';
      foreach ($nodes as $node) :
          $html .= '<li class="' . $node['navigation']->type . '" id="position_' . $node['navigation']->id . '_' . $node['navigation']->position . '">';
            $html .= '<div class="node">';
	            // expander 
	            $html .= '<div class="expander">';
	              if (count($node['navigation']->children) > 0) :
	                 $cssExpand = isset(Yii::app()->session['structure_' . $node['navigation']->id]) ? 'close' : 'open';
	                 $html .= CHtml::link('', 'javascript: void(0);', array('class' => $cssExpand, 'rel' => $node['navigation']->id));
	              endif;
	            $html .= '</div>';
	            
	            // label
	            $html .= '<div class="label">';
	              $html .= $node['navigation']->label;
	            $html .= '</div>';
	            
	            // action
	            $html .= '<div class="action">';
	              $html .= '<ul>';
	                $html .= '<li>' . CHtml::link('', array('structure/index/create', 'parent' => $node['navigation']->id, 'type' => 'nav'), array('class' => 'down'));
	                $html .= '<li>' . CHtml::link('', array('structure/index/create', 'sibling' => $node['navigation']->id, 'type' => $node['navigation']->type), array('class' => 'right'));
	                $html .= '<li>' . CHtml::link('', array('structure/index/update', 'id' => $node['navigation']->id), array('class' => 'edit'));
	                $html .= '<li>' . CHtml::link('', array('structure/index/delete', 'id' => $node['navigation']->id), array('class' => 'delete'));
	              $html .= '</ul>';
	            $html .= '</div>';
            $html .= '</div>';
	          if (!empty($node['children'])) :
	             $hideOl = isset(Yii::app()->session['structure_' . $node['navigation']->id]) ? false : true;
	             $html .= self::getTreeView($node['children'], $hideOl);
	          endif;
          $html .= '</li>';
      endforeach;
      $html .= '</ol>';
      return $html;
  }
  
  public static function getStatusData()
  {
      return array('active' => Yii::t('app', 'Aktiv'),
                   'inactive' => Yii::t('app', 'Inaktiv'));
  }
  
  public static function getRedirectData()
  {
      return array('child' => Yii::t('app', 'Auf erste Unternavigation'),
                   'other' => Yii::t('app', 'Auf eine andere Seite'));
  }
  
  
  public static function getSource($id)
  {
    return Structure::model()->find('id = :id AND source_id = 0', array(':id' => $id));
  }
  
  public static function getModel($sourceModel, $locale)
  {
    if ($locale->id == $sourceModel->locale_id)
      return $sourceModel;
    else {
      $localeModel = Structure::model()->find('source_id = :source AND locale_id = :locale', array(':source' => $sourceModel->id, ':locale' => $locale->id));
      if (empty($localeModel))
        return $sourceModel;
      else
        return $localeModel; 
    }
  }
  
  public function getLocale($locale)
  {
    $sourceParent = $this->parent;
    
    if ($this->type != 'site') {
        if (!empty($sourceParent))
        {
          $localeParent = Structure::model()->find('source_id = :source AND locale_id = :locale', array(':source' => $sourceParent->id, ':locale' => $locale->id));
          
          
          if (empty($localeParent) && $sourceParent->type != 'site')
            return false;
        }
        
        return Structure::model()->find('source_id = :source AND locale_id = :locale', array(':source' => $this->id, ':locale' => $locale->id));
    } else {
        return $this;
    }
  }

  
  
  public function getUrl($realUrl = false)
  { 
       $link = '/' . $this->locale_id;
  
       if ($this->is_extern) {
          $link = strpos($this->link, 'http') === false ? 'http://' . $this->link : $this->link;
       } else {
		       $parents = array();
		       $this->getParents($parents);     
		       foreach ($parents as $parent) {
		         if ($parent->id != $this->id && $parent->type != 'site')
		          $link .= '/' . $parent->link;
		       }
		       
		       if ($realUrl)
		        $link .= '/' . $this->link . '.html';
		       else {
                if ($this->link_redirect == 1 && $this->link_redirect_point == 'other') {
                $link = $this->getCmsUrl();             
             } else {
    		       $link .= $this->getCmsUrl() . '.html';
             }
		       }
       }
       return $link;
  }
  
  public function getCmsUrl()
  {
    $link = '';
    if ($this->link_redirect == 1) {
      if ($this->link_redirect_point == 'other') {
          $link .= empty($this->point_link) ? $this->link : $this->point_link;
      } else {
		      $link .= '/' . $this->link;
		      $firstChild = $this->getFirstChild();
		      if (!empty($firstChild))
		        $link .= $firstChild->getCmsUrl();
      }
    } else {
      $link .= '/' . $this->link;
    }
    
    return $link;
    
  }
  
  public function getFirstChild()
  {
      if (count($this->children) > 0) {
          foreach ($this->children(array('condition' => 'status = "active"', 'order' => 'position ASC', 'limit' => '1')) as $child) {
            return $child;
          }
      }
      return null;
  }

  public function getParents(&$parents)
  {
    
    if ($this->hasParent()) {
       if ($this->parent->hasParent())
          $this->parent->getParents($parents);
       $parents[] = $this->parent;
    } else {
      $parents[] = $this;
    }
  }
  
  public function hasParent()
  {
    return $this->parent_id != 0;
  }
  
  public function getSiteRoot()
  {
    $siteRoot = null;
    $parents = array();
    $this->getParents($parents);
    
    if (!$this->hasParent()) {
         return $this;   
    }
    
    
    foreach ($parents as $parent) {
        if ($parent->type == 'site') {
            $siteRoot = $parent;
            break;
        }
    }
    
    return $siteRoot;
  }
  
  public function isActive()
  {
    $cNav = self::getCurrentNavigation();
    if ($this->id == $cNav->id)
      return true;
    else {
      foreach ($this->children as $child) {
        if ($child->id == $cNav->id)
          return true;
        $child->isActive();
      }
      return false;
    }
  }
  
  public static function getMenuItems($parentId, $localeId, $showChildrenIfActive = true, $condition = null, $level = 1, $currentLevel = 1, $orderBy = null)
  {
      $items = array();
      if ($level) {
        
        if ($parentId == 0) {
        	if(isset($_SESSION['piisite'])){
            	$site = $_SESSION['piisite'];
            	$parentId = $site->id;
			}else{
				Yii::app()->controller->redirect('/');
			}
        }
          
          
        foreach (self::getTreeStructure($parentId, $localeId, $condition, $orderBy) as $node) {
            $navigation = $node['navigation'];
            if ($navigation->hasAccess())
              $items[$navigation->id] = array('id' => $navigation->id, 'label' =>$navigation->children ? '<span>' . $navigation->label .'</span><span class="fa fa-angle-right"></span></a>' :  '<span>' . $navigation->label . '</span>', 'url' =>$navigation->children ? "":$navigation->getUrl(), 'active' => $navigation->isActive(), 'itemOptions' => array('class' => $navigation->children ? 'withSub' : ''), 'linkOptions' => array('target' => $navigation->target, 'class'=>$navigation->children ? "dropdown-toggle" :"",'data-toggle'=>$navigation->children ? "dropdown" :"",'title' => $navigation->label));
            if ($navigation->showMenuChildren($level, $currentLevel, $showChildrenIfActive)) {
                $items[$navigation->id]['items'] = array();
                $items[$navigation->id]['items'] = self::getMenuItems($navigation->id, $localeId, $showChildrenIfActive, $condition, $level, $currentLevel + 1);
            }
        }
      }
      return $items;
  }
  
  public function showMenuChildren($level, $currentLevel, $showChildrenIfActive)
  {
    if (($level > $currentLevel) || ($showChildrenIfActive && $this->isActive())) return true;
  }
  
  public function hasAccess()
  {
      if ($this->is_protected == 1) {
          $roles = $this->protected_role;
          return in_array(Yii::app()->user->role, $roles) || Yii::app()->user->role == 'admin';
          
      }
      return true;
  }
  
  public static function getCurrentNavigation()
  {
	    $urlParts = App::getUrlParts();
		
        if (isset($_SESSION['piisite'])) {
            $sessionSite = $_SESSION['piisite'];
            $sessionSiteModel = Structure::model()->findByPk($sessionSite->id);
            if ($sessionSiteModel->attributes != $sessionSite->attributes)
                unset($_SESSION['piisite']);   
        }
        
        if (isset($_SESSION['piidomain'])) {
            if ($_SESSION['piidomain'] != Yii::app()->request->serverName)
                unset($_SESSION['piisite']);
            
        }

		if (!isset($_SESSION['piisite'])) {
		  $piisite = Structure::getSite();
          $_SESSION['piisite'] = $piisite;
          $_SESSION['piidomain'] = Yii::app()->request->serverName;
		} else {
		    $piisite = $_SESSION['piisite'];
		}
        
		$localeId = array_shift($urlParts);
		
        
		if (!empty($localeId)) {
			$locale = LocaleModel::model()->findByPk($localeId);
			if (!empty($locale) && $locale->status == 'active') {
		        Yii::app()->session['language'] = $localeId;
		        Yii::app()->language = Yii::app()->session['language'];
			}
		} 
      
	    $structureId = $piisite->id;
        if (count($urlParts) > 0) {
		    foreach ($urlParts as $part) {
		      if (!empty($part)) {
		        $node = $part;
		        if (strpos($part, '.html') !== false) {
		          
		          $nodePart = explode('.', $part);
		          $node = $nodePart[0];
                  
		        }
                    
	               $structureId = self::model()->find('link = :node AND status = "active" AND parent_id = :parent AND locale_id = :locale AND site_id = :site', array(':site' => $piisite->id, ':node' => $node, ':parent' => $structureId, ':locale' => Yii::app()->language))->id;
		      }
		    }
		    
            
		    if ($structureId) {
	        $cNav = self::model()->findByPk($structureId);
	      } else {
	        return null;
	      }  
		    
      } else {
        $defaultSite = self::getDefaultSite();
        $cNav = self::model()->find('is_home = 1 AND status = "active" AND locale_id = :locale AND site_id = :site', array(':site' => $defaultSite->id, ':locale' => Yii::app()->language));
      }
      
      
      if (!empty($cNav) && $cNav->hasAccess())
        return $cNav;
        
      return null;
  }

  public static function getSite()
  {
      $domain = Yii::app()->request->serverName;
      $sites = Structure::model()->findAll('type = "site" AND status = "active" ORDER BY position ASC');
      $currentSite = null;
      foreach ($sites as $site) {
          if ($site->matchDomain($domain)) {
              $currentSite = $site;
              break;
          }
      }
      
      if (!$currentSite)
        $currentSite = self::getDefaultSite();
      
      return $currentSite;
  }
  
  public function matchDomain($domain = null) {
      $domainParts = explode('.', $domain);
      $subDomain = count($domainParts) > 2 ? $domainParts[0] : null;
      $domainWithoutSub = $domain;
      
      if ($subDomain) {
          $domainWithoutSub = $domainParts[1] . '.' . $domainParts[2];
      }
      
      $domains = App::removeEmptyValue(explode("\n", $this->domain));
      foreach ($domains as $domainItem) {
          $domainItem = trim($domainItem);
          if ($domainItem == $domain)
            return true;
          elseif ($domainItem == "*")
            return true;
          elseif (strpos($domainItem, '*.') !== false) {
              $subDomainItem = str_replace('*.', '', $domainItem);
              if ($domainWithoutSub == $subDomainItem)
                return true;
          }
      }
  }

  public static function getDefaultSite()
  {
      return self::model()->find('is_default = 1 AND type = "site"');
  }
  
  public function getTopLevelNavigation()
  {
    if ($this->parent->type == 'site')
      return $this;
    else {
      return $this->parent->getTopLevelNavigation();
    }
  }
  
  public static function getTargetTypes()
  {
      return array(
        '_self' => Yii::t('app', 'In gleichem Fenster öffnen'),
        '_blank' => Yii::t('app', 'In neuem Fenster / Tab öffnen')
      );
  }
  
  public function cleanUrl(){
  	$url = App::toAscii($this->link);
  	$this->link = $url;
  }
  
}