<?php

/**
 * This is the model class for table "kontaktseite".
 *
 * The followings are the available columns in table 'kontaktseite':
 * @property integer $id
 * @property string $titel1
 * @property string $anfuehrungstext1
 * @property string $titel2
 * @property string $anfuehrungstext2
 * @property string $titel3
 * @property string $anfuehrungstext3
 * @property string $text
 * @property string $titel
 * @property integer $position
 * @property string $locale_id
 * @property string $unit_hash
 * @property string $telefon
 * @property string $fax
 * @property string $email
 */
class Kontaktseite extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'kontaktseite';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('text, titel, locale_id, unit_hash', 'required'),
            array('position', 'numerical', 'integerOnly' => true),
            array('titel1, titel2, titel3, titel, unit_hash', 'length', 'max' => 250),
            array('locale_id', 'length', 'max' => 45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, titel1, anfuehrungstext1, titel2,telefon,fax,email, titel3, anfuehrungstext3, text, titel, position, locale_id, unit_hash', 'safe'),
            array('id, titel1, anfuehrungstext1, titel2, anfuehrungstext2, titel3, anfuehrungstext3, text, titel, position, locale_id, unit_hash', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'titel1' => 'Titel1',
            'anfuehrungstext1' => 'Anfuehrungstext1',
            'titel2' => 'Titel2',
            'anfuehrungstext2' => 'Anfuehrungstext2',
            'titel3' => 'Titel3',
            'anfuehrungstext3' => 'Anfuehrungstext3',
            'text' => 'Text',
            'titel' => 'Titel',
            'position' => 'Position',
            'locale_id' => 'Locale',
            'unit_hash' => 'Unit Hash',
            'email'=>'E-Mail',
            'telefon'=>'Telefon',
            'fax'=>'Fax',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('titel1', $this->titel1, true);
        $criteria->compare('anfuehrungstext1', $this->anfuehrungstext1, true);
        $criteria->compare('titel2', $this->titel2, true);
        $criteria->compare('anfuehrungstext2', $this->anfuehrungstext2, true);
        $criteria->compare('titel3', $this->titel3, true);
        $criteria->compare('anfuehrungstext3', $this->anfuehrungstext3, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('titel', $this->titel, true);
        $criteria->compare('position', $this->position);
        $criteria->compare('locale_id', $this->locale_id, true);
        $criteria->compare('unit_hash', $this->unit_hash, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Kontaktseite the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function getAnzDaten(){
        $models = $this->model()->findAll();
        $anzMod = count($models);
        if($anzMod<1){
            return true;
        }else{
            return false;
        }
    }

}
