<?php

/**
 * This is the model class for table "buchung".
 *
 * The followings are the available columns in table 'buchung':
 * @property integer $id
 * @property string $anrede
 * @property string $nachname
 * @property string $vorname
 * @property string $geburtsdatum
 * @property string $strasse
 * @property integer $plz
 * @property string $ort
 * @property string $telefon
 * @property string $email
 * @property string $bemerkung
 * @property string $datum
 * @property string $buchungsdatum
 * @property string $zeit
 * @property integer $terminerinnerung
 * @property integer $bestatigung
 * @property string $dauer
 * @property string $status
 * @property integer $terminplan_idfs
 *
 * The followings are the available model relations:
 * @property Terminplan $terminplanIdfs
 * @property Buchungbehandlung[] $buchungbehandlungs
 */
class Buchung extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'buchung';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('anrede, nachname, vorname, strasse, plz, ort, telefon, email', 'required'),
            //array('anrede, nachname, vorname, geburtsdatum, strasse, plz, ort, telefon, email, datum, zeit, dauer, status, terminplan_idfs', 'required'),
            array('plz, terminerinnerung, bestatigung, terminplan_idfs', 'numerical', 'integerOnly' => true),
            array('nachname,vorname,ort','CRegularExpressionValidator', 'pattern'=>'/^[a-zA-z äöü]+$/','message'=>Yii::t('site', "{attribute} muss aus Buchstaben bestehen.")),
            array('anrede, buchungsdatum,nachname, vorname, ort, telefon, email, datum, zeit, dauer', 'length', 'max' => 250),
            array('geburtsdatum, status', 'length', 'max' => 50),
            array('datum', 'date', 'format' => array('dd-MM-yyyy')),
            array('email', 'email'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, anrede, nachname, vorname, geburtsdatum, strasse, plz, ort, telefon, email, bemerkung, datum, zeit, terminerinnerung, bestatigung, dauer, status, terminplan_idfs', 'safe', 'on' => 'search'),
            array('id, anrede, nachname, vorname, geburtsdatum, strasse, plz, ort, telefon, email, bemerkung, datum, zeit, terminerinnerung, bestatigung, dauer, status, terminplan_idfs', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'terminplanIdfs' => array(self::BELONGS_TO, 'Terminplan', 'terminplan_idfs'),
            'buchungbehandlungs' => array(self::HAS_MANY, 'Buchungbehandlung', 'buchung_idfs'),
            'behandlungsart' => array(self::HAS_MANY, 'Behandlungsart', array('behandlungsart_idfs' => 'id'), 'through' => 'buchungbehandlungs'),
        );
    }

    /* Kisanth Ketheeswaran
     * Das ist eine Funktion von der Superklasse CActiveRecord
     * Diese Funktion wird vor dem speichern aufgeführt, jedoch aber nach den Validierungen
     */

    public function beforeSave() {
        $this->datum = CDateTimeParser::parse($this->datum, 'dd-MM-yyyy');
        $this->buchungsdatum = CDateTimeParser::parse($this->buchungsdatum, 'dd-MM-yyyy');
        return parent::beforeSave();
    }

    /* Kisanth Ketheeswaran
     * Das ist eine Funktion von der Superklasse CActiveRecord
     * Diese Funktion wird nach dem speichern aufgeführt, jedoch aber nach den Validierungen
     * Sie wandelt den Timestamp in der normalen Datum Ansicht.
     */

    public function afterSave() {
        $this->datum = date("d-m-Y", $this->datum);
        $this->buchungsdatum = date("d-m-Y", $this->buchungsdatum);
        return parent::afterSave();
    }

    /* Kisanth Ketheeswaran
     * Das ist eine Funktion von der Superklasse CActiveRecord
     * Diese Funktion wird nach dem find-Methode aufgeführt, welche die Daten
     * aus der Datenbank holen
     */

    public function afterFind() {
        $this->datum = date("d-m-Y", $this->datum);
        $this->buchungsdatum = date("d-m-Y", $this->buchungsdatum);
        return parent::afterFind();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'anrede' => 'Anrede',
            'nachname' => 'Nachname',
            'vorname' => 'Vorname',
            'geburtsdatum' => 'Geburtsdatum',
            'strasse' => 'Strasse',
            'plz' => 'Plz',
            'ort' => 'Ort',
            'telefon' => 'Telefon',
            'email' => 'Email',
            'bemerkung' => 'Bemerkung',
            'datum' => 'Datum',
            'zeit' => 'Uhrzeit',
            'terminerinnerung' => 'Terminerinnerung',
            'bestatigung' => 'Bestätigungsmail',
            'dauer' => 'Dauer',
            'status' => 'Status',
            'terminplan_idfs' => 'Terminplan Idfs',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($nachname = "") {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('terminplanIdfs');
        $criteria->compare('id', $this->id);
        $criteria->compare('anrede', $this->anrede, true);
        $criteria->compare('nachname', $this->nachname, true);
        $criteria->compare('vorname', $this->vorname, true);
        $criteria->compare('geburtsdatum', $this->geburtsdatum, true);
        $criteria->compare('strasse', $this->strasse, true);
        $criteria->compare('plz', $this->plz);
        $criteria->compare('ort', $this->ort, true);
        $criteria->compare('telefon', $this->telefon, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('bemerkung', $this->bemerkung, true);
        $criteria->compare('datum', $this->datum, true);
        $criteria->compare('zeit', $this->zeit, true);
        $criteria->compare('terminerinnerung', $this->terminerinnerung);
        $criteria->compare('bestatigung', $this->bestatigung);
        $criteria->compare('dauer', $this->dauer, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('terminplan_idfs', $this->terminplan_idfs);
        $criteria->compare('buchungsdatum', $this->buchungsdatum);
        if (empty($nachname)) {
            $criteria->compare('terminplanIdfs.nachname', Yii::app()->request->getParam('terminplan_nachname'), true);
        } else {
            $criteria->compare('terminplanIdfs.id', $nachname, false);
        }

        $criteria->order = 'DATE_FORMAT(datum,"%m-%d-%Y"),DATE_FORMAT(buchungsdatum,"%m-%d-%Y")';
        return new CActiveDataProvider($this->with('terminplanIdfs'), array(
            'criteria' => $criteria
        ));
    }

    public function searchadmin($name) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('terminplanIdfs');
        $criteria->compare('id', $this->id);
        $criteria->compare('anrede', $this->anrede, true);
        $criteria->compare('nachname', $this->nachname, true);
        $criteria->compare('vorname', $this->vorname, true);
        $criteria->compare('geburtsdatum', $this->geburtsdatum, true);
        $criteria->compare('strasse', $this->strasse, true);
        $criteria->compare('plz', $this->plz);
        $criteria->compare('ort', $this->ort, true);
        $criteria->compare('telefon', $this->telefon, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('bemerkung', $this->bemerkung, true);
        $criteria->compare('datum', $this->datum, true);
        $criteria->compare('zeit', $this->zeit, true);
        $criteria->compare('terminerinnerung', $this->terminerinnerung);
        $criteria->compare('bestatigung', $this->bestatigung);
        $criteria->compare('dauer', $this->dauer, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('terminplan_idfs', $this->terminplan_idfs);
        $criteria->compare('buchungsdatum', $this->buchungsdatum);
        $criteria->order = 'DATE_FORMAT(datum,"%m-%d-%Y"),DATE_FORMAT(buchungsdatum,"%m-%d-%Y")';
        $criteria->compare('terminplanIdfs.nachname', $name, true);

        return new CActiveDataProvider($this->with('terminplanIdfs'), array(
            'criteria' => $criteria
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Buchung the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function listAnrede($item = false) {
        $list = array(
            'herr' => Yii::t('site', 'Herr'),
            'frau' => Yii::t('site', 'Frau')
        );
        if ($item)
            return $list[$item];
        return $list;
    }
    
    public static function buchungStatus($item = false) {
        $list = array(
            'offen' => 'Offen',
            'bestaetigt' => 'Termin bestätigt',
            'abgesagt'=>'Termin abgesagt',
        );
        if ($item)
            return $list[$item];
        return $list;
    }

    /* Kisanth Ketheeswaran
     * Generiert das Array mit der Dauer des Termines     
     */

    public static function getTerminDauer() {
        $zeitSlots = array();
        for ($i = 30; $i <= 510; $i = $i + 30) {
            $zeitSlots +=array(
                "" . strval($i) => "" . strval($i),
            );
        }
        return $zeitSlots;
    }

    public static function holNichtBuchbareTermine($datum, $terminplanid) {
        date_default_timezone_set('Europe/Madrid');
        $datum = empty($datum) ? date('d-m-Y') : $datum;
        $datumTimestamp = CDateTimeParser::parse($datum, 'dd-MM-yyyy');
        $datumSechsMonate = (int) strtotime('+6 month', $datumTimestamp);
        $summand = 60 * 60 * 24;
        $nichtBuchbareTermine = array();
        $j=0;
        for ($i = $datumTimestamp; $i <= $datumSechsMonate; $i = $i + $summand) {
            $lok = date('d-m-Y',$i);
            $zeiten = Buchung::holZeiten($lok, $terminplanid);
            if (empty($zeiten)) {
                $nichtBuchbareTermine[$j]= date('d-m-Y',$i);
                $j++;
            } elseif ($zeiten[0] == 'nichts') {
                $nichtBuchbareTermine[$j]= date('d-m-Y',$i);
                $j++;
            }
        }
        return $nichtBuchbareTermine;
    }

    /* Kisanth Ketheeswaran
     * Diese Funktion bekommt das Datum und den Terminplaner-Id 
     * vom TerminController, dadurch werden die Ferien, Freiertag
     * und Arbeitszeiten geholt, die wiederum werden dannach mit einzelnen Funktionen
     * überprüft. Nach der Überprüfung bekommen sie je nachdem, eine andere 
     * oder gleiche Arbeitszeite
     */
    public static function holZeiten($datum, $terminplanid) {
        date_default_timezone_set('Europe/Madrid');
        $datum = empty($datum) ? date('d-m-Y') : $datum;
        $datumTimestamp = CDateTimeParser::parse($datum, 'dd-MM-yyyy');
        $datumSechsMonate = (int) strtotime('+6 month', $datumTimestamp);
        $zeitSlots = array();
        $ferien = Ferien::model()->findAll('terminplan_idfs =:id AND bis_datum >= :datum AND bis_datum <= :datumSechsMonate', array(':datum' => $datumTimestamp, ':datumSechsMonate' => $datumSechsMonate, ':id' => $terminplanid));
        $freiertag = Freiertag::model()->find('terminplan_idfs =:id AND datum = :datum', array(':datum' => $datumTimestamp, ':id' => $terminplanid));
        $arbeitsZeit = Buchung::model()->arbeitsZeit($datumTimestamp, $terminplanid);
        if (!empty($arbeitsZeit)) {
            $zeitSlots = Buchung::model()->pruefFerien($datum, $ferien, $arbeitsZeit);
            if (empty($zeitSlots)) {
                if (!empty($freiertag)) {
                    $zeitSlots = Buchung::model()->istFreiertag($freiertag->von, $freiertag->bis, $arbeitsZeit);
                } else {
                    $zeitSlots = $arbeitsZeit;
                }
            }
        }
        return $zeitSlots;
    }

    /* Kisanth Ketheeswaran
     * Diese Funktion überprüft, ob das Datum in einem Ferienzeitraum ist.
     * Ausserdem überprüft es mit dem Datum, ob der Terminplaner von seinen Ferien gekommen ist
     * oder ob er schon gegangen ist. 
     */
    public function pruefFerien($datum, $ferien, $arbeitsZeit) {
        $datumTimestamp = CDateTimeParser::parse($datum, 'dd-MM-yyyy');
        $zeitSlots = array();
        foreach ($ferien as $fer) {
            $bis_datum = CDateTimeParser::parse($fer->bis_datum, 'dd-MM-yyyy');
            $von_datum = CDateTimeParser::parse($fer->von_datum, 'dd-MM-yyyy');
            if ($fer->bis_datum == $datum) {
                $zeitSlots = Buchung::model()->ankunftFerien($fer->bis_zeit, $arbeitsZeit);
            } elseif ($fer->von_datum == $datum) {
                $zeitSlots = Buchung::model()->abfahrtFerien($fer->von_zeit, $arbeitsZeit);
            } elseif ($von_datum < $datumTimestamp && $bis_datum > $datumTimestamp) {
                $zeitSlots[0] = "nichts";
            }
        }
        return $zeitSlots;
    }

    /* Kisanth Ketheeswaran
     * Hier überprüfe ich wie lange der Terminplaner frei hat.
     * Dadurch wird seine Arbeitszeit neu berrechnet
     */
    public function istFreiertag($von, $bis, $arbeitsZeit) {
        $zeiten = array();
        foreach ($arbeitsZeit as $schluessel => $wert) {
            if ($schluessel > $bis || $schluessel < $von) {
                $zeiten[$schluessel] = $wert;
            }
        }
        return $zeiten;
    }

    /* Kisanth Ketheeswaran
     * Hier überprüfe ich wie lange der Terminplaner Ferien hat, 
     * seine Ankunftszeit ist. Dadurch wird seine Arbeitszeit neu berrechnet 
     */
    public function ankunftFerien($zeit, $arbeitsZeit) {
        $zeiten = array();
        foreach ($arbeitsZeit as $schluessel => $wert) {
            if ($schluessel > $zeit) {
                $zeiten[$schluessel] = $wert;
            }
        }
        return $zeiten;
    }

    /* Kisanth Ketheeswaran
     * Hier überprüfe ich wie lange der Terminplaner Ferien hat, seine
     * Abfahrtszeit ist. Dadurch wird seine Arbeitszeit neu berrechnet 
     */
    public function abfahrtFerien($zeit, $arbeitsZeit) {
        $zeiten = array();
        foreach ($arbeitsZeit as $schluessel => $wert) {
            if ($schluessel < $zeit) {
                $zeiten[$schluessel] = $wert;
            }
        }
        return $zeiten;
    }

    /* Kisanth Ketheeswaran
     * Hier gebe ich das Datum und den Terminplaner-Id mit, dann holt
     * er die Arbeitszeit des jeweiligen Terminplaner. 
     */
    public function arbeitsZeit($datum, $terminplanid) {
        $zeitArray = Standardkalender::getZeitSlots();
        $arbeitsZeiten = array();
        $tag = date('l', $datum);
        $tag = Standardkalender::ubersetztTagEnglischZuDeutsch($tag);
        $terminplan = Terminplan::model()->with($tag)->findByPk($terminplanid);
        $tagModel = $terminplan->$tag;
        if ($tagModel->frei != 1) {
            foreach ($zeitArray as $schluessel => $wert) {
                if ($schluessel >= $tagModel->von && $schluessel <= $tagModel->bis) {
                    $arbeitsZeiten[$schluessel] = $wert;
                }
            }
            return Buchung::model()->kommendeTermineEinkalkulieren($datum, $terminplanid, $arbeitsZeiten);
        }
        return $arbeitsZeiten;
    }

    /* Kisanth Ketheeswaran
     * Diese Funtkion wird von der Funktion arbeitsZeit() aufgerufen.
     * Sie synchronisiert die Arbetiszeit mit den Terminen die an diesem Tag bevorstehen
     */
    public function kommendeTermineEinkalkulieren($datum, $terminplanid, $arbeitsZeiten) {
        $neueArbeitsZeiten = array();
        $multiArrayasd = array();
        $termine = Termin::model()->findAll('datum=:datum AND terminplan_idfs = :id order by zeit', array(':datum' => $datum, ':id' => $terminplanid));
        if (!empty($termine)) {
            $i = 0;
            foreach ($termine as $termin) {
                $startZeit = $termin->zeit;
                $endZeit = Buchung::model()->berechnenEndZeitTermin($termin->zeit, $termin->dauer);
                foreach ($arbeitsZeiten as $schluessel => $wert) {
                    if ($schluessel > $endZeit || $schluessel < $startZeit) {
                        $neueArbeitsZeiten[$schluessel] = $wert;
                        $multiArrayasd[$i][$schluessel] = $wert;
                    }
                }
                $i++;
            }
            $neueArbeitsZeiten = Buchung::model()->holGemeinsameZeit($multiArrayasd);
            return $neueArbeitsZeiten;
        } else {
            return $arbeitsZeiten;
        }
    }

    /* Kisanth Ketheeswaran
     * Diese Funtkion wird von der Funktion kommendeTermineEinkalkulieren() aufgerufen.
     * Sie berrechnet mit der Startzeit und der Dauer des Termins, die Zeit an dem 
     * der Termin endet.
     */
    public function berechnenEndZeitTermin($startZeit, $dauer) {
        $startZeit = (int) $startZeit;
        $dauer = (int) $dauer;
        if (($dauer % 60) == 0) {
            $dauer = ($dauer / 60) * 100;
        } else {
            $dauer = (($dauer / 60) * 100) - 20;
        }
        $endZeit = $startZeit + $dauer;
        $endZeit = strval($endZeit);
        return $endZeit;
    }

    /* Kisanth Ketheeswaran
     * Diese Funtkion wird von der Funktion kommendeTermineEinkalkulieren() aufgerufen.
     * Sie bekommt von der Funktion kommendeTermineEinkalkulieren(), 
     * ein multidimonsionales Array, wo die neu berrechneten Arbeitszeiten des
     * jeweiligem Termins, diese Arbeitszeiten sollten, verglichen und die die 
     * gleiche Zeit haben, sollten in einem neuen eindimensionales Array gespeichert werden.
     */
    public function holGemeinsameZeit($array) {
        $results = array();
        $results = $array[0];
        foreach ($array as $subarray) {
            $results = array_intersect_key($results, $subarray);
        }

        return $results;
    }

}
