<?php

/**
 * This is the model class for table "gallery".
 *
 * The followings are the available columns in table 'gallery':
 * @property string $id
 * @property string $unit_hash
 * @property string $locale_id
 * @property string $status
 * @property string $title
 */
class Gallery extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Gallery the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gallery';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('unit_hash, locale_id, status, title', 'required'),
			array('unit_hash, title', 'length', 'max'=>250),
			array('locale_id, status', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, unit_hash, locale_id, status, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'unit_hash' => 'Unit Hash',
			'locale_id' => 'Locale',
			'status' => Yii::t('site', 'Status'),
			'title' => Yii::t('site', 'Titel'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('unit_hash',$this->unit_hash,true);
		$criteria->compare('locale_id',$this->locale_id,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('title',$this->title,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	
	    public static function listDataStatus($item = false)
    {
        $list = array(
            'active' => Yii::t('app', 'Aktiv'),
            'inactive' => Yii::t('app', 'Inaktiv')
        );
        
        if ($item)
            return $list[$item];
        
        return $list;
    }
    
    public function deleteModel()
    {
        $models = self::model()->findAll('unit_hash = :hash', array(':hash' => $this->unit_hash));
        foreach ($models as $model) {
            $model->delete();
        }
    }

    public function getModel($localeId, $fallback = false)
    {
        $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $this->unit_hash, ':locale' => $localeId));
        
        if (empty($model) && $fallback) {
            $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $this->unit_hash, ':locale' => LocaleModel::getDefault()->id));
        }

        return $model;
    }
    
    public function getElements() {
	    $pictures = GalleryElement::model()->findAll('gallery_id = :id ORDER BY position ASC', array(':id' => $this->id));
	    
	    return $pictures;
    }
    
    public static function getGalleries(){
        $dbGalleries = Gallery::model()->findAll();
        $galleries[0] = '';
        foreach ( $dbGalleries as $gallery) {
            $galleries[$gallery->id] = $gallery->title;
        }
        return $galleries;
    }
}