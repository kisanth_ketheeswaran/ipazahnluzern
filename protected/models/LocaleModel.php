<?php

/**
 * This is the model class for table "locale".
 *
 * The followings are the available columns in table 'locale':
 * @property string $id
 * @property string $label
 * @property integer $is_default
 * @property string $status
 */
class LocaleModel extends CActiveRecord
{
  /**
   * Returns the static model of the specified AR class.
   * @return LocaleBase the static model class
   */
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return 'locale';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('id, label', 'required'),
      array('is_default', 'numerical', 'integerOnly'=>true),
      array('id', 'length', 'max'=>10),
      array('label', 'length', 'max'=>250),
      array('status', 'length', 'max'=>45),
      // The following rule is used by search().
      // Please remove those attributes that should not be searched.
      array('id, label, is_default, status', 'safe', 'on'=>'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'id' => 'ID',
      'label' => 'Label',
      'is_default' => 'Is Default',
      'status' => 'Status',
    );
  }

  /**
   * Retrieves a list of models based on the current search/filter conditions.
   * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
   */
  public function search()
  {
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.

    $criteria=new CDbCriteria;

    $criteria->compare('id',$this->id,true);
    $criteria->compare('label',$this->label,true);
    $criteria->compare('is_default',$this->is_default);
    $criteria->compare('status',$this->status,true);

    return new CActiveDataProvider(get_class($this), array(
      'criteria'=>$criteria,
    ));
  }
  
  public static function getDefault()
  {
    return self::model()->find('is_default = 1');
  }
  
  public static function getListData()
  {
    return CHtml::listData(
          self::model()->findAll(array('order' => 'position ASC')),
          'id',
          'label'
    );
  }
  
  public function cssStatus($locale = null)
  {
    if (empty($locale)) {
      $locale = self::getDefault()->id; 
    }
    
    return $locale == $this->id ? 'active' : '';
  }
}