<?php

/**
 * This is the model class for table "behandlungsart".
 *
 * The followings are the available columns in table 'behandlungsart':
 * @property integer $id
 * @property string $behandlung
 * @property string $bemerkung
 * @property integer $terminplan_idfs
 * @property integer $position
 *
 * The followings are the available model relations:
 * @property Behandlung[] $behandlungs
 * @property Terminplan $terminplanIdfs
 * @property Buchungbehandlung[] $buchungbehandlungs
 */
class Behandlungsart extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'behandlungsart';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('behandlung', 'required'),
            array('terminplan_idfs, position', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, behandlung, bemerkung, terminplan_idfs, position', 'safe'),
            array('id, behandlung, bemerkung, terminplan_idfs, position', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'behandlungs' => array(self::HAS_MANY, 'Behandlung', 'behandlungsart_idfs'),
            'terminplanIdfs' => array(self::BELONGS_TO, 'Terminplan', 'terminplan_idfs'),
            'buchungbehandlungs' => array(self::HAS_MANY, 'Buchungbehandlung', 'behandlungsart_idfs'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'behandlung' => 'Behandlung',
            'bemerkung' => 'Bemerkung',
            'terminplan_idfs' => 'Terminplan Idfs',
            'position' => 'Position',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('behandlung', $this->behandlung, true);
        $criteria->compare('bemerkung', $this->bemerkung);
        $criteria->compare('terminplan_idfs', $this->terminplan_idfs);
        $criteria->compare('position', $this->position);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Behandlungsart the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
