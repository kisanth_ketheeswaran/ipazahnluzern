<?php

/**
 * This is the model class for table "glossary".
 *
 * The followings are the available columns in table 'glossary':
 * @property integer $id
 * @property string $word
 * @property string $company
 * @property string $address
 * @property string $place
 * @property string $postcode
 * @property string $telefon
 * @property string $fax
 * @property string $mail
 * @property string $status
 * @property string $locale_id
 * @property string $unit_hash
 *  * @property string $description
 */
class Contact extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return Glossary the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'contact';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('description, locale_id, unit_hash', 'required'),
            array('description', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, description,company, address, place, postcode, telefon, fax, mail, status, locale_id, unit_hash', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'description' => Yii::t('app', 'Kontaktfeld'),
            'status' => Yii::t('app', 'Status'),
            'locale_id' => Yii::t('app', 'Sprache'),
            'unit_hash' => 'Unit Hash',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($lang = null)
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $lang = empty($lang) ? LocaleModel::getDefault()->id : $lang;
        $criteria=new CDbCriteria;

        $criteria->condition = 'locale_id = "' . $lang . '"';

        $criteria->compare('id',$this->id);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('status',$this->status,true);
        $criteria->compare('locale_id',$this->locale_id,true);
        $criteria->compare('unit_hash',$this->unit_hash,true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria
        ));
    }
    
    public static function listDataStatus($item = false)
    {
        $list = array(
            'active' => Yii::t('site', 'Aktiv'),
            'inactive' => Yii::t('site', 'Inaktiv')
        );
        
        if ($item)
            return $list[$item];
        
        return $list;
    }
    
    public function deleteModel()
    {
        $models = self::model()->findAll('unit_hash = :hash', array(':hash' => $this->unit_hash));
        foreach ($models as $model) {
            $model->delete();
        }
    }
    
    public function getModel($localeId, $fallback = false)
    {
        $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $this->unit_hash, ':locale' => $localeId));
        
        if (empty($model) && $fallback) {
            $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $this->unit_hash, ':locale' => LocaleModel::getDefault()->id));
        }

        return $model;
    }

}