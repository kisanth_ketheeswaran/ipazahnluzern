<?php

/**
 * This is the model class for table "faq_group".
 *
 * The followings are the available columns in table 'faq_group':
 * @property integer $id
 * @property string $name
 * @property string $locale_id
 * @property string $unit_hash
 */
class FaqGroup extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'faq_group';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, locale_id, unit_hash', 'required'),
            array('name, unit_hash', 'length', 'max' => 250),
            array('locale_id', 'length', 'max' => 45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, locale_id, unit_hash', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'faqs'=>array(self::HAS_ONE, 'Faq', 'faq_group_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'locale_id' => 'Locale',
            'unit_hash' => 'Unit Hash',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('locale_id', $this->locale_id, true);
        $criteria->compare('unit_hash', $this->unit_hash, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FaqGroup the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function listData() {
        $models = self::model()->findAll(array(
            'condition' => 'locale_id = :locale',
            'order' => 'id, name ASC',
            'params' => array(
                ':locale' => LocaleModel::getDefault()->id
            )
        ));

        $container = array();

        foreach ($models as $model) {
            $localeModel = $model->getModel($localeId);
            if ($localeModel)
                $container[] = $localeModel;
            else
                $container[] = $model;
        }

        return CHtml::listData($container, 'unit_hash', 'name');
    }

    public function getModel($localeId, $fallback = false) {
        $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $this->unit_hash, ':locale' => $localeId));

        if (empty($model) && $fallback) {
            $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $this->unit_hash, ':locale' => LocaleModel::getDefault()->id));
        }

        return $model;
    }

    public static function getFromHash($hash, $localeId, $fallback = false) {
        $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $hash, ':locale' => $localeId));

        if (empty($model) && $fallback) {
            $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $hash, ':locale' => LocaleModel::getDefault()->id));
        }

        return $model;
    }

    public function deleteModel() {
        if ($this->locale_id != LocaleModel::getDefault()->id)
            $this->delete();
        else {
            $models = self::model()->findAll('unit_hash = :hash', array(':hash' => $this->unit_hash));
            foreach ($models as $model) {
                $model->delete();
            }
        }
    }
    
    public function getFaqs($model,$locale='de'){
        $allFaq = Faq::model()->findAll('faq_group_id = :hash AND locale_id=:locale', array(':hash'=>$model->unit_hash,':locale'=>$locale));
        if(!empty($allFaq)){
            return $allFaq;
        }else{
            return false;
        }
        
    }

}
