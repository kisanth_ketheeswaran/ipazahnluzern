<?php

/**
 * This is the model class for table "cms_widget_contact_form".
 *
 * The followings are the available columns in table 'cms_widget_contact_form':
 * @property integer $id
 * @property integer $cms_widget_id
 * @property integer $target_id
 * @property integer $value
 */
class CmsWidgetContactForm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return CmsWidgetContactForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cms_widget_contact_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cms_widget_id, target_id', 'required'),
			array('cms_widget_id, target_id, value, position', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cms_widget_id, target_id, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cms_widget_id' => 'Cms Widget',
			'target_id' => 'Target',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cms_widget_id',$this->cms_widget_id);
		$criteria->compare('target_id',$this->target_id);
		$criteria->compare('value',$this->value);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	
	public function getObject($target)
	{
		return self::model()->find('target_id = :target', array(':target' => $target->id));
	}
	
  public function saveWidget($model, $postRequest)
  {
    $widgetName = __CLASS__;
    $cmsWidgetId = $model->getWidget($widgetName)->id;
    $widget = self::model()->find('cms_widget_id = :widget_id AND target_id = :target', array(':target' => $model->id, ':widget_id' => $cmsWidgetId));
    if (empty($widget)) {
        $widget = new $widgetName();
    } 
    $widget->cms_widget_id = $cmsWidgetId;
    $widget->target_id = $model->id;
	if(!isset($postRequest['value']))
		$postRequest['value'] = 0;
    $widget->value = $postRequest['value'];
    $widget->position = $postRequest['position'];
    $widget->save();
  }
}