<?php

/**
 * This is the model class for table "gallery_element".
 *
 * The followings are the available columns in table 'gallery_element':
 * @property string $id
 * @property string $unit_hash
 * @property string $locale_id
 * @property string $description
 * @property integer $gallery_id
 * @property integer $position
 * @property string $image
 */
class GalleryElement extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return GalleryElement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gallery_element';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('unit_hash, locale_id, gallery_id, position, image', 'required'),
			array('gallery_id, position', 'numerical', 'integerOnly'=>true),
			array('unit_hash, image', 'length', 'max'=>250),
			array('locale_id', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, unit_hash, locale_id, description, gallery_id, position, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'unit_hash' => 'Unit Hash',
			'locale_id' => 'Locale',
			'description' => 'Description',
			'gallery_id' => 'Gallery',
			'position' => 'Position',
			'image' => 'Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('unit_hash',$this->unit_hash,true);
		$criteria->compare('locale_id',$this->locale_id,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('gallery_id',$this->gallery_id);
		$criteria->compare('position',$this->position);
		$criteria->compare('image',$this->image,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	
	public static function listDataStatus($item = false)
    {
        $list = array(
            'active' => Yii::t('app', 'Aktiv'),
            'inactive' => Yii::t('app', 'Inaktiv')
        );
        
        if ($item)
            return $list[$item];
        
        return $list;
    }
    
    public function deleteModel()
    {
        $models = self::model()->findAll('unit_hash = :hash', array(':hash' => $this->unit_hash));
        foreach ($models as $model) {
            App::deleteFile($model->file, 'job');
            $model->delete();
        }
    }

    public function getModel($localeId, $fallback = false)
    {
        $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $this->unit_hash, ':locale' => $localeId));
        
        if (empty($model) && $fallback) {
            $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $this->unit_hash, ':locale' => LocaleModel::getDefault()->id));
        }

        return $model;
    }
    
    public function getFile()
    {
        if (empty($this->file)) {
            $sourceModel = $this->getModel(LocaleModel::getDefault()->id, true);
            return $sourceModel->file;
        }
        
        return $this->file;
    }
}