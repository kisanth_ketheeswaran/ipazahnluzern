<?php

/**
 * This is the model class for table "terminplan".
 *
 * The followings are the available columns in table 'terminplan':
 * @property integer $id
 * @property string $anrede
 * @property string $vorname
 * @property string $nachname
 * @property integer $position
 * @property string $bemerkung
 * @property string $email
 * @property string $locale_id
 *  @property string $unit_hash
 * 
 * The followings are the available model relations:
 * @property Behandlungsart[] $behandlungsarts
 * @property Buchung[] $buchungs
 * @property Ferien[] $feriens
 * @property Freiertag[] $freiertags
 * @property Standardkalender[] $standardkalenders
 * @property Termin[] $termins
 */
class Terminplan extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'terminplan';
    }
    public $full_name = '';
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('anrede, vorname, nachname,email,locale_id,unit_hash', 'required'),
            array('position', 'numerical', 'integerOnly' => true),
            array('anrede, vorname, nachname,email', 'length', 'max' => 250),
            array('email', 'email'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('anrede, vorname, nachname, position, bemerkung', 'safe'),
            array('id, anrede, vorname, nachname, position, bemerkung', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'behandlungsarts' => array(self::HAS_MANY, 'Behandlungsart', 'terminplan_idfs'),
            'buchungs' => array(self::HAS_MANY, 'Buchung', 'terminplan_idfs'),
            'feriens' => array(self::HAS_MANY, 'Ferien', 'terminplan_idfs'),
            'freiertags' => array(self::HAS_MANY, 'Freiertag', 'terminplan_idfs'),
            'standardkalenders' => array(self::HAS_ONE, 'Standardkalender', 'terminplan_idfs'),
            'termins' => array(self::HAS_MANY, 'Termin', 'terminplan_idfs'),
            'montag' => array(self::HAS_ONE, 'Montag', array('mo_idfs' => 'id'), 'through' => 'standardkalenders'),
            'dienstag' => array(self::HAS_ONE, 'Dienstag', array('di_idfs' => 'id'), 'through' => 'standardkalenders'),
            'mittwoch' => array(self::HAS_ONE, 'Mittwoch', array('mi_idfs' => 'id'), 'through' => 'standardkalenders'),
            'donnerstag' => array(self::HAS_ONE, 'Donnerstag', array('do_idfs' => 'id'), 'through' => 'standardkalenders'),
            'freitag' => array(self::HAS_ONE, 'Freitag', array('fr_idfs' => 'id'), 'through' => 'standardkalenders'),
            'samstag' => array(self::HAS_ONE, 'Samstag', array('sa_idfs' => 'id'), 'through' => 'standardkalenders'),
            'sonntag' => array(self::HAS_ONE, 'Sonntag', array('so_idfs' => 'id'), 'through' => 'standardkalenders'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'anrede' => 'Anrede',
            'vorname' => 'Vorname',
            'nachname' => 'Nachname',
            'position' => 'Position',
            'bemerkung' => 'Bemerkung',
            'email' => 'E-Mail',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('anrede', $this->anrede, true);
        $criteria->compare('vorname', $this->vorname, true);
        $criteria->compare('nachname', $this->nachname, true);
        $criteria->compare('position', $this->position);
        $criteria->compare('bemerkung', $this->bemerkung, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Terminplan the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function afterFind() {
        $this->full_name = $this->anrede . ' ' . $this->vorname . ' ' . $this->nachname;
    }

}
