<?php


class PageOption extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'page_option';
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_id', 'required'),
			array('emotion_pic', 'length', 'max'=>250),
			array('emotion_pic', 'safe')
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			//array('id, emotion_pic, page_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		  'page' => array(self::BELONGS_TO, 'Page', 'page_id')
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'emotion_pic' => Yii::t('app', 'Hintergrundbild'),
		);
	}

  public static function fields()
  {
    return array(
      'emotion_pic' => 'fileField',
      //'navigation_pic' => 'fileField',
    );
  }
	
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emotion_pic',$this->emotion_pic,true);
        $criteria->compare('page_id',$this->page_id,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	
  public function beforeDelete()
  {
    foreach (self::fields() as $key => $field) {
      if ($field == 'fileField') {
        @unlink(Yii::app()->basePath . '/../images/option/' . $this->$key);      
        @unlink(Yii::app()->basePath . '/../images/option/thumb/70_' . $this->$key);      
      }
    }
    return parent::beforeDelete();
  }
}