<?php

/**
 * This is the model class for table "team".
 *
 * The followings are the available columns in table 'team':
 * @property integer $id
 * @property string $vorname
 * @property string $nachname
 * @property string $funktion
 * @property string $text
 * @property string $bild
 * @property string $status
 * @property integer $position
 * @property string $locale_id
 * @property string $unit_hash
 */
class Team extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'team';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vorname, nachname, funktion, locale_id, unit_hash', 'required'),
            array('position', 'numerical', 'integerOnly' => true),
            array('vorname, nachname, funktion, bild, unit_hash', 'length', 'max' => 250),
            array('status, locale_id', 'length', 'max' => 45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, vorname, nachname, funktion, text, bild, status, position, locale_id, unit_hash', 'safe'),
            array('id, vorname, nachname, funktion, text, bild, status, position, locale_id, unit_hash', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'vorname' => 'Vorname',
            'nachname' => 'Nachname',
            'funktion' => 'Funktion',
            'text' => 'Text',
            'bild' => 'Bild',
            'status' => 'Status',
            'position' => 'Position',
            'locale_id' => 'Locale',
            'unit_hash' => 'Unit Hash',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('vorname', $this->vorname, true);
        $criteria->compare('nachname', $this->nachname, true);
        $criteria->compare('funktion', $this->funktion, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('bild', $this->bild, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('position', $this->position);
        $criteria->compare('locale_id', $this->locale_id, true);
        $criteria->compare('unit_hash', $this->unit_hash, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Team the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function listDataStatus($item = false) {
        $list = array(
            'active' => Yii::t('site', 'Aktiv'),
            'inactive' => Yii::t('site', 'Inaktiv')
        );

        if ($item)
            return $list[$item];

        return $list;
    }

    public function checkPosition($model, $locale = "de") {
        $position = $model->position;
        if (empty($position)) {
            $lastModel = $this->model()->find('locale_id = :locale order by position desc', array(':locale' => $locale));
            if (empty($lastModel)) {
                $position = 1;
                return $position;
            } else {
                $position = $lastModel->position + 1;
                return $position;
            }
        } else {
            $allModels = $this->model()->findAll('locale_id = :locale ', array(':locale' => $locale));
            if (empty($allModels)) {
                return $position;
            } else {
                foreach ($allModels as $slid) {
                    if ($slid->position == $position) {
                        foreach ($allModels as $mod) {
                            if ($mod->position >= $position) {
                                $mod->position = $mod->position + 1;
                                $mod->save(false);
                            }
                        }
                    }
                }
                return $position;
            }
        }
    }

}
