<?php

/**
 * This is the model class for table "freiertag".
 *
 * The followings are the available columns in table 'freiertag':
 * @property integer $id
 * @property string $datum
 * @property string $von
 * @property string $bis
 * @property integer $terminplan_idfs
 *
 * The followings are the available model relations:
 * @property Terminplan $terminplanIdfs
 */
class Freiertag extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'freiertag';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('datum, von, bis', 'required'),
            array('terminplan_idfs', 'numerical', 'integerOnly' => true),
            array('datum, von, bis', 'length', 'max' => 250),
            array('von', 'compare', 'compareAttribute' => 'bis', 'operator' => '<'),
            array('datum', 'date', 'format' => array('dd-MM-yyyy')),
            array('datum', 'prueftJetztigesDatum'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, datum, von, bis, terminplan_idfs', 'safe', 'on' => 'search'),
        );
    }

    /* Kisanth Ketheeswaran 
     * Prüft ob datum nicht in der Vergangenheit ist, sondern in der Gegenwart bzw. Zukunft.     */
    public function prueftJetztigesDatum($attribute, $params) {
        $format = (!empty($params['format'])) ? $params['format'] : 'dd-MM-yyyy';
        $datum = CDateTimeParser::parse($this->$attribute, $format);
        $jetztigesDatum = CDateTimeParser::parse(date("d-m-Y"), $format);
        if (version_compare($datum, $jetztigesDatum, '>=')) {
            return;
        } else {
            $this->addError($attribute, "Datum darf nicht in der Vergangenheit sein.");
        }
    }
    
    /*Kisanth Ketheeswaran
     * Das ist eine Funktion von der Oberklasse CActiveRecord
     * Diese Funktion wird vor dem speichern aufgeführt, jedoch aber nach den Validierungen
     */
    public function beforeSave() {
        $this->datum = CDateTimeParser::parse($this->datum, 'dd-MM-yyyy');
        return parent::beforeSave();
    }
    
    /*Kisanth Ketheeswaran
     * Das ist eine Funktion von der Superklasse CActiveRecord
     * Diese Funktion wird nach dem speichern aufgeführt, jedoch aber nach den Validierungen
     * Sie wandelt den Timestamp in der normalen Datum Ansicht.
     */
    public function afterSave() {
        $this->datum = date("d-m-Y", $this->datum);
        return parent::afterSave();
    }

    /*Kisanth Ketheeswaran
     * Das ist eine Funktion von der Superklasse CActiveRecord
     * Diese Funktion wird nach dem find-Methode aufgeführt, welche die Daten
     * aus der Datenbank holen
     */
    public function afterFind() {
        $this->datum = date("d-m-Y", $this->datum);
        return parent::afterFind();
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'terminplanIdfs' => array(self::BELONGS_TO, 'Terminplan', 'terminplan_idfs'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'datum' => 'Datum',
            'von' => 'Von',
            'bis' => 'Bis',
            'terminplan_idfs' => 'Terminplan Idfs',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('datum', $this->datum, true);
        $criteria->compare('von', $this->von, true);
        $criteria->compare('bis', $this->bis, true);
        $criteria->compare('terminplan_idfs', $this->terminplan_idfs);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Freiertag the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
