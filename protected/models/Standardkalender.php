<?php

/**
 * This is the model class for table "standardkalender".
 *
 * The followings are the available columns in table 'standardkalender':
 * @property integer $id
 * @property integer $mo_idfs
 * @property integer $di_idfs
 * @property integer $mi_idfs
 * @property integer $do_idfs
 * @property integer $fr_idfs
 * @property integer $sa_idfs
 * @property integer $so_idfs
 * @property integer $terminplan_idfs
 *
 * The followings are the available model relations:
 * @property Terminplan $terminplanIdfs
 * @property Mittwoch $miIdfs
 * @property Montag $moIdfs
 * @property Samstag $saIdfs
 * @property Sonntag $soIdfs
 * @property Dienstag $diIdfs
 * @property Donnerstag $doIdfs
 * @property Freitag $frIdfs
 */
class Standardkalender extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'standardkalender';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('', 'required'),
            array('mo_idfs, di_idfs, mi_idfs, do_idfs, fr_idfs, sa_idfs, so_idfs, terminplan_idfs', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, mo_idfs, di_idfs, mi__idfs, do_idfs, fr_idfs, sa_idfs, so_idfs, terminplan_idfs', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'terminplanIdfs' => array(self::BELONGS_TO, 'Terminplan', 'terminplan_idfs'),
            'miIdfs' => array(self::BELONGS_TO, 'Mittwoch', 'mi_idfs'),
            'moIdfs' => array(self::BELONGS_TO, 'Montag', 'mo_idfs'),
            'saIdfs' => array(self::BELONGS_TO, 'Samstag', 'sa_idfs'),
            'soIdfs' => array(self::BELONGS_TO, 'Sonntag', 'so_idfs'),
            'diIdfs' => array(self::BELONGS_TO, 'Dienstag', 'di_idfs'),
            'doIdfs' => array(self::BELONGS_TO, 'Donnerstag', 'do_idfs'),
            'frIdfs' => array(self::BELONGS_TO, 'Freitag', 'fr_idfs'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'mo_idfs' => 'Mo Idfs',
            'di_idfs' => 'Di Idfs',
            'mi_idfs' => 'Mi Idfs',
            'do_idfs' => 'Do Idfs',
            'fr_idfs' => 'Fr Idfs',
            'sa_idfs' => 'Sa Idfs',
            'so_idfs' => 'So Idfs',
            'terminplan_idfs' => 'Terminplan Idfs',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('mo_idfs', $this->mo_idfs);
        $criteria->compare('di_idfs', $this->di_idfs);
        $criteria->compare('mi_idfs', $this->mi__idfs);
        $criteria->compare('do_idfs', $this->do_idfs);
        $criteria->compare('fr_idfs', $this->fr_idfs);
        $criteria->compare('sa_idfs', $this->sa_idfs);
        $criteria->compare('so_idfs', $this->so_idfs);
        $criteria->compare('terminplan_idfs', $this->terminplan_idfs);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Standardkalender the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /* Gibt die jeweiligen Zeit Slots heraus */

    public static function getZeitSlots() {
        $zeitSlots = array();
        $zeitSlots = array(
            "" => "",
            "0700" => "07:00",
            "0730" => "07:30",
            "0800" => "08:00",
            "0830" => "08:30",
            "0900" => "09:00",
            "0930" => "09:30",
            "1000" => "10:00",
            "1030" => "10:30",
            "1100" => "11:00",
            "1130" => "11:30",
            "1200" => "12:00",
            "1230" => "12:30",
            "1300" => "13:00",
            "1330" => "13:30",
            "1400" => "14:00",
            "1430" => "14:30",
            "1500" => "15:00",
            "1530" => "15:30",
            "1600" => "16:00",
            "1630" => "16:30",
            "1700" => "17:00",
            "1730" => "17:30",
            "1800" => "18:00",
            "1830" => "18:30",
            "1900" => "19:00",
            "1930" => "19:30",
            "2000" => "20:00",
        );
        return $zeitSlots;
    }

    public static function getArrayWert($array, $id) {
        foreach ($array as $schluessel => $wert) {
            if ($schluessel == $id) {
                return $wert;
            }
        }
    }

    public static function ubersetztTagEnglischZuDeutsch($tag) {
        switch ($tag) {
            case "Monday":
                return "montag";
                break;
            case "Tuesday":
                return "dienstag";
                break;
            case "Wednesday":
                return "mittwoch";
                break;
            case "Thursday":
                return "donnerstag";
                break;
            case "Friday":
                return "freitag";
                break;
            case "Saturday":
                return "samstag";
                break;
            case "Sunday":
                return "sonntag";
                break;
        }
    }

    public static function pruefeDBEintrag($terminplan) {
        $model = Standardkalender::model()->find('terminplan_idfs=:id', array(':id' => $terminplan->id));
        if (!empty($model)) {
            return $model;
        } else {
            $model = new Standardkalender;
            return $model;
        }
    }

}
