<?php

/**
 * This is the model class for table "job".
 *
 * The followings are the available columns in table 'job':
 * @property integer $id
 * @property string $label
 * @property string $file
 * @property string $description_text
 * @property string $date
 * @property integer $sort_order
 * @property string $status
 * @property string $locale_id
 * @property string $unit_hash
 */
class Job extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Job the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'job';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('label, date, sort_order, status, locale_id, unit_hash', 'required'),
			array('sort_order', 'numerical', 'integerOnly'=>true),
			array('label, file, unit_hash', 'length', 'max'=>250),
			array('status, locale_id', 'length', 'max'=>45),
			array('description_text', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, label, file, description_text, date, sort_order, status, locale_id, unit_hash', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'label' => Yii::t('site', 'Job-Bezeichnung'),
			'file' => Yii::t('site', 'PDF-Datei'),
			'description_text' => Yii::t('site', 'Beschreibung'),
			'date' => Yii::t('site', 'Datum'),
			'sort_order' => Yii::t('site', 'Reihenfolge'),
			'status' => Yii::t('site', 'Status'),
			'locale_id' => Yii::t('site', 'Sprache'),
			'unit_hash' => 'Unit Hash',
		);
	}

    public function beforeSave()
    {
        $this->date = date('Y-m-d', strtotime($this->date));
        return parent::beforeSave();
    }

    public function afterSave()
    {
        $this->date = date('d.m.Y', strtotime($this->date));
    }
    
    public function afterFind()
    {
        $this->date = date('d.m.Y', strtotime($this->date));
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
    public function search($lang = null)
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $lang = empty($lang) ? LocaleModel::getDefault()->id : $lang;
        $criteria=new CDbCriteria;

        $criteria->condition = 'locale_id = "' . $lang . '"';

		$criteria->compare('id',$this->id);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('description_text',$this->description_text,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('locale_id',$this->locale_id,true);
		$criteria->compare('unit_hash',$this->unit_hash,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'sort' => array(
                'defaultOrder' => 'sort_order ASC'
            )
		));
	}
    
    public static function listDataStatus($item = false)
    {
        $list = array(
            'active' => Yii::t('app', 'Aktiv'),
            'inactive' => Yii::t('app', 'Inaktiv')
        );
        
        if ($item)
            return $list[$item];
        
        return $list;
    }
    
    public function deleteModel()
    {
        $models = self::model()->findAll('unit_hash = :hash', array(':hash' => $this->unit_hash));
        foreach ($models as $model) {
            App::deleteFile($model->file, 'job');
            $model->delete();
        }
    }

    public function getModel($localeId, $fallback = false)
    {
        $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $this->unit_hash, ':locale' => $localeId));
        
        if (empty($model) && $fallback) {
            $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $this->unit_hash, ':locale' => LocaleModel::getDefault()->id));
        }

        return $model;
    }
    
    public function getFile()
    {
        if (empty($this->file)) {
            $sourceModel = $this->getModel(LocaleModel::getDefault()->id, true);
            return $sourceModel->file;
        }
        
        return $this->file;
    }
}