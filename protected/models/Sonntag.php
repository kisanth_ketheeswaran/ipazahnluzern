<?php

/**
 * This is the model class for table "sonntag".
 *
 * The followings are the available columns in table 'sonntag':
 * @property integer $id
 * @property string $von
 * @property string $bis
 * @property integer $frei
 *
 * The followings are the available model relations:
 * @property Standardkalender[] $standardkalenders
 */
class Sonntag extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sonntag';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('von, bis', 'required'),
            array('frei', 'numerical', 'integerOnly' => true),
            array('von, bis', 'length', 'max' => 250),
           /* Kisanth Ketheeswaran
             * Prueft ob von nicht höher ist als bis.
             * Z.B von=>15:00 und bis=> 13:00, das muss dann eine Fehlermeldung geben.
             */
            array('von', 'compare', 'compareAttribute' => 'bis', 'operator' => '<'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, von, bis, frei', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'standardkalenders' => array(self::HAS_ONE, 'Standardkalender', 'so_idfs'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'von' => 'Von',
            'bis' => 'Bis',
            'frei' => 'Frei',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('von', $this->von, true);
        $criteria->compare('bis', $this->bis, true);
        $criteria->compare('frei', $this->frei);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Sonntag the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
