<?php

/**
 * This is the model class for table "ferien".
 *
 * The followings are the available columns in table 'ferien':
 * @property integer $id
 * @property string $von_datum
 * @property string $bis_datum
 * @property string $von_zeit
 * @property string $bis_zeit
 * @property integer $terminplan_idfs
 *
 * The followings are the available model relations:
 * @property Terminplan $terminplanIdfs
 */
class Ferien extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'ferien';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('von_datum, bis_datum, von_zeit, bis_zeit', 'required'),
            array('terminplan_idfs', 'numerical', 'integerOnly' => true),
            array('von_datum, bis_datum, von_zeit, bis_zeit', 'length', 'max' => 250),
            array('von_datum, bis_datum ', 'date', 'format' => array('dd-MM-yyyy')),
            array('von_datum', 'dateCompare', 'compareAttribute' => 'bis_datum'),
            array('von_datum', 'prueftJetztigesDatum'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, von_datum, bis_datum, von_zeit, bis_zeit, terminplan_idfs', 'safe', 'on' => 'search'),
        );
    }

    /* Kisanth Ketheeswaran 
     * Prüft ob von_datum nicht in der Vergangenheit ist, sondern in der Gegenwart bzw. Zukunft.     */

    public function prueftJetztigesDatum($attribute, $params) {
        $format = (!empty($params['format'])) ? $params['format'] : 'dd-MM-yyyy';
        $datum = CDateTimeParser::parse($this->$attribute, $format);
        $jetztigesDatum = CDateTimeParser::parse(date("d-m-Y"), $format);
        if (version_compare($datum, $jetztigesDatum, '>=')) {
            return;
        } else {
            $this->addError($attribute, "Von Datum darf nicht in der Vergangenheit sein.");
        }
    }

    /* Kisanth Ketheeswaran
     * Vergleicht die 2 Daten, denn das Datum bis_datum darf nicht
     * kleiner sein als von_datum      
     */
    public function dateCompare($attribute, $params) {
        $compareTo = $this->$params['compareAttribute'];
        $format = (!empty($params['format'])) ? $params['format'] : 'dd-MM-yyyy';
        $start = CDateTimeParser::parse($this->$attribute, $format);
        $ende = CDateTimeParser::parse($compareTo, $format);

        if (version_compare($start, $ende, '<')) {
            return;
        } else {
            $this->addError($attribute, "Von Datum darf nicht grösser als Bis Datum sein.");
        }
    }
    
    /*Kisanth Ketheeswaran
     * Das ist eine Funktion von der Superklasse CActiveRecord
     * Diese Funktion wird vor dem speichern aufgeführt, jedoch aber nach den Validierungen
     * Sie wandelt das normale Datum, welches in diesem Format ist z.B 05-12-2015,
     * in einem Timestamp um.
     */
    public function beforeSave() {
        $this->bis_datum = CDateTimeParser::parse($this->bis_datum, 'dd-MM-yyyy');
        $this->von_datum = CDateTimeParser::parse($this->von_datum, 'dd-MM-yyyy');
        return parent::beforeSave();
    }
    /*Kisanth Ketheeswaran
     * Das ist eine Funktion von der Superklasse CActiveRecord
     * Diese Funktion wird nach dem speichern aufgeführt, jedoch aber nach den Validierungen
     * Sie wandelt den Timestamp in der normalen Datum Ansicht.
     */
    public function afterSave() {
        $this->bis_datum = date("d-m-Y", $this->bis_datum);
        $this->von_datum = date("d-m-Y", $this->von_datum);
        return parent::afterSave();
    }
    /*Kisanth Ketheeswaran
     * Das ist eine Funktion von der Superklasse CActiveRecord
     * Diese Funktion wird nach dem find-Methode aufgeführt, welche die Daten
     * aus der Datenbank holt und diese wieder in diesm 
     * Datum Format umwandelt z.B 05-12-2015
     * 
     */
    public function afterFind() {
        $this->bis_datum = date("d-m-Y", $this->bis_datum);
        $this->von_datum = date("d-m-Y", $this->von_datum);
        return parent::afterFind();
    }
    

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.

        return array(
            'terminplanIdfs' => array(self::BELONGS_TO, 'Terminplan', 'terminplan_idfs'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'von_datum' => 'Von Datum',
            'bis_datum' => 'Bis Datum',
            'von_zeit' => 'Von Zeit',
            'bis_zeit' => 'Bis Zeit',
            'terminplan_idfs' => 'Terminplan Idfs',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('von_datum', $this->von_datum, true);
        $criteria->compare('bis_datum', $this->bis_datum, true);
        $criteria->compare('von_zeit', $this->von_zeit, true);
        $criteria->compare('bis_zeit', $this->bis_zeit, true);
        $criteria->compare('terminplan_idfs', $this->terminplan_idfs);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Ferien the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
