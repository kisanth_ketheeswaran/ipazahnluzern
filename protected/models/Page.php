<?php

/**
 * This is the model class for table "page".
 *
 * The followings are the available columns in table 'page':
 * @property integer $id
 * @property integer $structure_id
 * @property string $cms_h1
 * @property string $cms_h2
 * @property string $cms_content
 * @property string $obertitel
 * @property string $oberleadtext
 * @property string $linkertext
 * @property string $rechtertext
 * @property string $untertitel
 * @property string $unterleadtext
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $meta_title
 * @property string $layout
 * @property string $galleryID
 */
class Page extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return Page the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'page';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(array('structure_id', 'required'),
            array('structure_id, cms_content_position,galleryID', 'numerical', 'integerOnly' => true),
            array('cms_h1, layout', 'length', 'max' => 250),
            array('cms_content,obertitel,oberleadtext,linkertext,rechtertext,untertitel,unterleadtext ,meta_keywords, meta_description, meta_title, meta_tags', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, structure_id, cms_h1, cms_content, meta_keywords, cms_content_position, meta_description, meta_title, meta_tags, layout', 'safe', 'on' => 'search'),);
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array('structure' => array(self::BELONGS_TO, 'Structure', 'structure_id'),
            'pageOption' => array(self::HAS_ONE, 'PageOption', 'page_id'),
            'pagePortfolio' => array(self::HAS_MANY, 'PagePortfolio', 'page_id'),
            'pageSlider' => array(self::HAS_ONE, 'PageSlider', 'page_id'),
            'emotionpics' => array(self::HAS_MANY, 'Emotionpic', 'page_idfs'),
            'attachments' => array(self::HAS_MANY, 'PageAttachment', 'page_id'));
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array('id' => 'ID',
            'structure_id' => Yii::t('app', 'Struktur-Element'), 'cms_h1' => Yii::t('app', 'Seitentitel'),
            'cms_content_position' => Yii::t('app', 'Position'), 'cms_content' => Yii::t('app', 'Seiteninhalt'),
            'meta_keywords' => Yii::t('app', 'Meta Schlüsselwörter'),
            'meta_description' => Yii::t('app', 'Meta Beschreibung'),
            'meta_title' => Yii::t('app', 'Meta Titel'), 'meta_tags' => Yii::t('app', 'Head Tags'),
            'layout' => Yii::t('app', 'Layout'),
            'obertitel' => Yii::t('app', 'Titel'),
            'oberleadtext' => Yii::t('app', 'Einführungstext'),
            'linkertext' => Yii::t('app', 'linker Text'),
            'rechtertext' => Yii::t('app', 'rechter Text'),
            'untertitel' => Yii::t('app', 'Titel'),
            'unterleadtext' => Yii::t('app', 'Einführungstext'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('structure_id', $this->structure_id);
        $criteria->compare('cms_h1', $this->cms_h1, true);
        $criteria->compare('cms_content', $this->cms_content, true);
        $criteria->compare('obertitel', $this->obertitel, true);
        $criteria->compare('oberleadtext', $this->oberleadtext, true);
        $criteria->compare('linkertext', $this->linkertext, true);
        $criteria->compare('rechtertext', $this->rechtertext, true);
        $criteria->compare('untertitel', $this->untertitel, true);
        $criteria->compare('unterleadtext', $this->unterleadtext, true);
        $criteria->compare('cms_content_position', $this->cms_content_position, true);
        $criteria->compare('meta_keywords', $this->meta_keywords, true);
        $criteria->compare('meta_description', $this->meta_description, true);
        $criteria->compare('meta_title', $this->meta_title, true);
        $criteria->compare('meta_tags', $this->meta_tags, true);
        $criteria->compare('layout', $this->layout, true);

        return new CActiveDataProvider(get_class($this), array('criteria' => $criteria,));
    }

    public function searchDataProvider($searchParts = array()) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        
        foreach ($searchParts as $search) {
            $criteria->compare('t.cms_h1', $search, true, 'OR', true);
            $criteria->compare('t.cms_content', $search, true, 'OR', true);
            $criteria->compare('t.obertitel', $search, true, 'OR', true);
            $criteria->compare('t.oberleadtext', $search, true, 'OR', true);
            $criteria->compare('t.linkertext', $search, true, 'OR', true);
            $criteria->compare('t.rechtertext', $search, true, 'OR', true);
            $criteria->compare('t.untertitel', $search, true, 'OR', true);
            $criteria->compare('t.unterleadtext', $search, true, 'OR', true);
            $criteria->compare('t.cms_content_position', $search, true, 'OR', true);
            $criteria->compare('t.meta_keywords', $search, true, 'OR', true);
            $criteria->compare('t.meta_description', $search, true, 'OR', true);
            $criteria->compare('t.meta_title', $search, true, 'OR', true);
            $criteria->compare('t.meta_tags', $search, true, 'OR', true);
            $criteria->compare('t.layout', $search, true, 'OR', true);
        }
        //$criteria->with = array('structure');

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 5,
            )
        ));
    }

    public function beforeDelete() {
        $this->pageOption->delete();
        return parent::beforeDelete();
    }

    public function getWidgets() {
        return CmsWidget::model()->findAll('target = :target AND status = "active" ORDER BY position ASC', array(':target' => __CLASS__));
    }

    public function getWidget($widget) {
        $widgetName = str_replace('CmsWidget', '', $widget);
        return CmsWidget::model()->find('target = :target AND name = :name', array(':name' => $widgetName, ':target' => __CLASS__));
    }

    public function beforeSave() {
        if ($this->structure->is_landing) {
            $this->layout = 'landingpage';
        }
        return parent::beforeSave();
    }

    public function getHeaderLayout($id) {
        $cmsWidget = CmsWidgetPhpFile::model()->find('target_id = :id', array(':id' => $id));
        $allEmotion = Emotionpic::model()->findAll('page_idfs=:id', array(':id' => $id));
        $contact = CmsWidgetContactForm::model()->find('target_id = :id', array(':id' => $id));
        if ($cmsWidget->value == "WidgetTeam" || $cmsWidget->value == "WidgetGalerie" || $contact->value == 1) {
            return true;
        } elseif (!empty($allEmotion)) {
            return true;
        } else {
            return false;
        }
    }

}
