<?php

/**
 * This is the model class for table "termin".
 *
 * The followings are the available columns in table 'termin':
 * @property integer $id
 * @property string $anrede
 * @property string $nachname
 * @property string $vorname
 * @property string $geburtsdatum
 * @property string $strasse
 * @property integer $plz
 * @property string $ort
 * @property string $telefon
 * @property string $email
 * @property string $bemerkung
 * @property string $datum
 * @property string $zeit
 * @property integer $terminerinnerung
 * @property integer $bestatigung
 * @property string $dauer
 * @property string $status
 * @property integer $terminplan_idfs
 *
 * The followings are the available model relations:
 * @property Behandlung[] $behandlungs
 * @property Terminplan $terminplanIdfs
 */
class Termin extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'termin';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('anrede, nachname, vorname, strasse, plz, ort, telefon, email', 'required'),
            array('nachname,vorname,ort','CRegularExpressionValidator', 'pattern'=>'/^[a-zA-z äöü]+$/','message'=>Yii::t('site', "{attribute} muss aus Buchstaben bestehen.")),
            array('plz, terminerinnerung, bestatigung, terminplan_idfs', 'numerical', 'integerOnly' => true),
            array('anrede, nachname, vorname, ort, telefon, email, datum, zeit, dauer', 'length', 'max' => 250),
            array('geburtsdatum, status', 'length', 'max' => 50),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, anrede, nachname, vorname, geburtsdatum, strasse, plz, ort, telefon, email, bemerkung, datum, zeit, terminerinnerung, bestatigung, dauer, status, terminplan_idfs', 'safe'),
            array('id, anrede, nachname, vorname, geburtsdatum, strasse, plz, ort, telefon, email, bemerkung, datum, zeit, terminerinnerung, bestatigung, dauer, status, terminplan_idfs', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'behandlungs' => array(self::HAS_MANY, 'Behandlung', 'termin_idfs'),
            'terminplanIdfs' => array(self::BELONGS_TO, 'Terminplan', 'terminplan_idfs'),
            'behandlungsart' => array(self::HAS_MANY, 'Behandlungsart', array('behandlungsart_idfs' => 'id'), 'through' => 'behandlungs'),
        );
    }

    /* Kisanth Ketheeswaran
     * Das ist eine Funktion von der Superklasse CActiveRecord
     * Diese Funktion wird vor dem speichern aufgeführt, jedoch aber nach den Validierungen
     */

    public function beforeSave() {
        $this->datum = CDateTimeParser::parse($this->datum, 'dd-MM-yyyy');
        return parent::beforeSave();
    }

    /* Kisanth Ketheeswaran
     * Das ist eine Funktion von der Superklasse CActiveRecord
     * Diese Funktion wird nach dem find-Methode aufgeführt, welche die Daten
     * aus der Datenbank holen
     */

    public function afterFind() {
        $this->datum = date("d-m-Y", $this->datum);
        return parent::afterFind();
    }

    /* Kisanth Ketheeswaran
     * Das ist eine Funktion von der Superklasse CActiveRecord
     * Diese Funktion wird nach dem speichern aufgeführt, jedoch aber nach den Validierungen
     * Sie wandelt den Timestamp in der normalen Datum Ansicht.
     */

    public function afterSave() {
        $this->datum = date("d-m-Y", $this->datum);
        return parent::afterSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'anrede' => 'Anrede',
            'nachname' => 'Nachname',
            'vorname' => 'Vorname',
            'geburtsdatum' => 'Geburtsdatum',
            'strasse' => 'Strasse',
            'plz' => 'Plz',
            'ort' => 'Ort',
            'telefon' => 'Telefon',
            'email' => 'Email',
            'bemerkung' => 'Bemerkung',
            'datum' => 'Datum',
            'zeit' => 'Zeit',
            'terminerinnerung' => 'Terminerinnerung',
            'bestatigung' => 'Bestätigungsmail',
            'dauer' => 'Dauer',
            'status' => 'Status',
            'terminplan_idfs' => 'Terminplan Idfs',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('anrede', $this->anrede, true);
        $criteria->compare('nachname', $this->nachname, true);
        $criteria->compare('vorname', $this->vorname, true);
        $criteria->compare('geburtsdatum', $this->geburtsdatum, true);
        $criteria->compare('strasse', $this->strasse, true);
        $criteria->compare('plz', $this->plz);
        $criteria->compare('ort', $this->ort, true);
        $criteria->compare('telefon', $this->telefon, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('bemerkung', $this->bemerkung, true);
        $criteria->compare('datum', $this->datum, true);
        $criteria->compare('zeit', $this->zeit, true);
        $criteria->compare('terminerinnerung', $this->terminerinnerung);
        $criteria->compare('bestatigung', $this->bestatigung);
        $criteria->compare('dauer', $this->dauer, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('terminplan_idfs', $this->terminplan_idfs);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Termin the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function listAnrede($item = false) {
        $list = array(
            'herr' => Yii::t('site', 'Herr'),
            'frau' => Yii::t('site', 'Frau')
        );

        if ($item)
            return $list[$item];

        return $list;
    }

}
