<?php

/**
 * This is the model class for table "behandlung".
 *
 * The followings are the available columns in table 'behandlung':
 * @property integer $id
 * @property integer $termin_idfs
 * @property integer $behandlungsart_idfs
 *
 * The followings are the available model relations:
 * @property Behandlungsart $behandlungsartIdfs
 * @property Termin $terminIdfs
 */
class Behandlung extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'behandlung';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('termin_idfs, behandlungsart_idfs', 'required'),
            array('termin_idfs, behandlungsart_idfs', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, termin_idfs, behandlungsart_idfs', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'behandlungsartIdfs' => array(self::BELONGS_TO, 'Behandlungsart', 'behandlungsart_idfs'),
            'terminIdfs' => array(self::BELONGS_TO, 'Termin', 'termin_idfs'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'termin_idfs' => 'Termin Idfs',
            'behandlungsart_idfs' => 'Behandlungsart Idfs',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('termin_idfs', $this->termin_idfs);
        $criteria->compare('behandlungsart_idfs', $this->behandlungsart_idfs);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Behandlung the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    /* Kisanth Ketheeswaran
     * Die Funktion brauch ich um im View die Checkboxen auf checked zu setzen,
     * denn wenn die Daten übereinstimmt gibt dises Funktion den return Wert true
     * und somit wird es checked      
     */

    public function pruefObBehandlungAusgewaehlt($behandlung, $buchungId) {
        $buchungen = $this->model()->with('behandlungsartIdfs')->findAll('termin_idfs =:id', array(':id' => $buchungId));
        foreach ($buchungen as $buchung) {
            if ($buchung->behandlungsart_idfs == $behandlung->id) {
                return true;
            }
        }
        return false;
    }

    /* Kisanth Ketheeswaran
     * Diese Funktion speichert die Buchung und deren Behandlungen
     * in der Zwischentabelle Buchungbehandlung.
     * Einfachheitshalber lösche ich alle Eintrage der Buchung, 
     * in der Zwischentabelle, da es ja sein kann, dass er eins abwählt und 
     * da ja auch sowieso alle Behandlungen, die die Buchung gebraucht wieder 
     * im Array zu dieser Funktion kommen.
     */
    public function speicherBehandlung($buchungId, $behandlungen) {
        Behandlung::model()->deleteAll('termin_idfs =:id', array(':id' => $buchungId));
        foreach ($behandlungen as $behandlung) {
            $buchungBehandlung = new Behandlung;
            $buchungBehandlung->termin_idfs = (int) $buchungId;
            $buchungBehandlung->behandlungsart_idfs = (int) $behandlung;
            $buchungBehandlung->save();
        }
    }

}
