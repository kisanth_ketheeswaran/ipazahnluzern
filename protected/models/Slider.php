<?php

/**
 * This is the model class for table "slider".
 *
 * The followings are the available columns in table 'slider':
 * @property integer $id
 * @property string $title
 * @property string $summary
 * @property string $pic
 * @property string $link
 * @property string $type
 * @property integer $sort_order
 * @property string $locale_id
 * @property string $unit_hash
 * @property string $status
 */
class Slider extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Slider the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'slider';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, sort_order, locale_id, unit_hash, status', 'required'),
			array('sort_order', 'numerical', 'integerOnly'=>true),
			array('title, pic, link, unit_hash', 'length', 'max'=>250),
			array('type, locale_id, status', 'length', 'max'=>45),
			array('summary, slider_group_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, summary, pic, slider_group_id, link, type, sort_order, locale_id, unit_hash, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => Yii::t('app', 'Titel'),
			'summary' => Yii::t('app', 'Sub-Titel'),
			'pic' => Yii::t('app', 'Bild'),
			'link' => Yii::t('app', 'Link'),
			'slider_group_id' => Yii::t('app', 'Gruppe'),
			'type' => 'Type',
			'sort_order' => Yii::t('app', 'Reihenfolge'),
			'locale_id' => Yii::t('app', 'Sprache'),
			'unit_hash' => 'Unit Hash',
			'status' => Yii::t('app', 'Status'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
    public function search($type, $lang = null)
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $lang = empty($lang) ? LocaleModel::getDefault()->id : $lang;
        $criteria=new CDbCriteria;

        $criteria->condition = 'locale_id = "' . $lang . '" AND type = "' . $type . '"';

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('pic',$this->pic,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('sort_order',$this->sort_order);
        $criteria->compare('slider_group_id',$this->slider_group_id);
		$criteria->compare('locale_id',$this->locale_id,true);
		$criteria->compare('unit_hash',$this->unit_hash,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'sort_order ASC'
            )
		));
	}
    
    public static function listDataStatus($item = false)
    {
        $list = array(
            'active' => Yii::t('app', 'Aktiv'),
            'inactive' => Yii::t('app', 'Inaktiv')
        );
        
        if ($item)
            return $list[$item];
        
        return $list;
    }
    
    public function deleteModel()
    {
        $models = self::model()->findAll('unit_hash = :hash', array(':hash' => $this->unit_hash));
        foreach ($models as $model) {
            App::deleteFile($model->pic, 'slider');
            $model->delete();
        }
    }

    public function getModel($localeId, $fallback = false)
    {
        $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $this->unit_hash, ':locale' => $localeId));
        
        if (empty($model) && $fallback) {
            $model = self::model()->find('unit_hash = :unit AND locale_id = :locale', array(':unit' => $this->unit_hash, ':locale' => LocaleModel::getDefault()->id));
        }

        return $model;
    }
    
    public function getFile()
    {
        if (empty($this->pic)) {
            $sourceModel = $this->getModel(LocaleModel::getDefault()->id, true);
            return $sourceModel->pic;
        }
        
        return $this->pic;
    }
    
    public function getGroup()
    {
        return SliderGroup::model()->findByPk($this->slider_group_id);
    }
}