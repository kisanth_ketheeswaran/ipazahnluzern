<?php

class App {

    public static function hasAccess($role) {
        return ((Yii::app()->user->role == $role) || Yii::app()->user->role == 'admin');
    }

    public static function isMenuItemActive($module, $controller) {
        return ((Yii::app()->controller->module->id == $module) && (Yii::app()->controller->id == $controller)) ? true : false;
    }

    public static function removeEmptyValue($array) {
        foreach ($array as $key => $val) {
            if (empty($val))
                unset($array[$key]);
        }
        return $array;
    }

    public static function getUrlParts() {
        $url = Yii::app()->request->getUrl();
        $pos = strpos($url, '.html');
        if ($pos !== false) {
            $url = substr($url, 0, $pos + 5);
        }
        $urlParts = explode('/', $url);

        return self::removeEmptyValue($urlParts);
    }

    public static function getUrl($url) {
        return strpos($url, 'http://') === false ? 'http://' . $url : $url;
    }

    public static function recRmdir($path) {
        // schau' nach, ob das ueberhaupt ein Verzeichnis ist
        if (!is_dir($path)) {
            return -1;
        }
        // oeffne das Verzeichnis
        $dir = @opendir($path);

        // Fehler?
        if (!$dir) {
            return -2;
        }

        // gehe durch das Verzeichnis
        while (($entry = @readdir($dir)) !== false) {
            // wenn der Eintrag das aktuelle Verzeichnis oder das Elternverzeichnis
            // ist, ignoriere es
            if ($entry == '.' || $entry == '..')
                continue;
            // wenn der Eintrag ein Verzeichnis ist, dann 
            if (is_dir($path . '/' . $entry)) {
                // rufe mich selbst auf
                $res = self::recRmdir($path . '/' . $entry);
                // wenn ein Fehler aufgetreten ist
                if ($res == -1) { // dies duerfte gar nicht passieren
                    @closedir($dir); // Verzeichnis schliessen
                    return -2; // normalen Fehler melden
                } else if ($res == -2) { // Fehler?
                    @closedir($dir); // Verzeichnis schliessen
                    return -2; // Fehler weitergeben
                } else if ($res == -3) { // nicht unterstuetzer Dateityp?
                    @closedir($dir); // Verzeichnis schliessen
                    return -3; // Fehler weitergeben
                } else if ($res != 0) { // das duerfe auch nicht passieren...
                    @closedir($dir); // Verzeichnis schliessen
                    return -2; // Fehler zurueck
                }
            } else if (is_file($path . '/' . $entry) || is_link($path . '/' . $entry)) {
                // ansonsten loesche diese Datei / diesen Link
                $res = @unlink($path . '/' . $entry);
                // Fehler?
                if (!$res) {
                    @closedir($dir); // Verzeichnis schliessen
                    return -2; // melde ihn
                }
            } else {
                // ein nicht unterstuetzer Dateityp
                @closedir($dir); // Verzeichnis schliessen
                return -3; // tut mir schrecklich leid...
            }
        }

        // schliesse nun das Verzeichnis
        @closedir($dir);

        // versuche nun, das Verzeichnis zu loeschen
        $res = @rmdir($path);

        // gab's einen Fehler?
        if (!$res) {
            return -2; // melde ihn
        }

        // alles ok
        return 0;
    }

    public static function getPic($model, $attribute, $width = null, $height = null, $label = null, $onlyUrl = false, $folder = 'media') {
        if (!empty($model->$attribute)) {
            if (!file_exists(Yii::app()->basePath . '/../images/thumb/' . $width . '_' . $height . '_' . $model->$attribute)) {
                $mediaPic = Yii::app()->image->load(Yii::app()->basePath . '/../images/' . $folder . '/' . $model->$attribute);
                $mediaPic->resize($width, $height);
                $mediaPic->save(Yii::app()->basePath . '/../images/thumb/' . $width . '_' . $height . '_' . $model->$attribute);
            }

            if ($onlyUrl) {
                $mediaPic = '/images/thumb/' . $width . '_' . $height . '_' . $model->$attribute;
            } else {
                $mediaPic = CHtml::image('/images/thumb/' . $width . '_' . $height . '_' . $model->$attribute, $label, array('title' => $label));
            }


            return $mediaPic;
        }

        return null;
    }

    public static function deleteFile($file, $folder = 'media') {
        @unlink(Yii::app()->basePath . '/../images/' . $folder . '/' . $file);

        $handle = opendir(Yii::app()->basePath . '/../images/thumb');
        while ($data = readdir($handle)) {
            if (strpos($data, '_' . $file) !== false) {
                @unlink(Yii::app()->basePath . '/../images/thumb/' . $data);
            }
        }
        closedir($handle);
    }

    public static function getEmotionPic() {
        $cPage = Yii::app()->controller->cPage;

        if ($cPage) {
            if (get_class($cPage) == 'Page') {
                $emotionPic = $cPage->pageOption->emotion_pic;
                if ($emotionPic)
                    return '/images/option/' . $emotionPic;
                elseif ($cPage->structure->locale_id != LocaleModel::getDefault()->id) {
                    $emotionPic = $cPage->structure->getLocale(LocaleModel::getDefault()->id)->emotion_pic;
                    if ($emotionPic)
                        return '/images/option/' . $emotionPic;
                }
            }
        }

        return self::getRandomEmotionPic();
    }

    public static function getRandomEmotionPic() {
        $handle = opendir(Yii::app()->basePath . '/../images/emotion');
        $pics = array();
        while ($data = readdir($handle)) {
            if ($data != '.' && $data != '..')
                $pics[] = $data;
        }
        closedir($handle);
        shuffle($pics);

        return '/images/emotion/' . $pics[0];
    }

    public static function cutText($text, $length) {
        if (strlen($text) <= $length)
            return $text;

        return substr($text, 0, $length) . '...';
    }

    public static function writeSitemap() {
        $site_record = Structure::model()->find('type = "site" AND parent_id=0');
        $sitemapXml = Yii::app()->basePath . '/../sitemap.xml';
        $f = fopen($sitemapXml, 'w');
        fwrite($f, '<?xml version="1.0" encoding="utf-8"?>' . "\n");
        fwrite($f, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"' . "\n" . 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' . "\n" . 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9' . "\n" . 'http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . "\n");

        foreach (Structure::model()->findAll('type = "nav" AND status = "active" AND parent_id=' . $site_record->id . ' ORDER BY position ASC') as $structure) {
            //if($structure->parent_id == $site_record->id && !in_array($structure->link, $forbidLinks)){
            if ($structure->parent_id == $site_record->id) {
                fwrite($f, '<url>' . "\n" . '<loc>' . Yii::app()->params['sitemapUrl'] . '/' . $structure->locale_id . '/' . $structure->link . '.html</loc>' . "\n" . '<changefreq>always</changefreq>' . "\n" . '</url>' . "\n");
            }
            //if($structure->parent_id != $site_record->id && $structure->link != "account"){
            foreach (Structure::model()->findAll("type = 'nav' AND parent_id = '" . $structure->id . "' AND status = 'active'") as $subElement) {

                //if($subElement->parent_id != $site_record->id && $structure->link != "account" && !in_array($structure->link, $forbidLinks)){
                if ($subElement->parent_id != $site_record->id && $structure->link != "account") {
                    fwrite($f, '<url>' . "\n" . '<loc>' . Yii::app()->params['sitemapUrl'] . '/' . $structure->locale_id . '/' . $structure->link . '/' . $subElement->link . '.html</loc>' . "\n" . '<changefreq>always</changefreq>' . "\n" . '</url>' . "\n");

                    foreach (Structure::model()->findAll("type = 'nav' AND parent_id = '" . $subElement->id . "' AND status = 'active'") as $subSubElement) {
                        fwrite($f, '<url>' . "\n" . '<loc>' . Yii::app()->params['sitemapUrl'] . '/' . $structure->locale_id . '/' . $structure->link . '/' . $subElement->link . '/' . $subSubElement->link . '.html</loc>' . "\n" . '<changefreq>always</changefreq>' . "\n" . '</url>' . "\n");
                    }

                    /* foreach(Structure::model()->findAll("type = 'nav' AND parent_id = '".$subSubElement->id."' AND status = 'active'") as $thirdElement){
                      fwrite($f, '<url>' . "\n" . '<loc>'.Yii::app()->params['sitemapUrl'].'/' . $structure->locale_id . '/' . $structure->link . '/' . $subElement->link . '/' . $subSubElement->link . '/' . $thirdElement->link . '.html</loc>' . "\n"  .  '<changefreq>always</changefreq>' . "\n" . '</url>' . "\n");
                      } */
                }
            }
            //}
        }

        fwrite($f, '</urlset>');
        fclose($f);
    }

    public static function toAscii($str, $replace = array(), $delimiter = '-') {
        if (!empty($replace)) {
            $str = str_replace((array) $replace, ' ', $str);
        }

        $umlaut = array('ä', 'ü', 'ö', 'Ä', 'Ü', 'Ö');
        $uml_replace = array('ae', 'ue', 'oe', 'Ae', 'Ue', 'Oe');

        $str = str_replace($umlaut, $uml_replace, $str);

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }

}
