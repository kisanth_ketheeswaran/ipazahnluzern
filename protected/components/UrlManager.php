<?php

class UrlManager extends CUrlManager
{
  public function init()
  {
    $requestUrl = Yii::app()->request->getUrl();
    
    if (strpos($requestUrl, '?') !== false) {
        $posQuestionMark = strpos($requestUrl, '?');
        $subStr = substr($requestUrl, $posQuestionMark, strlen($requestUrl));
        $requestUrl = str_replace($subStr, '', $requestUrl);
    }
      
    if (strpos($requestUrl, '.html') !== false) {
    	if(isset($_GET['tolang']))
      		$requestUrl = str_replace($_GET['tolang'], '', $requestUrl);
      $this->addRules(array($requestUrl => 'website/index/cms'));
    }
    return parent::init();  
  }
  
}