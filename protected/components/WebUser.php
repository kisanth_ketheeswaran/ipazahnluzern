<?php

class WebUser extends CWebUser
{
  public function init() {
    parent::init();
    if ($this->isGuest)
      $this->setState('role', 'guest');
  }
}