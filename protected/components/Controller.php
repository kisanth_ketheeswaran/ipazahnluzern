<?php

class Controller extends CController
{
    public function init()
    {
    	
    	if (!isset(Yii::app()->session['language'])) {
    		Yii::app()->session['language'] = Yii::app()->language;
    	}
    	
    	if (isset($_GET['tolang']) && !empty($_GET['tolang'])) {
    		Yii::app()->session['language'] = $_GET['tolang'];
    		Yii::app()->language = Yii::app()->session['language'];
    		$requestUrl = str_replace('/?tolang=' . $_GET['tolang'], '', Yii::app()->request->getUrl());
            if (!empty($requestUrl)) {
    		  $this->redirect($requestUrl);
            } else {
            	$this->redirect('/');
            }
    	}
		
		Yii::app()->language = Yii::app()->session['language'];
    }
}